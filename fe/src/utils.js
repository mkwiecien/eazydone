import React from "react";
import moment from "moment";
import { notification, message, Tag, Tooltip } from "antd";
import { navigate } from "@reach/router";
import UserPopover from "./components/UserPopover/UserPopover";

export const handleError = async error => {
  let errMsg = "";

  if (error.message) {
    errMsg = error.message.replace("GraphQL error:", "").trim();
  } else if (typeof error !== "string") {
    try {
      const parsed = await error.json();
      errMsg = parsed.message || parsed.msg;
    } catch (err) {
      try {
        errMsg = await error.text();
      } catch (e) {}
    }
  } else {
    errMsg = error;
  }

  console.error(errMsg);

  if (errMsg.indexOf("status code 401") > -1) {
    navigate("/login");
    notification["error"]({
      message: "Error",
      description: "Authentication error. Please log in."
    });
    return;
  }

  notification["error"]({
    message: "Error",
    description: errMsg
  });
};

export const validateEmail = email => {
  var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

export const generatePassword = () =>
  Math.random()
    .toString(36)
    .slice(-8);

export const renderState = (task, space, onClick = () => {}) => {
  const state = space.states.find(s => s.text === task.state);
  return (
    <Tooltip
      mouseEnterDelay={0.5}
      title="Click to move task to the next state."
    >
      <Tag onClick={onClick} color={state.color}>
        {state.text}
      </Tag>
    </Tooltip>
  );
};

export const rand = (min, max) => {
  return parseInt(Math.random() * (max - min + 1), 10) + min;
};

export const formatTime = minutes => {
  const h = Math.floor(minutes / 60);
  const m = minutes % 60;
  return `${h ? h + "h" : ""} ${m}m`;
};

export const getRandomColor = () => {
  var h = rand(1, 360); // color hue between 1 and 360
  var s = rand(30, 100); // saturation 30-100%
  var l = rand(30, 70); // lightness 30-70%
  return "hsl(" + h + "," + s + "%," + l + "%)";
};

export const idToDate = _id => moment(parseInt(_id.substr(0, 8), 16) * 1000);

export const getTaskProgress = (task, states) => {
  if (task.todos.length === 0) {
    return task.state === states[states.length - 1].text ? 100 : 0;
  }
  return Math.round(
    (task.todos.filter(t => t.done).length * 100) / task.todos.length
  );
};

export const getIndexAsMember = (user, space) => {
  const accounts = space.accounts || [];
  let index = accounts.findIndex(el => el === user._id);
  if (index === -1) index = accounts.findIndex(el => el === user.username);
  if (index === -1) index = accounts.findIndex(el => el === user.email);

  return index;
};

export const isUsersSpace = (user, space) =>
  (space.frozen && space.ownerId === user._id) ||
  (space.frozen && space.companyId) ||
  getIndexAsMember(user, space) > -1;

// returns 1 or -1
export const copyToClipboard = (
  text,
  successMsg = "Text copied.",
  errMsg = "Oops, unable to copy."
) => {
  const tarea = document.createElement("textarea");

  // Place in top-left corner of screen regardless of scroll position.
  tarea.style.position = "fixed";
  tarea.style.top = 0;
  tarea.style.left = 0;

  // Ensure it has a small width and height. Setting to 1px / 1em
  // doesn't work as this gives a negative w/h on some browsers.
  tarea.style.width = "2em";
  tarea.style.height = "2em";

  // We don't need padding, reducing the size if it does flash render.
  tarea.style.padding = 0;

  // Clean up any borders.
  tarea.style.border = "none";
  tarea.style.outline = "none";
  tarea.style.boxShadow = "none";

  // Avoid flash of white box if rendered for any reason.
  tarea.style.background = "transparent";

  tarea.value = text;

  document.body.appendChild(tarea);
  tarea.focus();
  tarea.select();

  let success = true;
  try {
    success = document.execCommand("copy");
  } catch (err) {
    success = false;
  }

  if (success) message.info(successMsg);
  else message.error(errMsg);

  document.body.removeChild(tarea);
};

export function linkify(text) {
  const urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#/%?=~_|!:,.;]*[-A-Z0-9+&@#/%=~_|])/gi;

  let match = null;
  let index = 0;
  let result = [];
  while ((match = urlRegex.exec(text))) {
    const link = match[0];
    result.push(<span key={index}>{text.slice(index, match.index)}</span>);
    result.push(
      <a href={link} key={index + link}>
        {link}
      </a>
    );

    index = match.index + link.length;
  }
  result.push(text.slice(index));
  result = result.filter(el => !!el);
  return result;
}

export function linkifyAndHoverify(text, users) {
  const mentionRegex = /@([^.,/#!$%^&*;:{}=\-_`~()\s$]*)/g;
  const urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#/%?=~_|!:,.;]*[-A-Z0-9+&@#/%=~_|])/gi;

  // first links
  let match = null;
  let index = 0;
  let linksResult = [];
  while ((match = urlRegex.exec(text))) {
    const link = match[0];
    linksResult.push({
      type: "text",
      value: text.slice(index, match.index)
    });
    linksResult.push({
      type: "link",
      value: link
    });
    index = match.index + link.length;
  }
  linksResult.push({
    type: "text",
    value: text.slice(index)
  });
  linksResult = linksResult.filter(el => !!el);

  let result = [];
  linksResult.forEach((block, blockIndex) => {
    if (block.type === "link") {
      result.push(
        <a href={block.value} key={"link-" + blockIndex + generateId()}>
          {block.value}
        </a>
      );
    } else {
      match = null;
      index = 0;
      while ((match = mentionRegex.exec(block.value))) {
        const mention = match[0];
        const username = mention.slice(1);
        const user = users.find(u => u.username === username);

        result.push(
          <span key={"text-" + blockIndex + generateId()}>
            {block.value.slice(index, match.index)}
          </span>
        );
        result.push(
          <UserPopover
            user={user}
            key={"mention-" + blockIndex + generateId()}
            color
          >
            <a href="# ">{mention}</a>
          </UserPopover>
        );
        index = match.index + mention.length;
      }

      const lastVal = block.value.slice(index);
      if (lastVal)
        result.push(
          <span key={"last-" + blockIndex + generateId()}>{lastVal}</span>
        );
    }
  });
  return result;
}

export const isUserMentioned = (username, text) => {
  const match = text.match(/@([^.,/#!$%^&*;:{}=\-_`~()\s$]*)/g);
  if (match) {
    return username === match[0].slice(1);
  }
  return false;
};

export const generateId = () =>
  "_" +
  Math.random()
    .toString(36)
    .substr(2, 9);

export function notify(title, body, onClick) {
  if (Notification.permission !== "granted") Notification.requestPermission();
  else {
    var notification = new Notification(title, {
      icon: "./untitled.svg",
      body
    });

    notification.onclick = function() {
      onClick && onClick();
      window.parent.focus();
      window.focus(); //just in case, older browsers
      this.close();
    };
  }
}

export const isMongoId = str => !!str.match(/^[0-9a-fA-F]{24}$/);
