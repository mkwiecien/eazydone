import React, { Component } from "react";
import ReactDOM from "react-dom";
import { ApolloProvider } from "@apollo/react-hooks";
import { StripeProvider, Elements } from "react-stripe-elements";

// import ApolloClient from "apollo-boost";
import { ApolloClient } from "apollo-client";
import { split } from "apollo-link";
import { HttpLink } from "apollo-link-http";
import { WebSocketLink } from "apollo-link-ws";
import { getMainDefinition } from "apollo-utilities";
import { InMemoryCache } from "apollo-cache-inmemory";
import { onError } from "apollo-link-error";
import { ApolloLink } from "apollo-link";
import { DndProvider } from "react-dnd";
import HTML5Backend from "react-dnd-html5-backend";

import { notification } from "antd";
import * as serviceWorker from "./serviceWorker";
import { Router, navigate, globalHistory } from "@reach/router";
import App from "./containers/App/App";
import Login from "./containers/Login/Login";
import SignIn from "./containers/SignIn/SignIn";
import Objection from "./containers/Objection/Objection";
import FirstLogin from "./containers/FirstLogin/FirstLogin";
import "./index.css";
import "semantic-ui-css/semantic.min.css";
import ForgotPassword from "./containers/ForgotPassword/ForgotPassword";

export const LAST_SEEN_DELAY = 30;

export const apiUrl =
  process.env.NODE_ENV === "production"
    ? "https://app.eazydone.com/api/"
    : "http://localhost:4000/";

export const websocketUrl =
  process.env.NODE_ENV === "production"
    ? "wss://app.eazydone.com/ws/graphql"
    : "ws://localhost:1337/graphql";

// Create an http link:
const httpLink = new HttpLink({
  uri: `${apiUrl}graphql`,
  credentials: "include"
});

const wsLink = new WebSocketLink({
  uri: websocketUrl,
  options: {
    reconnect: true
  }
});

// using the ability to split links, you can send data to each link
// depending on what kind of operation is being sent
const link = split(
  // split based on operation type
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === "OperationDefinition" &&
      definition.operation === "subscription"
    );
  },
  wsLink,
  httpLink
);

// const history = createBrowserHistory();
const cache = new InMemoryCache({
  dataIdFromObject: obj => (obj ? obj._id || obj.id || null : null)
});

const client = new ApolloClient({
  link: ApolloLink.from([
    onError(({ graphQLErrors, networkError }) => {
      if (graphQLErrors)
        graphQLErrors.forEach(({ message, locations, path }) =>
          console.log(
            `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
          )
        );
      if (networkError) {
        console.log(`[Network error]: ${networkError}`);
        notification["error"]({
          message: "Network error ",
          description: (
            <div>
              <p>Please check your internet connection and refresh the page.</p>
              <p style={{ fontSize: "80%" }}>
                In case of further issues please contact us via email:
                help@eazydone.com
              </p>
            </div>
          )
        });
      }
    }),
    link
  ]),
  cache
});

// // no need to export it, it can be accessed
// // with withApollo wrapper
// const client = new ApolloClient({
//   uri: `${apiUrl}graphql`,
//   credentials: "include",
//   cache
// });

globalHistory.listen(change => {
  if (change.location.state && change.location.state.title) {
    document.title = change.location.state.title;
  } else {
    document.title = "eazydone";
  }
});

class EazyDone extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {}
    };
  }

  setUser = user => this.setState({ user });

  componentDidMount = () => {
    if (
      ["login", "signin", "reset-password"].some(path =>
        window.location.pathname.includes(path)
      )
    ) {
      return null;
    }

    fetch(`${apiUrl}login`, {
      method: "POST",
      credentials: "include"
    })
      .then(response => response.json())
      .then(response => {
        if (response.msg) {
          notification["info"]({
            message: "Info",
            description: "You are not logged in"
          });
          navigate("/login");
          // history.push("/login");
        } else {
          this.setUser(response);
        }
      })
      .catch(err => {
        console.error(err);
        notification["error"]({
          message: "Network error ",
          description: (
            <div>
              <p>Please check your internet connection and refresh the page.</p>
              <p style={{ fontSize: "80%" }}>
                In case of further issues please contact us via email:
                help@eazydone.com
              </p>
            </div>
          )
        });
      });
  };

  setUser = user => this.setState({ user });

  render = () => {
    // return <App {...this.props} user={this.state.user} />;
    const user = this.state.user;
    if (!user.username && user.email) {
      return (
        <Objection
          msg="This is your first login. Please fill the form below."
          renderContent={() => <FirstLogin user={user} />}
        />
      );
    }

    return (
      <Router style={{ height: "100%" }}>
        <App path="/*" user={this.state.user} />
        <Login path="/login" onLogin={this.setUser} />
        <SignIn path="/signin" onSignIn={this.setUser} />
        <ForgotPassword path="/reset-password" />
        <ForgotPassword path="/reset-password/:token" />
      </Router>
    );
  };
}

ReactDOM.render(
  <ApolloProvider client={client}>
    <DndProvider backend={HTML5Backend}>
      <StripeProvider apiKey={process.env.REACT_APP_STRIPE_KEY}>
        <Elements>
          <EazyDone />
        </Elements>
      </StripeProvider>
    </DndProvider>
  </ApolloProvider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.register({
  onUpdate: registration => {
    window.onAlertCancel = function onAlertCancel() {
      document.body.removeChild(alert);
    };
    window.onAlertConfirm = function onAlertConfirm() {
      const waitingServiceWorker = registration.waiting;
      if (waitingServiceWorker) {
        waitingServiceWorker.addEventListener("statechange", function(event) {
          if (event.target.state === "activated") {
            window.location.reload();
          }
        });

        waitingServiceWorker.postMessage({ type: "SKIP_WAITING" });
      }
    };
    const alert = document.createElement("div");
    alert.innerHTML = `
      <div
        style="
          height: 40px;
          display: grid;
          grid-template-columns: 250px 100px 100px;
          width: 500px;
          align-items: center;
          justify-content: center;
        "
      >
        <span>There is new version available.</span>
        <button
          onclick="window.onAlertConfirm()"
          style="cursor: pointer; padding: 5px;"
        >
          Update
        </button>
        <button
          onclick="window.onAlertCancel()"
          style="cursor: pointer; padding: 5px;"
        >
          Cancel
        </button>
      </div>
    `;

    document.body.insertBefore(alert, document.getElementById("root"));
  }
});
