import { gql } from "apollo-boost";

export const SIGN_IN = gql`
  mutation AddUser($username: String!, $email: String!, $password: String!) {
    addUser(username: $username, email: $email, password: $password) {
      username
      email
    }
  }
`;

export const CHECK_IN = gql`
  mutation CheckIn($username: String!) {
    checkIn(username: $username) {
      username
      lastSeen
    }
  }
`;

export const USER = gql`
  query User($username: String, $email: String) {
    user(username: $username, email: $email) {
      _id
      username
      email
      name
      surname
      avatar
      active
      accountType
      lastSelectedSpaceId
      jobTitle
      isDemo
      stripeSubscriptionId
      companies {
        name
        description
        logo
        _id
        ownerId
        owner {
          _id
          username
          email
          avatar
          lastSeen
          jobTitle
          name
          surname
        }
        users {
          _id
          username
          email
          active
          avatar
          lastSeen
          jobTitle
          name
          surname
        }
        inactiveUsers {
          _id
          email
          password
        }
        storagePerUserMb
      }
    }
  }
`;

export const USER_SPACES = gql`
  query UserSpaces($username: String) {
    userSpaces(username: $username) {
      _id
      name
      frozen
      logo
      description
      private
      accounts
      companyId
      ownerId
      users {
        username
        _id
        avatar
        jobTitle
        name
        surname
      }
      states {
        text
        color
      }
    }
  }
`;

export const EDIT_USER = gql`
  mutation EditUser(
    $username: String
    $email: String!
    $password: String
    $name: String
    $surname: String
    $avatar: String
    $lastSelectedSpaceId: String
    $jobTitle: String
  ) {
    editUser(
      username: $username
      email: $email
      password: $password
      active: true
      name: $name
      surname: $surname
      avatar: $avatar
      lastSelectedSpaceId: $lastSelectedSpaceId
      jobTitle: $jobTitle
    ) {
      _id
      name
      surname
      avatar
      lastSelectedSpaceId
      jobTitle
      name
      surname
    }
  }
`;

// export const COMPANIES = gql`
//   query Companies($userId) {
//     companies(userId: $userId) {
//       name
//     }
//   }
// `;

export const ADD_COMPANY = gql`
  mutation AddCompany(
    $name: String!
    $description: String
    $logo: String
    $ownerId: String!
    $accounts: [AccountInput]
  ) {
    addCompany(
      name: $name
      description: $description
      logo: $logo
      ownerId: $ownerId
      accounts: $accounts
    ) {
      _id
      name
      logo
      description
      ownerId
      owner {
        _id
        username
        email
        avatar
        lastSeen
        jobTitle
        name
        surname
      }
      users {
        _id
        username
        email
        active
        password
        avatar
        lastSeen
        jobTitle
        name
        surname
      }
      inactiveUsers {
        email
        _id
        password
      }
    }
  }
`;

export const EDIT_COMPANY = gql`
  mutation EditCompany(
    $_id: String!
    $name: String
    $description: String
    $logo: String
    $accounts: [AccountInput]
  ) {
    editCompany(
      _id: $_id
      name: $name
      description: $description
      logo: $logo
      accounts: $accounts
    ) {
      _id
      name
      description
      ownerId
      logo
      users {
        _id
        username
        email
        active
        avatar
        jobTitle
        name
        surname
      }
      inactiveUsers {
        _id
        email
        password
      }
    }
  }
`;

export const DELETE_COMPANY = gql`
  mutation DeleteCompany($_id: String!) {
    deleteCompany(_id: $_id) {
      _id
    }
  }
`;

export const CREATE_COMPANY_SPACE = gql`
  mutation CreateCompanySpace($_id: String!, $name: String!) {
    createSpace(
      private: false
      frozen: true
      name: $name
      description: "Shared space for eveyrone at the company."
      logo: "https://eazydone.s3.eu-west-3.amazonaws.com/logos/home.png"
      companyId: $_id
    ) {
      _id
      name
      frozen
      logo
      description
      private
      accounts
      ownerId
      companyId
      users {
        _id
        username
        avatar
        jobTitle
        name
        surname
      }
      states {
        text
        color
      }
    }
  }
`;

export const CREATE_SPACE = gql`
  mutation CreateSpace(
    $private: Boolean
    $logo: String
    $name: String!
    $description: String
    $accounts: [String]
    $ownerId: String!
    $companyId: String
    $states: [StateInput]
  ) {
    createSpace(
      private: $private
      frozen: false
      logo: $logo
      name: $name
      description: $description
      accounts: $accounts
      ownerId: $ownerId
      companyId: $companyId
      states: $states
    ) {
      _id
      name
      frozen
      logo
      description
      private
      accounts
      companyId
      ownerId
      users {
        _id
        username
        avatar
        jobTitle
        name
        surname
      }
      states {
        text
        color
      }
    }
  }
`;

export const EDIT_SPACE = gql`
  mutation EditSpace(
    $_id: String!
    $private: Boolean
    $logo: String
    $name: String
    $description: String
    $accounts: [String]
    $companyId: String
    $states: [StateInput]
  ) {
    editSpace(
      _id: $_id
      private: $private
      logo: $logo
      name: $name
      description: $description
      accounts: $accounts
      companyId: $companyId
      states: $states
    ) {
      _id
      name
      frozen
      logo
      description
      private
      accounts
      companyId
      ownerId
      users {
        _id
        username
        avatar
        jobTitle
        name
        surname
      }
      states {
        text
        color
      }
    }
  }
`;

export const DELETE_SPACE = gql`
  mutation DeleteSpace($_id: String!) {
    deleteSpace(_id: $_id) {
      _id
    }
  }
`;

export const SPACE_MILESTONES = gql`
  query SpaceMilestones($spaceId: String!) {
    spaceMilestones(spaceId: $spaceId) {
      _id
      name
      description
      spaceId
      startDate
      endDate
      completed
    }
  }
`;

export const SPACES_MILESTONES = gql`
  query SpacesMilestones($spaceIds: [String]) {
    spacesMilestones(spaceIds: $spaceIds) {
      _id
      name
      description
      spaceId
      startDate
      endDate
      completed
    }
  }
`;

export const ADD_MILESTONE = gql`
  mutation AddMilestone(
    $name: String!
    $description: String
    $spaceId: String!
    $startDate: Int
    $endDate: Int
  ) {
    addMilestone(
      name: $name
      description: $description
      spaceId: $spaceId
      startDate: $startDate
      endDate: $endDate
    ) {
      _id
      name
      description
      spaceId
      startDate
      endDate
      completed
    }
  }
`;

export const EDIT_MILESTONE = gql`
  mutation EditMilestone(
    $_id: String!
    $name: String
    $description: String
    $spaceId: String
    $startDate: Int
    $endDate: Int
    $completed: Boolean
  ) {
    editMilestone(
      _id: $_id
      name: $name
      description: $description
      spaceId: $spaceId
      startDate: $startDate
      endDate: $endDate
      completed: $completed
    ) {
      _id
      name
      description
      spaceId
      startDate
      endDate
      completed
    }
  }
`;

export const DELETE_MILESTONE = gql`
  mutation DeleteMilestone($_id: String!) {
    deleteMilestone(_id: $_id) {
      _id
    }
  }
`;

export const TASKS = gql`
  query Tasks($spacesIds: [String]) {
    tasks(spacesIds: $spacesIds) {
      _id
      title
      createdBy
      updatedBy
      description
      todos {
        text
        done
      }
      milestoneId
      spaceId
      files
      assignee
      state
      difficulty
      comments {
        text
        author
        date
      }
      deadline
      order
      tagsNames
      history {
        text
        icon
        date
        diff
        updatedBy
      }
    }
  }
`;

export const CREATE_TASK = gql`
  mutation CreateTask(
    $title: String!
    $createdBy: String!
    $description: String
    $todos: [TodoInput]
    $milestoneId: String
    $spaceId: String!
    $files: [String]
    $state: String!
    $difficulty: Int!
    $assignee: String
    $comments: [CommentInput]
    $deadline: Int
    $tagsNames: [String]!
  ) {
    createTask(
      title: $title
      createdBy: $createdBy
      updatedBy: $createdBy
      description: $description
      todos: $todos
      milestoneId: $milestoneId
      spaceId: $spaceId
      files: $files
      state: $state
      difficulty: $difficulty
      assignee: $assignee
      comments: $comments
      deadline: $deadline
      tagsNames: $tagsNames
    ) {
      _id
      title
      createdBy
      updatedBy
      description
      todos {
        done
        text
      }
      milestoneId
      spaceId
      files
      state
      difficulty
      assignee
      comments {
        author
        text
        date
      }
      deadline
      order
      tagsNames
      history {
        text
        icon
        date
        diff
        updatedBy
      }
    }
  }
`;

export const EDIT_TASK = gql`
  mutation EditTask(
    $_id: String!
    $title: String
    $description: String
    $todos: [TodoInput]
    $milestoneId: String
    $spaceId: String
    $files: [String]
    $state: String
    $difficulty: Int
    $assignee: String
    $comments: [CommentInput]
    $deadline: Int
    $updatedBy: String
    $tagsNames: [String]
  ) {
    editTask(
      _id: $_id
      title: $title
      description: $description
      todos: $todos
      milestoneId: $milestoneId
      spaceId: $spaceId
      files: $files
      state: $state
      difficulty: $difficulty
      assignee: $assignee
      comments: $comments
      deadline: $deadline
      updatedBy: $updatedBy
      tagsNames: $tagsNames
    ) {
      _id
      title
      createdBy
      updatedBy
      description
      todos {
        text
        done
      }
      milestoneId
      spaceId
      files
      state
      difficulty
      assignee
      comments {
        text
        author
        date
      }
      deadline
      order
      tagsNames
      history {
        text
        icon
        date
        diff
        updatedBy
      }
    }
  }
`;

export const EDIT_TASKS = gql`
  mutation EditTasks($ids: [String]!) {
    editTasks(ids: $ids) {
      _id
      milestoneId
      order
    }
  }
`;

export const CHANGE_ORDER = gql`
  mutation ChangeOrder($tasks: [TaskOrderInput]) {
    changeOrder(tasks: $tasks) {
      _id
      order
      state
      updatedBy
      history {
        text
        icon
        date
        diff
        updatedBy
      }
    }
  }
`;

export const DELETE_TASK = gql`
  mutation DeleteTask($_id: String!) {
    deleteTask(_id: $_id) {
      _id
    }
  }
`;

export const DELETE_TASKS = gql`
  mutation DeleteTasks($ids: [String]!) {
    deleteTasks(ids: $ids) {
      _id
    }
  }
`;

export const TIME_LOG = gql`
  query TimeLog($userId: String, $taskId: String) {
    timeLog(userId: $userId, taskId: $taskId) {
      _id
      userId
      taskId
      minutes
    }
  }
`;

export const TIME_LOGS = gql`
  query TimeLogs($tasksIds: [String]) {
    timeLogs(tasksIds: $tasksIds) {
      _id
      userId
      taskId
      minutes
    }
  }
`;

export const LOG_TIME = gql`
  mutation LogTime($userId: String!, $taskId: String!, $minutes: Int!) {
    logTime(userId: $userId, taskId: $taskId, minutes: $minutes) {
      _id
      userId
      taskId
      minutes
    }
  }
`;

export const DELETE_LAST_TIME_LOG = gql`
  mutation DeleteLastTimeLog($userId: String!, $taskId: String!) {
    deleteLastTimeLog(userId: $userId, taskId: $taskId) {
      _id
    }
  }
`;

export const PROGRESS = gql`
  query Progress($milestoneIds: [String]!) {
    progress(milestoneIds: $milestoneIds) {
      _id
      milestoneId
      progress
    }
  }
`;

export const COMPANY_MEETINGS = gql`
  query CompanyMeetings($companyId: String!, $username: String) {
    companyMeetings(companyId: $companyId, username: $username) {
      _id
      name
      description
      startDate
      endDate
      private
      repetition
      companyId
      invitations {
        username
        state
      }
      invitedSpaces
      color
      createdBy
      updatedBy
    }
  }
`;

export const CREATE_MEETING = gql`
  mutation CreateMeeting(
    $name: String!
    $description: String
    $startDate: Int!
    $endDate: Int
    $private: Boolean
    $invitations: [InvitationInput]
    $invitedSpaces: [String]
    $repetition: String
    $companyId: String
    $color: String!
    $createdBy: String!
  ) {
    createMeeting(
      name: $name
      description: $description
      startDate: $startDate
      endDate: $endDate
      private: $private
      invitations: $invitations
      invitedSpaces: $invitedSpaces
      repetition: $repetition
      companyId: $companyId
      color: $color
      createdBy: $createdBy
    ) {
      _id
      name
      description
      startDate
      endDate
      private
      repetition
      companyId
      invitations {
        username
        state
      }
      invitedSpaces
      color
      createdBy
      updatedBy
    }
  }
`;

export const EDIT_MEETING = gql`
  mutation EditMeeting(
    $_id: String!
    $name: String!
    $description: String
    $startDate: Int!
    $endDate: Int
    $private: Boolean
    $invitations: [InvitationInput]
    $invitedSpaces: [String]
    $repetition: String
    $updatedBy: String
  ) {
    editMeeting(
      _id: $_id
      name: $name
      description: $description
      startDate: $startDate
      endDate: $endDate
      private: $private
      invitations: $invitations
      invitedSpaces: $invitedSpaces
      repetition: $repetition
      updatedBy: $updatedBy
    ) {
      _id
      name
      description
      startDate
      endDate
      private
      repetition
      companyId
      invitations {
        username
        state
      }
      invitedSpaces
      color
      createdBy
      updatedBy
    }
  }
`;

export const DELETE_MEETING = gql`
  mutation DeleteMeeting($_id: String!) {
    deleteMeeting(_id: $_id) {
      _id
    }
  }
`;

export const MESSAGES = gql`
  query Messages($username: String!, $spacesIds: [String]!) {
    messages(username: $username, spacesIds: $spacesIds) {
      _id
      from
      to
      text
    }
  }
`;

export const CHAT = gql`
  query Chat($from: String, $to: String!, $spaceId: String, $offset: Int!) {
    chat(from: $from, to: $to, spaceId: $spaceId, offset: $offset) {
      _id
      from
      to
      text
    }
  }
`;

export const ADD_MESSAGE = gql`
  mutation AddMessage($text: String!, $from: String!, $to: String!) {
    addMessage(text: $text, from: $from, to: $to) {
      _id
      from
      to
      text
    }
  }
`;

export const NOTIFICATIONS = gql`
  query Notifications($username: String!) {
    notifications(username: $username) {
      _id
      title
      text
      link
      seen
      username
      important
    }
  }
`;

export const MARK_SEEN = gql`
  mutation MarkSeen($_id: String!) {
    markSeen(_id: $_id) {
      _id
      title
      text
      link
      seen
      username
    }
  }
`;

export const MARK_SEEN_MANY = gql`
  mutation MarkSeenMany($ids: [String!]!) {
    markSeenMany(ids: $ids) {
      _id
    }
  }
`;

export const TYPE = gql`
  mutation Type($username: String!, $to: String!) {
    type(username: $username, to: $to) {
      username
    }
  }
`;

export const TIME_REPORT = gql`
  query TimeReport($userId: String!, $startDate: Int!, $endDate: Int!) {
    timeReport(userId: $userId, startDate: $startDate, endDate: $endDate) {
      _id
      minutes
      task {
        title
        _id
      }
    }
  }
`;

export const FILES = gql`
  query Files($spacesIds: [String]) {
    files(spacesIds: $spacesIds) {
      _id
      url
      name
      uploadedBy
      spaceId
      size
    }
  }
`;

export const TAGS = gql`
  query Tags($spacesIds: [String]!) {
    tags(spacesIds: $spacesIds) {
      _id
      name
      color
      spaceId
    }
  }
`;

export const ADD_TAG = gql`
  mutation AddTag($name: String!, $color: String!, $spaceId: String!) {
    addTag(name: $name, color: $color, spaceId: $spaceId) {
      _id
      name
      color
      spaceId
    }
  }
`;

export const EDIT_TAG = gql`
  mutation EditTag($_id: String!, $name: String, $color: String) {
    editTag(_id: $_id, name: $name, color: $color) {
      _id
      name
      color
      spaceId
    }
  }
`;

export const DELETE_TAG = gql`
  mutation DeleteTag($_id: String!) {
    deleteTag(_id: $_id) {
      _id
    }
  }
`;
