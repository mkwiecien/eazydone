import React from "react";
import moment from "moment";
import { navigate } from "@reach/router";
import { Icon } from "antd";
import CountUp from "react-countup";
import bg from "../../bg.jpg";
import "./Home.css";

export default function Home(props) {
  const { tasks, meetings, user, spaces } = props;

  const doneTodosCount = tasks.reduce((prev, val) => {
    return prev + val.todos.filter(t => t.done).length;
  }, 0);
  const doneTasksCount = tasks.filter(task => {
    const spaceStates = spaces.find(s => s._id === task.spaceId).states;
    const lastState = spaceStates[spaceStates.length - 1];
    return task.state === lastState.text;
  }).length;

  const fileteredMeetings = [...meetings].filter(m => {
    const startDate = moment.unix(m.startDate);
    const endDate = m.endDate && moment.unix(m.endDate);
    const now = moment();

    if (m.repetition === "None") return startDate.isAfter(now);
    return now.isBefore(endDate);
  });
  const sortedMeetings = fileteredMeetings
    .filter(m => m.name !== "Private")
    .sort((a, b) => a.startDate - b.startDate)
    .slice(0, 10);

  const filteredTasks = tasks.filter(t => {
    const space = spaces.find(s => s._id === t.spaceId);
    const firstState = space.states[0].text;
    const lastState = space.states[space.states.length - 1].text;

    return (
      t.assignee === user.username &&
      t.state !== firstState &&
      t.state !== lastState
    );
  });

  function renderStartDate(m) {
    let startDate = moment.unix(m.startDate);
    const now = moment();

    if (startDate.isBefore(now)) {
      const daysDiff = now.diff(startDate, "days");
      if (daysDiff > 0) {
        startDate = startDate.add(daysDiff, "days");
      } else {
        startDate = startDate.add(1, "day");
      }
    }

    return startDate.format("MMMM Do YYYY, h:mm a");
  }

  return (
    <div className="Home" style={{ backgroundImage: `url(${bg})` }}>
      <div className="counters">
        <div className="counter left">
          <CountUp
            className="count"
            end={doneTodosCount}
            delay={1}
            duration={3}
          />
          <span className="counter-title">done todos</span>
        </div>
        <div className="counter right">
          <CountUp
            className="count"
            end={doneTasksCount}
            delay={1}
            duration={3}
          />
          <span className="counter-title">done tasks</span>
        </div>
      </div>

      {sortedMeetings.length > 0 && (
        <div className="meetings">
          <span className="section-title">
            <Icon type="calendar" style={{ marginRight: 10 }} />
            Upcoming meetings
          </span>
          <div className="meetings-list">
            {sortedMeetings.map(m => (
              <div
                key={m._id}
                className="meeting"
                onClick={() => navigate(`/calendar/${m._id}`)}
              >
                <span>{renderStartDate(m)}</span>
                <span>{m.name}</span>
              </div>
            ))}
          </div>
        </div>
      )}

      {filteredTasks.length > 0 && (
        <div className="tasks">
          <span className="section-title">Your in-progress tasks</span>
          {filteredTasks.map(task => (
            <div
              key={task._id}
              className="task"
              onClick={() => navigate(`/tasks/${task._id}`)}
            >
              <span>{task.title}</span>
            </div>
          ))}
        </div>
      )}
    </div>
  );
}
