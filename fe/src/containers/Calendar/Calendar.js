import React, { useState, useEffect } from "react";
import moment from "moment";
import { useQuery } from "@apollo/react-hooks";
import { navigate } from "@reach/router";
import { Calendar, Button, Modal, Badge, Select, Popover, Icon } from "antd";
import { COMPANY_MEETINGS } from "../../queries";
import MeetingForm from "../MeetingForm/MeetingForm";
import { MEETING_UPDATED, MEETING_DELETED } from "../../subscriptions";
import DayView from "../../components/DayView/DayView";

import "./Calendar.css";

export default function MyCalendar(props) {
  const { user, selectedCompanyId, formMode, spaces } = props;
  const [selectedUser, selectUser] = useState(user.username);
  const [currDate, setCurrDate] = useState(null);
  const [date, setDate] = useState(moment());

  // const [formMode, setFormMode] = useState("");

  // queries
  const {
    data: companyMeetingsData,
    loading: companyMeetingsLoading,
    subscribeToMore
  } = useQuery(COMPANY_MEETINGS, {
    variables: { companyId: selectedCompanyId, username: user.username }
  });

  const subscribe = () => {
    subscribeToMore({
      document: MEETING_UPDATED,
      variables: { companyId: selectedCompanyId, username: user.username },
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data || !subscriptionData.data.meetingUpdated)
          return prev;
        const meetingUpdated = subscriptionData.data.meetingUpdated;
        const index = prev.companyMeetings.findIndex(
          el => el._id === meetingUpdated._id
        );

        if (index > -1) {
          const updated = [...prev.companyMeetings];
          updated[index] = meetingUpdated;
          return { ...prev, companyMeetings: updated };
        } else {
          return {
            ...prev,
            companyMeetings: [...prev.companyMeetings, meetingUpdated]
          };
        }
      }
    });
    subscribeToMore({
      document: MEETING_DELETED,
      variables: { companyId: selectedCompanyId },
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data || !subscriptionData.data.meetingDeleted)
          return prev;
        const deletedMeeting = subscriptionData.data.meetingDeleted;
        const index = prev.companyMeetings.findIndex(
          m => m._id === deletedMeeting._id
        );

        if (index === -1) return prev;

        const updated = [...prev.companyMeetings];
        updated.splice(index, 1);

        return {
          ...prev,
          companyMeetings: updated
        };
      }
    });
  };

  useEffect(
    () => subscribe(),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  if (companyMeetingsLoading) return "Loading...";

  let meetings =
    (companyMeetingsData && companyMeetingsData.companyMeetings) || [];
  meetings = meetings.filter(
    m => !m.private || !!m.invitations.find(i => i.username === selectedUser)
  );
  const company = user.companies.find(c => c._id === selectedCompanyId);
  const users = [...company.users, company.owner];

  const setFormMode = mode => navigate(`/calendar/${mode}`);

  const renderDay = day => {
    const dayMeetings = meetings.filter(m => {
      const startDate = m.startDate && moment.unix(m.startDate);
      const endDate = m.endDate && moment.unix(m.endDate);

      // range
      if (
        endDate &&
        (day.isBefore(startDate, "day") || day.isAfter(endDate, "day"))
      ) {
        return false;
      } else if (day.isBefore(startDate, "day")) return false;

      if (m.repetition === "Daily") {
        return true;
      } else if (m.repetition === "Weekly") {
        return day.weekday() === startDate.weekday();
      } else if (m.repetition === "Monthly (weekday)") {
        const dayWeek = Math.floor(parseInt(day.format("D"), 10) / 7) + 1;
        const startDayWeek =
          Math.floor(parseInt(startDate.format("D"), 10) / 7) + 1;

        return (
          dayWeek === startDayWeek && day.weekday() === startDate.weekday()
        );
      } else if (m.repetition === "Monthly (day number)") {
        return day.format("D") === startDate.format("D");
      }
      return endDate ? true : day.isSame(startDate, "day");
    });

    if (dayMeetings.length === 0) return null;

    const dayTitle = day.format("dddd, Do of MMMM");

    return (
      <Popover
        className="day-cell"
        title={dayTitle}
        mouseEnterDelay={0.7}
        content={<DayView events={dayMeetings} spaces={spaces} />}
      >
        {dayMeetings.map(m => {
          const invitation = m.invitations.find(
            i => i.username === user.username
          );
          const count = invitation && invitation.state === "pending" ? 1 : 0;

          return (
            <span
              className="meeting-span"
              key={m._id}
              style={{ backgroundColor: m.color }}
              onClick={e => {
                if (m.name === "Private") return;
                // if following lines is not present, layoud breaks on modal
                // close
                document.getElementsByClassName("app-content")[0].focus();
                setFormMode(m._id);
              }}
            >
              <Badge dot count={count} title="Need your response">
                <span className="cell-time">
                  {moment.unix(m.startDate).format("HH:mm")}
                </span>
                <span> {m.name}</span>
              </Badge>
            </span>
          );
        })}
      </Popover>
    );
  };

  return (
    <div className="Calendar">
      <Button
        className="add-meeting-button"
        shape="circle"
        icon="plus"
        type="primary"
        onClick={() => setFormMode("create")}
      />
      <Select
        className="user-select"
        defaultValue={user.username}
        onChange={val => selectUser(val)}
      >
        {users.map(u => (
          <Select.Option key={u._id} value={u.username}>
            {u.username}
          </Select.Option>
        ))}
      </Select>
      <div className="navigation">
        <Icon
          type="left"
          onClick={_ => setDate(moment(date).add(-1, "month"))}
        />
        <span className="month-name">{date.format("MMMM")}</span>
        <Icon
          type="right"
          onClick={_ => setDate(moment(date).add(1, "month"))}
        />
      </div>
      <Modal>
        <DayView />
      </Modal>
      <Modal
        title={formMode === "create" ? "Create meeting" : "Edit meeting"}
        visible={!!formMode}
        onCancel={() => setFormMode("")}
        footer={null}
        // mask={false}
        destroyOnClose
        width={950}
      >
        <MeetingForm
          mode={formMode}
          companyId={selectedCompanyId}
          user={user}
          spaces={spaces}
          afterSubmit={() => setFormMode("")}
          initialData={
            formMode === "create"
              ? {
                  startDate: currDate && currDate.unix(),
                  invitations: [{ state: "accepted", username: user.username }],
                  name: "",
                  description: "",
                  private: false,
                  invitedSpaces: [],
                  companyId: "",
                  repetition: "None"
                }
              : meetings.find(m => m._id === formMode) || {}
          }
        />
      </Modal>
      <Calendar
        dateCellRender={renderDay}
        onSelect={setCurrDate}
        value={date}
        onChange={setDate}
      />
    </div>
  );
}
