import React, { useState, useRef } from "react";
import lodash from "lodash";
import { useMutation } from "@apollo/react-hooks";
import {
  Form,
  Input,
  Upload,
  Button,
  Icon,
  notification,
  Switch,
  Select
} from "antd";
import { apiUrl } from "../..";
import { handleError } from "../../utils";
import { CREATE_SPACE, USER_SPACES, EDIT_SPACE } from "../../queries";
import InputWithList from "../../components/InputWithList/InputWithList";
import StateInput from "../../components/StateInput/StateInput";
import "./SpaceForm.css";
import Loader from "../../components/Loader/Loader";

export function SpaceForm(props) {
  const form = useRef(null);
  const [uploadedLogo, setLogo] = useState(null);

  const { mode = "create " } = props;
  const {
    initialData = {
      name: "",
      description: "",
      logo: null,
      accounts: [],
      private: false,
      states: [
        { text: "Open", color: "blue" },
        { text: "In progress", color: "orange" },
        { text: "Done", color: "green" }
      ],
      frozen: false
    },
    selectedCompanyId,
    user,
    afterSubmit = () => {}
  } = props;

  const [createSpace, { loading: createSpaceLoading }] = useMutation(
    CREATE_SPACE
  );
  const [editSpace, { loading: editSpaceLoading }] = useMutation(EDIT_SPACE);

  if (createSpaceLoading || editSpaceLoading) return <Loader />;

  const onSubmit = mutation => e => {
    const creating = mode === "create";

    e.preventDefault();
    props.form.validateFields(async (err, values) => {
      if (!err) {
        const data = {
          ...initialData,
          ...values,
          states: values.states.map(s => ({
            text: s.text,
            color: s.color
          })),
          logo: values.logo ? values.logo.file.response.Location : ""
        };

        if (creating) {
          data.ownerId = props.user._id;
          data.companyId = data.companyId || selectedCompanyId;
          data.accounts = lodash.uniq([...data.accounts, props.user.username]);
        }

        try {
          await mutation({
            variables: data,
            update: (cache, { data }) => {
              if (creating) {
                const cacheParams = {
                  query: USER_SPACES,
                  variables: {
                    username: props.user.username
                  }
                };
                const { userSpaces } = cache.readQuery(cacheParams);
                cache.writeQuery({
                  ...cacheParams,
                  data: { userSpaces: [...userSpaces, data.createSpace] }
                });
              }
            }
          });

          afterSubmit();

          notification["success"]({
            message: "Success",
            description: `${creating ? "Created" : "Edited"} space.`
          });
        } catch (err) {
          handleError(err);
        }
      }
    });
  };

  const onUploadChange = e => setLogo(e.file);
  const { getFieldDecorator } = props.form;
  const { hideSubmitButton } = props;
  const formItemLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 14 }
  };

  let fileList =
    uploadedLogo !== null
      ? uploadedLogo
      : initialData.logo
      ? {
          url: initialData.logo,
          name: "",
          thumbUrl: initialData.logo,
          uid: initialData.logo
        }
      : null;
  fileList = [fileList].filter(el => !!el);

  const mutation = mode === "create" ? createSpace : editSpace;

  let companyUsers = user.companies.map(c => [
    ...c.users.map(u => u.username)
    // c.owner.username
  ]);
  companyUsers = lodash.flattenDeep(companyUsers);

  const defaultItems = [
    <Form.Item label="Space name" key="name">
      {getFieldDecorator("name", {
        rules: [{ required: true, message: "Name is required." }],
        initialValue: initialData.name
      })(<Input placeholder="Name" />)}
    </Form.Item>,

    <Form.Item label="Description" key="description">
      {getFieldDecorator("description", {
        initialValue: initialData.description
      })(<Input placeholder="Description" />)}
    </Form.Item>,

    <Form.Item label="Logo" key="logo">
      {getFieldDecorator("logo")(
        <Upload
          className="logo-upload"
          onChange={onUploadChange}
          fileList={fileList}
          name="logo"
          action={`${apiUrl}pics`}
          listType="picture"
          onRemove={() => setLogo("")}
        >
          <Button>
            <Icon type="upload" />
            Upload
          </Button>
        </Upload>
      )}
    </Form.Item>,

    <Form.Item label="Accounts" key="accounts">
      {getFieldDecorator("accounts", {
        initialValue: initialData.accounts
      })(
        <InputWithList inputAutoComplete={companyUsers} uniqueProp="username" />
      )}
    </Form.Item>,

    <Form.Item label="Task statuses" key="statuses">
      {getFieldDecorator("states", {
        initialValue: initialData.states
      })(<StateInput />)}
    </Form.Item>,

    <Form.Item label="Private" key="private">
      {getFieldDecorator("private", {
        initialValue: initialData.private,
        valuePropName: "checked"
      })(<Switch unCheckedChildren="public" checkedChildren="private" />)}
    </Form.Item>,

    props.user.companies.length > 1 && (
      <Form.Item label="Company" key="company">
        {getFieldDecorator("companyId", {
          initialValue: initialData.companyId || selectedCompanyId
        })(
          <Select>
            {props.user.companies.map(c => (
              <Select.Option key={c._id} value={c._id}>
                {c.name}
              </Select.Option>
            ))}
          </Select>
        )}
      </Form.Item>
    )
  ];

  const limitedItems = [
    <Form.Item label="Statuses" key="statuses">
      {getFieldDecorator("states", {
        initialValue: initialData.states
      })(<StateInput />)}
    </Form.Item>
  ];

  const formItems = initialData.frozen ? limitedItems : defaultItems;

  return (
    <div className="SpaceForm">
      <Form
        ref={form}
        labelAlign="left"
        {...formItemLayout}
        onSubmit={onSubmit(mutation)}
      >
        {formItems}

        {!hideSubmitButton && (
          <Form.Item wrapperCol={{ span: 12, offset: 6 }}>
            <Button type="primary" htmlType="submit" className="submit-button">
              Submit
            </Button>
          </Form.Item>
        )}
      </Form>
    </div>
  );
}

export default Form.create()(SpaceForm);
