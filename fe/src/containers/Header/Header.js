import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import {
  Icon,
  Tooltip,
  Menu,
  Select,
  Avatar,
  Badge,
  Popover,
  Button,
  Empty,
  Dropdown,
  Modal
} from "antd";
import { Link, navigate } from "@reach/router";
import { apiUrl } from "../../index";
import { MARK_SEEN, NOTIFICATIONS, MARK_SEEN_MANY } from "../../queries";
import untitled from "../../logo.svg";
import "./Header.css";
import { idToDate } from "../../utils";
import PremiumSignin from "../../components/PremiumSignin/PremiumSignin";

// static propTypes = {
//   user: PropTypes.object.isRequired,
//   selectCompany: PropTypes.func,
//   selectedCompany: PropTypes.string,
//   selectSpace: PropTypes.func,
//   selectedSpace: PropTypes.string
// };

export default function Header(props) {
  const {
    user = {},
    selectCompany = () => {},
    selectedCompanyId = "",
    selectSpace = () => {},
    selectedSpaceId = "",
    notifications = []
  } = props;

  const [modalVisible, setModalVisible] = useState(false);

  const logout = () => {
    fetch(`${apiUrl}logout`, { method: "POST", credentials: "include" })
      .then(() => {
        navigate("/login");
      })
      .catch(err => console.error(err));
  };

  const [markSeen] = useMutation(MARK_SEEN);
  const [markSeenMany] = useMutation(MARK_SEEN_MANY);

  const spaces = props.spaces.filter(
    el => !el.companyId || el.companyId === selectedCompanyId
  );

  const withoutMessages = notifications.filter(
    n => !!!n.quiet && !!!n.link.match(/\/messages\//)
  );

  const renderNotifications = () => {
    if (withoutMessages.length === 0) return <Empty />;

    const onNClick = n => async e => {
      if (!n.link) e.preventDefault();

      await markSeen({
        variables: { _id: n._id },
        update: cache => {
          const params = {
            query: NOTIFICATIONS,
            variables: { username: user.username }
          };
          const { notifications } = cache.readQuery(params);
          const updated = [...notifications];
          updated.find(el => el._id === n._id).seen = true;
          cache.writeQuery({ ...params, data: { notifications: updated } });
        }
      });
    };

    return (
      <div className="notifications-container">
        {withoutMessages.map(n => (
          <Link to={n.link} key={n._id} onClick={onNClick(n)}>
            <div className="notification">
              <span
                className="notification-title"
                style={{ fontWeight: n.seen ? "normal" : "bold" }}
              >
                <span className="title">{n.title}</span>
                <span className="time">{idToDate(n._id).fromNow()}</span>
              </span>
              <span
                className="notification-text"
                style={{ fontWeight: n.seen ? "normal" : "bold" }}
              >
                {n.text}
              </span>
            </div>
          </Link>
        ))}
      </div>
    );
  };

  const markAllSeenHandler = async () => {
    const ids = withoutMessages.map(n => n._id);
    await markSeenMany({
      variables: { ids },
      update: cache => {
        const params = {
          query: NOTIFICATIONS,
          variables: { username: user.username }
        };
        const { notifications } = cache.readQuery(params);
        const updated = notifications.map(n =>
          ids.includes(n._id) ? { ...n, seen: true } : n
        );
        cache.writeQuery({ ...params, data: { notifications: updated } });
      }
    });
  };

  const contact = () => {
    window.open("mailto:help@eazydone.com", "_self");
  };

  const userMenu = (
    <Menu>
      <Menu.Item onClick={() => navigate("/edit-profile")}>
        <Icon type="user" style={{ marginRight: 5 }} />
        Edit profile
      </Menu.Item>
      {!!!user.stripeSubscriptionId && user.accountType === "OWNER" && (
        <Menu.Item onClick={_ => setModalVisible(true)}>
          <Icon type="thunderbolt" style={{ marginRight: 5 }} />
          Upgrade your plan
        </Menu.Item>
      )}
      <Menu.Item onClick={contact}>
        <Icon type="rocket" style={{ marginRight: 5 }} />
        Report bug / Request feature
      </Menu.Item>
      <Menu.Item onClick={logout}>
        <Icon type="logout" style={{ marginRight: 5 }} />
        Logout
      </Menu.Item>
    </Menu>
  );

  const currentMenuKey = window.location.pathname.split("/")[1];

  return (
    <header className="App-header">
      <Modal
        visible={modalVisible}
        destroyOnClose
        onCancel={_ => setModalVisible(false)}
        title={
          <span>
            <Icon type="trophy" style={{ marginRight: 5 }} />
            Upgrade your plan
          </span>
        }
        footer={null}
      >
        <div style={{ position: "relative" }}>
          <PremiumSignin user={user} />
        </div>
      </Modal>
      <Link to="/">
        <img src={untitled} className="App-logo" alt="logo" />
      </Link>
      <div className="menu" tabIndex={-1}>
        <Menu mode="horizontal" selectedKeys={[currentMenuKey]} theme="dark">
          {user.accountType === "OWNER" && (
            <Menu.Item key="companies">
              <Link to="/companies">Companies</Link>
            </Menu.Item>
          )}
          <Menu.Item key="spaces">
            <Link to="/spaces">Spaces</Link>
          </Menu.Item>
          <Menu.Item key="tasks">
            <Link to="tasks/list">Manage workload</Link>
          </Menu.Item>
          <Menu.Item key="calendar">
            <Link to="calendar">Calendar</Link>
          </Menu.Item>
          <Menu.Item key="messages">
            <Link to="messages">
              <Badge
                count={
                  notifications.filter(
                    n => !n.seen && n.link.match(/\/messages\//)
                  ).length
                }
              >
                Messages
              </Badge>
            </Link>
          </Menu.Item>
          <Menu.Item key="files">
            <Link to="files">Files</Link>
          </Menu.Item>
        </Menu>
      </div>
      {user.companies.length > 1 && (
        <Tooltip title="Select company" mouseEnterDelay={1}>
          <Select
            className="companies-select"
            placeholder="Select company"
            onChange={value => selectCompany(value)}
            defaultValue={selectedCompanyId}
          >
            {user.companies.map(c => (
              <Select.Option key={c._id} value={c._id}>
                {c.name}
              </Select.Option>
            ))}
          </Select>
        </Tooltip>
      )}
      <Tooltip title="Select space" mouseEnterDelay={1}>
        <Select
          className="space-select"
          placeholder="Select space"
          onChange={value => selectSpace(value)}
          // defaultValue={selectedSpaceId}
          value={selectedSpaceId}
        >
          {spaces.map(s => (
            <Select.Option key={s._id} value={s._id}>
              {s.name}
            </Select.Option>
          ))}
        </Select>
      </Tooltip>
      <Popover
        className="notifications-icon"
        title={
          <span>
            Notifications{" "}
            <Button
              className="mark-all-seen-button"
              size="small"
              type="ghost"
              onClick={markAllSeenHandler}
            >
              Mark all as seen
            </Button>
          </span>
        }
        trigger="click"
        content={renderNotifications()}
      >
        <Badge count={withoutMessages.filter(n => !n.seen).length}>
          <div className="notifications-icon-wrapper">
            <Icon type="bell" />
          </div>
        </Badge>
      </Popover>
      <Dropdown overlay={userMenu}>
        <div className="logout-menu">
          {!user.avatar && <Icon type="user" className="user-icon" />}
          {user.avatar && (
            <Avatar src={user.avatar} style={{ marginRight: 5 }} />
          )}
          {user.username || user.email || "guest"}
        </div>
      </Dropdown>
    </header>
  );
}
