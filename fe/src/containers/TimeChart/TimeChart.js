import React, { useState } from "react";
import moment from "moment";
import { useQuery } from "@apollo/react-hooks";
import { ResponsiveLine } from "@nivo/line";
import { Empty, Select } from "antd";
import { SPACE_MILESTONES, TIME_LOGS } from "../../queries";
import { idToDate } from "../../utils";

import "./TimeChart.css";

export default function TimeChart(props) {
  const { selectedSpaceId, tasks } = props;
  const [mId, setMId] = useState("");

  const {
    data: spaceMilestonesData,
    loading: spaceMilestonesLoading
  } = useQuery(SPACE_MILESTONES, { variables: { spaceId: selectedSpaceId } });

  let milestones =
    (spaceMilestonesData && spaceMilestonesData.spaceMilestones) || [];

  const { data: timeLogsData, loading: timeLogLoading } = useQuery(TIME_LOGS, {
    variables: { tasksIds: tasks.map(t => t._id) }
  });

  if (spaceMilestonesLoading || timeLogLoading) return "Loading...";

  if (milestones.length === 0) return <Empty />;

  const selectedMilestoneId = mId || milestones[0]._id;

  const timeLogs = ((timeLogsData && timeLogsData.timeLogs) || []).filter(
    tL =>
      (tasks.find(t => t._id === tL.taskId) || {}).milestoneId ===
      selectedMilestoneId
  );

  const milestone = milestones.find(m => m._id === selectedMilestoneId);
  const startDate =
    (milestone.startDate && moment.unix(milestone.startDate)) ||
    idToDate(milestone._id);

  let data = timeLogs.map(tL => ({
    x: idToDate(tL._id).diff(startDate, "days") + 1,
    y: tL.minutes
  }));

  if (data.length > 0) {
    data = data.reduce(
      (acc, curr) => {
        if (curr.x !== acc[acc.length - 1].x) {
          return [...acc, curr];
        }
        acc[acc.length - 1].y += curr.y;
        return acc;
      },
      [data[0]]
    );

    for (let i = 1; i < data.length; i++) {
      data[i].y += data[i - 1].y;
    }
  }

  const series = [
    {
      id: milestones.find(m => m._id === selectedMilestoneId).name,
      data
    }
  ];

  const isEmpty = data.length === 0;

  return (
    <div className="TimeChart">
      <Select
        value={selectedMilestoneId}
        className="milestone-select"
        onChange={setMId}
      >
        {milestones.map(m => (
          <Select.Option key={m._id} value={m._id}>
            {m.name}
          </Select.Option>
        ))}
      </Select>
      {!isEmpty && (
        <ResponsiveLine
          enableArea
          enableCrosshair
          data={series}
          margin={{ top: 50, right: 110, bottom: 50, left: 60 }}
          xScale={{ type: "point" }}
          yScale={{ type: "linear", stacked: true, min: 0, max: "auto" }}
          axisTop={null}
          axisRight={null}
          axisBottom={{
            orient: "bottom",
            tickSize: 5,
            tickPadding: 5,
            tickRotation: 0,
            legend: "milestone duration (days)",
            legendOffset: 36,
            legendPosition: "middle"
          }}
          axisLeft={{
            orient: "left",
            tickSize: 5,
            tickPadding: 5,
            tickRotation: 0,
            legend: "minutes spent",
            legendOffset: -40,
            legendPosition: "middle"
          }}
          colors={{ scheme: "nivo" }}
          pointSize={10}
          pointColor={{ from: "color" }}
          pointBorderWidth={2}
          pointBorderColor={{ from: "serieColor" }}
          pointLabel="y"
          pointLabelYOffset={-12}
          useMesh={true}
        />
      )}
      {isEmpty && <Empty />}
    </div>
  );
}
