import React, { useState } from "react";
import moment from "moment";
import { uniqBy } from "lodash";
import { useQuery } from "@apollo/react-hooks";
import { ResponsiveLine } from "@nivo/line";
import { idToDate } from "../../utils";
import { SPACE_MILESTONES, PROGRESS } from "../../queries";
import { Empty, Select } from "antd";
import Loader from "../../components/Loader/Loader";

import "./BurnChart.css";

export default function BurnChart(props) {
  const { selectedSpaceId } = props;
  const [mId, setMId] = useState("");

  const {
    data: spaceMilestonesData,
    loading: spaceMilestonesLoading
  } = useQuery(SPACE_MILESTONES, { variables: { spaceId: selectedSpaceId } });

  const milestones =
    (spaceMilestonesData && spaceMilestonesData.spaceMilestones) || [];

  const { data: progressData, loading: progressLoading } = useQuery(PROGRESS, {
    variables: { milestoneIds: milestones.map(m => m._id) || [] }
  });

  const progress = progressData && progressData.progress;

  if (milestones.length === 0) return <Empty />;

  const selectedMilestoneId = mId || milestones[0]._id;

  if (spaceMilestonesLoading || progressLoading) return <Loader />;

  const milestone = milestones.find(m => m._id === selectedMilestoneId);
  const mProgress = progress.filter(p => p.milestoneId === milestone._id);

  let startDate = null;
  if (milestone.startDate) {
    startDate = moment.unix(milestone.startDate);
  } else {
    startDate = idToDate(milestone._id);
  }

  let data = uniqBy(
    mProgress.map(p => ({
      x: idToDate(p._id).diff(startDate, "days") + 1,
      y: p.progress
    })),
    "x"
  );

  if (data.length > 0) {
    data = data.reduce(
      (acc, curr) => (curr.y !== acc[acc.length - 1].y ? [...acc, curr] : acc),
      [data[0]]
    );
  }

  const series = [
    {
      id: milestone.name,
      data
    }
  ];

  const isEmpty = data.length === 0 && series.every(s => s.data.length === 0);

  return (
    <div className="BurnChart">
      <Select
        className="milestone-select"
        value={selectedMilestoneId}
        onChange={setMId}
      >
        {milestones.map(m => (
          <Select.Option key={m._id} value={m._id}>
            {m.name}
          </Select.Option>
        ))}
      </Select>
      {!isEmpty && (
        <ResponsiveLine
          enableArea
          enableCrosshair
          data={series}
          margin={{ top: 50, right: 110, bottom: 50, left: 60 }}
          xScale={{ type: "point" }}
          yScale={{ type: "linear", stacked: true, min: 0, max: "auto" }}
          axisTop={null}
          axisRight={null}
          axisBottom={{
            orient: "bottom",
            tickSize: 5,
            tickPadding: 5,
            tickRotation: 0,
            legend: "milestone duration (days)",
            legendOffset: 36,
            legendPosition: "middle"
          }}
          axisLeft={{
            orient: "left",
            tickSize: 5,
            tickPadding: 5,
            tickRotation: 0,
            legend: "progress",
            legendOffset: -40,
            legendPosition: "middle"
          }}
          colors={{ scheme: "nivo" }}
          pointSize={10}
          pointColor={{ from: "color" }}
          pointBorderWidth={2}
          pointBorderColor={{ from: "serieColor" }}
          pointLabel="y"
          pointLabelYOffset={-12}
          useMesh={true}
        />
      )}
      {isEmpty && <Empty style={{ margin: 50 }} />}
    </div>
  );
}
