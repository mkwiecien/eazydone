import React, { useState, useEffect } from "react";
import lodash from "lodash";
import { Router, navigate } from "@reach/router";
import { useQuery, useMutation, useSubscription } from "@apollo/react-hooks";
import { notification, Avatar, Icon, Modal } from "antd";
import Header from "../Header/Header";
import CompanyForm from "../CompanyForm/CompanyForm";
import {
  USER,
  USER_SPACES,
  NOTIFICATIONS,
  TASKS,
  COMPANY_MEETINGS,
  EDIT_USER,
  SPACES_MILESTONES,
  CHECK_IN,
  FILES,
  TAGS,
  CHAT
} from "../../queries";
import "./App.css";
import Objection from "../Objection/Objection";
import Companies from "../Companies/Companies";
import FirstLogin from "../FirstLogin/FirstLogin";
import Spaces from "../Spaces/Spaces";
import Tasks from "../Tasks/Tasks";
import Calendar from "../Calendar/Calendar";
import Messages from "../Messages/Messages";
import {
  ADDED_MESSAGE,
  NEW_NOTIFICATION,
  TASK_DELETED,
  TASK_UPDATED,
  MEETING_DELETED,
  MEETING_UPDATED,
  TYPING,
  CHECKED_IN,
  MILESTONE_DELETED,
  TASKS_UPDATED
} from "../../subscriptions";
import EditProfile from "../EditProfile/EditProfile";
import Home from "../Home/Home";
import Loader from "../../components/Loader/Loader";
import Files from "../Files/Files";
import { notify, isMongoId } from "../../utils";

const AppWrapper = function(props) {
  if (Object.keys(props.user).length === 0) return <Loader size={50} />;
  return <App {...props} />;
};

function App(props) {
  const [selectedCompanyId, selectCompany] = useState("");
  // const [selectedSpaceId, selectSpace] = useState("");
  const [menuCollapsed, setMenuCollapsed] = useState(false);
  const [whoIsTyping, setTyping] = useState([]);
  const [demoConfirmed, setDemoConfirmed] = useState(false);

  const getObjection = user => {
    if (user.accountType === "USER" && !user.active) {
      return {
        msg: "This is your first login. Please fill the form below.",
        renderContent: () => <FirstLogin user={user} />
      };
    } else if (props["*"] === "edit-profile") {
      return null;
    } else if (user.accountType === "OWNER" && user.companies.length === 0) {
      return {
        msg: "You need to add at least one company",
        renderContent: () => <CompanyForm user={user} mode="create" />
      };
    } else if (user.companies.length === 0) {
      return {
        msg: `
          You were removed from your last company and you are not assigned
          to any. Please contact company administator.
        `,
        renderContent: () => <span />
      };
    }
    return null;
  };

  const loginUser = props.user;

  const variables = {
    ...(loginUser.username && { username: loginUser.username }),
    ...(loginUser.email && { email: loginUser.email })
  };

  const [checkInMutation] = useMutation(CHECK_IN);

  const checkIn = async username => {
    await checkInMutation({ variables: { username } });
  };

  useEffect(
    () => {
      setInterval(() => checkIn(loginUser.username), 20 * 1000);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  let {
    loading: userLoading,
    data: userData,
    subscribeToMore: subscribeToMoreCheckIns
  } = useQuery(USER, {
    variables
    // fetchPolicy: "cache-and-network"
  });

  userData = userData || {};

  const selectedCompanyForNow =
    selectedCompanyId ||
    (userData.user &&
      userData.user.companies &&
      userData.user.companies[0] &&
      userData.user.companies[0]._id) ||
    "";

  let selectedSpaceId =
    (userData.user && userData.user.lastSelectedSpaceId) ||
    loginUser.lastSelectedSpaceId;

  const [editUser] = useMutation(EDIT_USER);

  const selectSpace = async spaceId => {
    await editUser({
      variables: { ...variables, lastSelectedSpaceId: spaceId }
    });
  };

  const { loading: userSpacesLoading, data: userSpacesData } = useQuery(
    USER_SPACES,
    {
      variables: { username: loginUser.username },
      skip:
        !userData.user ||
        !userData.user.companies ||
        userData.user.companies.length === 0
    }
  );

  let spaces = (userSpacesData || {}).userSpaces || [];
  spaces = spaces
    .map(s => {
      let name = s.name;

      if (name !== "Personal") {
        const company = userData.user.companies.find(
          c => c._id === s.companyId
        );
        if (!company) return null; // company was removed (it's a bug to fix)
        if (name === "Company") name = `${name} (${company.name})`;
      }

      return {
        ...s,
        name:
          s.name === "Company"
            ? `${s.name} (${
                userData.user.companies.find(c => c._id === s.companyId).name
              })`
            : s.name
      };
    })
    .filter(el => !!el);

  const {
    data: milestonesData,
    loading: milestonesLoading,
    subscribeToMore: subscribeToMoreMilestones
  } = useQuery(SPACES_MILESTONES, {
    variables: { spaceIds: spaces.map(s => s._id) },
    skip: spaces.length === 0
  });

  const milestones = (milestonesData && milestonesData.spacesMilestones) || [];

  useEffect(() => {
    if (spaces.length === 0) return;
    subscribeToMoreMilestones({
      document: MILESTONE_DELETED,
      variables: { spacesIds: spaces.map(s => s._id) },
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data || !subscriptionData.data.milestoneDeleted)
          return prev;
        const milestoneDeleted = subscriptionData.data.milestoneDeleted;
        const index = prev.spacesMilestones.findIndex(
          m => m._id === milestoneDeleted._id
        );
        if (index === -1) return prev;
        const updated = [...prev.spacesMilestones];
        updated.splice(index, 1);
        return {
          ...prev,
          spacesMilestones: updated
        };
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [spaces.length]);

  const { data: tagsData, loading: tagsLoading } = useQuery(TAGS, {
    variables: { spacesIds: spaces.map(s => s._id) },
    skip: spaces.length === 0
  });

  const handleNewMessage = ({
    client,
    subscriptionData: {
      data: { addedMessage }
    }
  }) => {
    let onClick = _ => {};
    let fromName = "";
    let fromAvatar = "";

    const mentionsRe = new RegExp(`@(${loginUser.username})`, "gm");
    const match = addedMessage.text.match(mentionsRe);

    // to space
    if (isMongoId(addedMessage.to)) {
      if (match) {
        onClick = navigateToChat(addedMessage.to);
        const fromUser = userData.user.companies
          .find(c => c._id === selectedCompanyForNow)
          .users.find(u => u.username === addedMessage.from);
        fromAvatar = fromUser.avatar;
        fromName = fromUser.username;
      }
    } else {
      // to user
      onClick = navigateToChat(addedMessage.from);
      const fromUser = userData.user.companies
        .find(c => c._id === selectedCompanyForNow)
        .users.find(u => u.username === addedMessage.from);
      fromAvatar = fromUser.avatar;
      fromName = fromUser.username;
    }

    if (fromName && !document.hasFocus()) {
      notify(
        fromName,
        addedMessage.text.length > 100
          ? addedMessage.text.slice(0, 100) + "..."
          : addedMessage.text,
        onClick
      );
    } else if (
      fromName &&
      window.location.href.indexOf(addedMessage.from) === -1 &&
      window.location.href.indexOf(addedMessage.to) === -1
    ) {
      const message = (
        <span>
          <Avatar style={{ marginRight: 5 }} src={fromAvatar}>
            {fromName.slice(0, 2)}
          </Avatar>
          {addedMessage.from}
        </span>
      );

      notification.open({
        message,
        description:
          addedMessage.text.length > 200
            ? addedMessage.text.slice(0, 200) + "..."
            : addedMessage.text,
        onClick,
        style: { cursor: "pointer" }
      });
    }

    const params = { from: "", to: "", spaceId: "", offset: 0 };
    if (isMongoId(addedMessage.to)) {
      params.spaceId = addedMessage.to;
    } else {
      params.from = loginUser.username;
      params.to = addedMessage.from;
    }

    try {
      const query = client.readQuery({ query: CHAT, variables: params });
      if (!query.chat) return;
      const chat = query.chat;
      client.writeQuery({
        query: CHAT,
        variables: params,
        data: { chat: [...chat, addedMessage] }
      });
    } catch (err) {
      return;
    }
  };

  useSubscription(ADDED_MESSAGE, {
    variables: { username: loginUser.username, spaces: spaces.map(s => s._id) },
    onSubscriptionData: handleNewMessage
  });

  const navigateToChat = chat => () => navigate(`/messages/${chat}`);

  const clearTyping = lodash.debounce(username => {
    let updated = [...whoIsTyping];
    updated.splice(
      updated.findIndex(el => el.username === username),
      1
    );
    setTyping(updated);
  }, 2000);

  const handleTyping = ({
    subscriptionData: {
      data: { typing }
    }
  }) => {
    if (
      !!!whoIsTyping.find(
        t => t.username === typing.username && t.to === typing.to
      )
    ) {
      setTyping([...whoIsTyping, typing]);
    }
    clearTyping(typing.username);
  };

  useSubscription(TYPING, {
    variables: { username: loginUser.username, spaces: spaces.map(s => s._id) },
    onSubscriptionData: handleTyping
  });

  // tasks
  const {
    data: tasksData,
    loading: tasksLoading,
    subscribeToMore: subscribeToMoreTasks
  } = useQuery(TASKS, {
    variables: {
      spacesIds: spaces.map(s => s._id)
    },
    skip: spaces.length === 0
  });

  useEffect(
    () => {
      if (spaces.length === 0) return;
      subscribeToMoreTasks({
        document: TASK_UPDATED,
        variables: {
          spacesIds: spaces.map(s => s._id),
          username: loginUser.username
        },
        updateQuery: (prev, { subscriptionData }) => {
          if (!subscriptionData.data || !subscriptionData.data.taskUpdated)
            return prev;

          const taskUpdated = subscriptionData.data.taskUpdated;
          const index = prev.tasks.findIndex(el => el._id === taskUpdated._id);

          if (index > -1) {
            const updated = [...prev.tasks];
            updated[index] = taskUpdated;
            return { ...prev, tasks: updated };
          } else {
            return {
              ...prev,
              tasks: [...prev.tasks, taskUpdated].sort(
                (a, b) => a.order - b.order
              )
            };
          }
        }
      });
      subscribeToMoreTasks({
        document: TASKS_UPDATED,
        variables: { spacesIds: spaces.map(s => s._id) },
        updateQuery: (prev, { subscriptionData }) => {
          if (!subscriptionData.data || !subscriptionData.data.tasksUpdated)
            return prev;

          const tasksUpdated = subscriptionData.data.tasksUpdated;
          const updated = [...prev.tasks];

          tasksUpdated.forEach(task => {
            const index = updated.findIndex(el => el._id === task._id);
            if (index === -1) return;

            updated[index] = {
              ...updated[index],
              ...task
            };
          });

          return { ...prev, tasks: updated };
        }
      });
      subscribeToMoreTasks({
        document: TASK_DELETED,
        variables: { spacesIds: spaces.map(s => s._id) },
        updateQuery: (prev, { subscriptionData }) => {
          if (!subscriptionData.data || !subscriptionData.data.taskDeleted)
            return prev;
          const deletedTask = subscriptionData.data.taskDeleted;
          const index = prev.tasks.findIndex(m => m._id === deletedTask._id);

          if (index === -1) return prev;

          const updated = [...prev.tasks];
          updated.splice(index, 1);

          return {
            ...prev,
            tasks: updated
          };
        }
      });
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [spaces.length]
  );

  const {
    data: notificationsData,
    subscribeToMore: subscribeToMoreNotifications
  } = useQuery(NOTIFICATIONS, {
    variables: { username: loginUser.username }
  });

  const notifications =
    (notificationsData && notificationsData.notifications) || [];

  useEffect(
    () => {
      subscribeToMoreNotifications({
        document: NEW_NOTIFICATION,
        variables: { username: loginUser.username },
        updateQuery: (prev, { subscriptionData }) => {
          if (!subscriptionData.data || !subscriptionData.data.newNotification)
            return prev;
          const newNotification = subscriptionData.data.newNotification;

          if (newNotification.important) {
            notify(newNotification.title, newNotification.text, () =>
              navigate(newNotification.link)
            );
          }

          return Object.assign({}, prev, {
            notifications: [newNotification, ...prev.notifications]
          });
        }
      });
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const {
    data: companyMeetingsData,
    loading: companyMeetingsLoading,
    subscribeToMore: subscribeToMoreMeetings
  } = useQuery(COMPANY_MEETINGS, {
    variables: {
      companyId: selectedCompanyForNow,
      username: loginUser.username
    }
  });

  useEffect(
    () => {
      if (!selectedCompanyId && !userData.user) return;
      const companyId = selectedCompanyForNow;
      subscribeToMoreMeetings({
        document: MEETING_UPDATED,
        variables: {
          companyId,
          username: loginUser.username
        },
        updateQuery: (prev, { subscriptionData }) => {
          if (!subscriptionData.data || !subscriptionData.data.meetingUpdated)
            return prev;

          const meetingUpdated = subscriptionData.data.meetingUpdated;
          const index = prev.companyMeetings.findIndex(
            el => el._id === meetingUpdated._id
          );

          if (index > -1) {
            const updated = [...prev.companyMeetings];
            updated[index] = meetingUpdated;
            return { ...prev, companyMeetings: updated };
          } else {
            return {
              ...prev,
              meetings: [...(prev.companyMeetings || []), meetingUpdated]
            };
          }
        }
      });
      subscribeToMoreMeetings({
        document: MEETING_DELETED,
        variables: { companyId },
        updateQuery: (prev, { subscriptionData }) => {
          if (!subscriptionData.data || !subscriptionData.data.meetingDeleted)
            return prev;
          const deletedMeeting = subscriptionData.data.meetingDeleted;
          const index = prev.companyMeetings.findIndex(
            m => m._id === deletedMeeting._id
          );

          if (index === -1) return prev;

          const updated = [...prev.companyMeetings];
          updated.splice(index, 1);

          return {
            ...prev,
            companyMeetings: updated
          };
        }
      });

      subscribeToMoreCheckIns({
        document: CHECKED_IN,
        variables: { companyId, username: loginUser.username },
        updateQuery: (prev, { subscriptionData }) => {
          const checkedIn = subscriptionData.data.checkedIn;
          const user = { ...prev.user };
          const company = user.companies.find(c => c._id === companyId);
          const companyUser = company.users.find(
            u => u.username === checkedIn.username
          );

          companyUser.lastSeen = checkedIn.lastSeen;

          return { user };
        }
      });
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [selectedCompanyId, userData.user]
  );

  const {
    data: filesData,
    loading: filesLoading,
    client,
    refetch: refetchFiles
  } = useQuery(FILES, {
    variables: { spacesIds: spaces.map(s => s._id) },
    skip: spaces.length === 0
  });

  const files = ((filesData && filesData.files) || []).filter(
    f => f.spaceId === selectedSpaceId
  );

  if (
    userLoading ||
    userSpacesLoading ||
    tasksLoading ||
    companyMeetingsLoading ||
    milestonesLoading ||
    tagsLoading
  )
    return <Loader size={50} />;

  if (selectedSpaceId && !!!spaces.find(s => s._id === selectedSpaceId)) {
    const newSpaceId = (spaces[0] || {})._id;
    selectedSpaceId = newSpaceId;
    selectSpace(newSpaceId);
  }

  const meetings = companyMeetingsData.companyMeetings;
  const user = userData.user;
  const objection = getObjection(user);
  const space = selectedSpaceId || (spaces[0] || {})._id;
  const tasks = tasksData && tasksData.tasks;
  const tags = (tagsData && tagsData.tags) || [];

  const selectedSpace = spaces.find(s => s._id === space);
  let company =
    selectedCompanyId ||
    (user.companies.find(c => c._id === selectedSpace.companyId) || {})._id ||
    (user.companies && user.companies.length > 0 && user.companies[0]._id);

  const messageNotifications = notifications.filter(
    n => !n.seen && !!n.link.match(/\/messages\//)
  );

  const tasksNotifications = notifications.filter(
    n => !n.seen && !!n.link.match(/\/tasks\//)
  );

  const showDemoModal = user.isDemo && !demoConfirmed;

  const onCompanySelect = async companyId => {
    const space = spaces.find(s => s.companyId === companyId);
    selectCompany(companyId);
    selectSpace(space._id);
  };

  return (
    <div className="App">
      <Modal
        onOk={_ => setDemoConfirmed(true)}
        visible={showDemoModal}
        title={
          <span>
            <Icon type="warning" style={{ color: "orange", marginRight: 10 }} />
            This is a demo account
          </span>
        }
        cancelButtonProps={{ style: { display: "none" } }}
        closable={false}
      >
        <p>
          Please keep in mind that you are using a shared demo account - this
          means other people can access it as well. Please do not upload any
          sensitive data here.
        </p>
        <p>
          Any information stored in this account can be removed without your
          permission.
        </p>
      </Modal>
      <Header
        user={user}
        spaces={spaces}
        selectCompany={onCompanySelect}
        selectedCompanyId={company}
        selectSpace={selectSpace}
        selectedSpaceId={space}
        notifications={notifications}
      />
      {objection && <Objection {...objection} />}
      {!objection && (
        <Router className="app-content">
          <Home
            path="/"
            tasks={tasks}
            meetings={meetings}
            user={user}
            spaces={spaces}
          />
          <EditProfile path="/edit-profile" user={user} />
          <Tasks
            path="tasks/*"
            user={user}
            selectedSpaceId={space}
            selectedCompanyId={company}
            tasks={tasks}
            spaces={spaces}
            menuCollapsed={menuCollapsed}
            toggleMenuCollapsed={() => setMenuCollapsed(!menuCollapsed)}
            selectSpace={selectSpace}
            milestones={milestones}
            notifications={tasksNotifications}
            files={files}
            client={client}
            refetchFiles={refetchFiles}
            tags={tags}
          />
          <Companies path="companies" user={user} />
          <Spaces
            path="spaces"
            selectedCompanyId={company}
            selectedSpaceId={space}
            selectSpace={selectSpace}
            user={user}
            spaces={spaces}
          />
          <Calendar
            path="calendar"
            selectedCompanyId={company}
            user={user}
            spaces={spaces}
            meetings={meetings}
          />
          <Calendar
            path="calendar/:formMode"
            selectedCompanyId={company}
            user={user}
            spaces={spaces}
            meetings={meetings}
          />
          <Messages
            path="messages"
            user={user}
            notifications={messageNotifications}
            spaces={spaces}
            whoIsTyping={whoIsTyping}
          />
          <Messages
            path="messages/:chatId"
            user={user}
            spaces={spaces}
            notifications={messageNotifications}
            whoIsTyping={whoIsTyping}
          />
          <Files
            path="files"
            user={user}
            spaces={spaces}
            selectedCompanyId={company}
            selectedSpaceId={space}
            loading={filesLoading}
            files={files}
            client={client}
            refetch={refetchFiles}
          />
        </Router>
      )}
    </div>
  );
}

export default AppWrapper;
