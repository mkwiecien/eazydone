import React, { useState, useRef, useEffect } from "react";
import { useMutation } from "@apollo/react-hooks";
import moment from "moment";
import {
  Form,
  Input,
  Button,
  DatePicker,
  Switch,
  Radio,
  Select,
  Tag,
  Badge,
  message,
  Tooltip,
  Icon,
  Empty
} from "antd";
import {
  CREATE_MEETING,
  EDIT_MEETING,
  COMPANY_MEETINGS,
  DELETE_MEETING
} from "../../queries";
import InputWithList from "../../components/InputWithList/InputWithList";
import { handleError } from "../../utils";

function MeetingForm(props) {
  const submitRef = useRef(null);
  const {
    form: { getFieldDecorator },
    companyId,
    user,
    initialData,
    mode = "create",
    afterSubmit = () => {},
    spaces: spacesFromProps,
    meetings = []
  } = props;
  const [spaceFilter, setSpaceFilter] = useState("");

  useEffect(() => {
    if (initialData.name) {
      document.title = initialData.name;
    }
  }, [initialData]);

  const company = user.companies.find(c => c._id === companyId);
  const companyUsers = [
    ...company.users.map(u => u.username)
    // company.owner.username
  ];

  const [createMeeting, { loading: createMeetingLoading }] = useMutation(
    CREATE_MEETING
  );
  const [editMeeting, { loading: editMeetingLoading }] = useMutation(
    EDIT_MEETING
  );
  const [deleteMeeting, { loading: deleteMeetingLoading }] = useMutation(
    DELETE_MEETING
  );

  if (createMeetingLoading || editMeetingLoading || deleteMeetingLoading)
    return "Loading";

  const spaces = spacesFromProps.filter(
    s => s.name.toLowerCase().indexOf(spaceFilter.toLowerCase()) > -1
  );

  const mutation = mode === "create" ? createMeeting : editMeeting;

  // remember to add companyId
  async function onSubmit(event) {
    event.preventDefault();

    props.form.validateFields(async (err, values) => {
      if (err) return;

      const variables = {
        ...values,
        startDate: values.startDate && values.startDate.unix(),
        endDate: (values.endDate && values.endDate.unix()) || null,
        invitations: values.invitations.map(i => ({
          username: i.username,
          state: i.state
        }))
      };

      if (mode === "create") {
        variables.companyId = companyId;
        variables.color =
          "hsl(" +
          360 * Math.random() +
          "," +
          (25 + 70 * Math.random()) +
          "%," +
          (85 + 10 * Math.random()) +
          "%)";
        variables.createdBy = user.username;
      } else {
        variables._id = mode;
        variables.updatedBy = user.username;
      }

      try {
        await mutation({
          variables
          // update: (cache, { data }) => {
          //   if (mode === "create") {
          //     const params = {
          //       query: COMPANY_MEETINGS,
          //       variables: { username: user.username, companyId }
          //     };
          //     const { companyMeetings } = cache.readQuery(params);
          //     cache.writeQuery({
          //       ...params,
          //       data: {
          //         companyMeetings: [...companyMeetings, data.createMeeting]
          //       }
          //     });
          //   }
          // }
        });

        message.info(`${mode === "create" ? "Created" : "Updated"} task.`);
        afterSubmit();
      } catch (err) {
        handleError(err);
      }
    });
  }

  const formItemLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 14 }
  };

  function accept() {
    const invitations = [...props.form.getFieldValue("invitations")];
    invitations.find(i => i.username === user.username).state = "accepted";
    props.form.setFieldsValue({ invitations: invitations });

    if (mode !== "create") {
      submitRef.current.buttonNode.click();
    }
  }

  function decline() {
    const invitations = [...props.form.getFieldValue("invitations")];
    invitations.find(i => i.username === user.username).state = "declined";
    props.form.setFieldsValue({ invitations: invitations });

    if (mode !== "create") {
      submitRef.current.buttonNode.click();
    }
  }

  const renderInvitation = item => {
    const tagColors = {
      pending: "orange",
      accepted: "green",
      declined: "red"
    };

    const startDate = props.form.getFieldValue("startDate");

    const available = !!!meetings.find(
      m =>
        m.invitations.some(i => i.username === item.username) &&
        startDate &&
        m.startDate &&
        m.endDate &&
        startDate.isAfter(moment.unix(m.startDate)) &&
        startDate.isBefore(moment.unix(m.endDate))
    );

    return (
      <span>
        <Tooltip title={available ? "Available" : "Busy"} mouseEnterDelay={1}>
          <Badge
            text={item.username}
            title={available ? "Available" : "Busy"}
            color={available ? "green" : "orange"}
            status={item.state === "pending" ? "processing" : "success"}
            style={{ marginRight: 3 }}
          />
        </Tooltip>
        <Tag color={tagColors[item.state]}>{item.state}</Tag>

        {item.username === user.username && (
          <span>
            <Button type="primary" size="small" onClick={accept}>
              Accept
            </Button>
            <Button type="danger" size="small" onClick={decline}>
              Decline
            </Button>
          </span>
        )}
      </span>
    );
  };

  const deleteMeetingHandler = async () => {
    await deleteMeeting({
      variables: { _id: mode },
      update: (cache, { data }) => {
        const cacheParams = {
          query: COMPANY_MEETINGS,
          variables: { companyId, username: user.username }
        };
        const { companyMeetings } = cache.readQuery(cacheParams);
        const updatedMeetings = [...companyMeetings];
        updatedMeetings.splice(
          updatedMeetings.findIndex(el => el._id === data.deleteMeeting._id),
          1
        );

        cache.writeQuery({
          ...cacheParams,
          data: { companyMeetings: updatedMeetings }
        });
      }
    });
    afterSubmit();
  };

  if (mode !== "create" && mode !== initialData._id) {
    return <Empty description="This meeting was removed." />;
  }

  return (
    <div className="MeetingForm">
      <Form labelAlign="left" {...formItemLayout} onSubmit={onSubmit}>
        <Form.Item label="Meeting name">
          {getFieldDecorator("name", {
            rules: [{ required: true, message: "Name is required." }],
            initialValue: initialData.name
          })(<Input placeholder="Name" />)}
        </Form.Item>

        <Form.Item label="Description">
          {getFieldDecorator("description", {
            initialValue: initialData.description
          })(<Input placeholder="Description" />)}
        </Form.Item>

        <Form.Item label="Start date">
          {getFieldDecorator("startDate", {
            rules: [{ required: true, message: "Start date is required." }],
            initialValue:
              initialData.startDate && moment.unix(initialData.startDate)
          })(<DatePicker showTime />)}
        </Form.Item>

        <Form.Item label="End date">
          {getFieldDecorator("endDate", {
            initialValue:
              initialData.endDate && moment.unix(initialData.endDate)
          })(<DatePicker showTime />)}
        </Form.Item>

        <Form.Item label="Repetition">
          {getFieldDecorator("repetition", {
            initialValue: initialData.repetition
          })(
            <Radio.Group buttonStyle="solid">
              {[
                "None",
                "Daily",
                "Weekly",
                "Monthly (weekday)",
                "Monthly (day number)"
              ].map(rep => (
                <Radio.Button key={rep} value={rep}>
                  {rep}
                </Radio.Button>
              ))}
            </Radio.Group>
          )}
        </Form.Item>

        <Form.Item label="Private" key="private">
          {getFieldDecorator("private", {
            initialValue: initialData.private,
            valuePropName: "checked"
          })(<Switch unCheckedChildren="public" checkedChildren="private" />)}
        </Form.Item>

        <Form.Item label="Invited">
          {getFieldDecorator("invitations", {
            initialValue: initialData.invitations
          })(
            <InputWithList
              inputAutoComplete={companyUsers}
              getItem={item => ({ username: item, state: "pending" })}
              renderItem={renderInvitation}
              addButtonCaption="Add person"
              uniqueProp="username"
            />
          )}
        </Form.Item>

        <Form.Item
          label={
            <span>
              Invited spaces
              <Tooltip
                title={`
                  You can invite whole spaces. You won't know if they
                  accepted your invitation. If you want specific person to
                  confirm his/her presence use "invitations" input above.
                `}
              >
                <Icon style={{ marginLeft: 3 }} type="info-circle" />
              </Tooltip>
            </span>
          }
        >
          {getFieldDecorator("invitedSpaces", {
            initialValue: initialData.invitedSpaces
          })(
            <Select
              showSearch
              onSearch={setSpaceFilter}
              filterOption={false}
              mode="multiple"
            >
              {spaces.map(s => (
                <Select.Option key={s._id} value={s._id} title={s.name}>
                  {s.name}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>

        <Form.Item wrapperCol={{ span: 12, offset: 6 }}>
          <Button
            type="primary"
            htmlType="submit"
            className="submit-button"
            ref={submitRef}
          >
            Submit
          </Button>
          {mode !== "create" && (
            <Button type="danger" onClick={deleteMeetingHandler}>
              Delete meeting
            </Button>
          )}
        </Form.Item>
      </Form>
    </div>
  );
}

export default Form.create()(MeetingForm);
