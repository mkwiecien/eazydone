import React, { Component } from "react";
import PropTypes from "prop-types";
import { apiUrl } from "../../index";
import untitled from "../../logo.svg";
import SignInForm from "../../components/SignInForm/SignInForm";

import "./SignIn.css";

export default class SignIn extends Component {
  static propTypes = {
    onSignIn: PropTypes.func
  };

  componentDidMount() {
    fetch(`${apiUrl}logout`, {
      method: "POST",
      credentials: "include"
    }).catch(err => console.error(err));
  }

  signIn = async data => {
    data.accountType = "OWNER";
    data.active = true;

    return new Promise(async (resolve, reject) => {
      try {
        const body = JSON.stringify(data);
        const res = await fetch(`${apiUrl}signin`, {
          method: "POST",
          credentials: "include",
          headers: {
            "Content-Type": "application/json; charset=utf-8"
          },
          body
        });
        resolve(res);
      } catch (err) {
        reject(err);
      }
    });
  };

  render() {
    return (
      <div className="Login Signin">
        <img src={untitled} className="my-logo my-logo-login" alt="logo" />
        <div className="example">
          <SignInForm signIn={this.signIn} onSignIn={this.props.onSignIn} />
        </div>
      </div>
    );
  }
}
