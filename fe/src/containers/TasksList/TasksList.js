import React, { useState } from "react";
import { navigate } from "@reach/router";
import { useMutation } from "@apollo/react-hooks";
import { Collapse, Button, Modal } from "antd";
import {
  EDIT_TASK,
  DELETE_TASK,
  DELETE_MILESTONE,
  EDIT_TASKS,
  DELETE_TASKS
} from "../../queries";
import MilestoneForm from "../MilestoneForm/MilestoneForm";
import TaskForm from "../TaskForm/TaskForm";
import "./TasksList.css";
import Loader from "../../components/Loader/Loader";
import FilteredTasks from "../FilteredTasks/FilteredTasks";
import ListMilestone from "../../components/ListMilestone/ListMilestone";

export default function TasksList(props) {
  const {
    selectedSpaceId,
    selectedCompanyId,
    user,
    spaces,
    milestones,
    notifications,
    files,
    filter,
    setFilter,
    tags
  } = props;
  let tasks = props.tasks.filter(t => t.spaceId === selectedSpaceId);
  const [editedSpaceId, setEditedSpaceId] = useState("");
  const [editedTaskId, setEditedTaskId] = useState("");
  const [milestoneTaskCount, setMilestoneTaskCount] = useState(0);
  const [deletedMilestoneId, setDeletedMilestoneId] = useState("");
  const [clickedMilestoneId, clickMilestoneId] = useState("");
  const [editTask, { loading: editTaskLoading }] = useMutation(EDIT_TASK);
  const [editTasks, { loading: editTasksLoading }] = useMutation(EDIT_TASKS);
  const [deleteTask, { loading: deleteTaskLoading }] = useMutation(DELETE_TASK);
  const [deleteTasks, { loading: deleteTasksLoading }] = useMutation(
    DELETE_TASKS
  );
  const [deleteMilestone, { loading: deleteMilestoneLoading }] = useMutation(
    DELETE_MILESTONE
  );

  if (
    deleteTaskLoading ||
    deleteTasksLoading ||
    editTaskLoading ||
    editTasksLoading ||
    deleteMilestoneLoading
  )
    return <Loader />;

  const space = spaces.find(s => s._id === selectedSpaceId);

  if (
    props.milestoneId &&
    !milestones.map(m => m._id).includes(props.milestoneId)
  ) {
    navigate("/tasks/list");
  }

  const onEditClick = mstone => e => {
    e.preventDefault();
    e.stopPropagation();
    setEditedSpaceId(mstone._id);
  };

  const addTaskToMilestone = milestoneId => event => {
    event.preventDefault();
    event.stopPropagation();
    clickMilestoneId(milestoneId);
    setEditedTaskId("create");
  };

  const hideTaskModal = () => {
    setEditedTaskId("");
    clickMilestoneId("");
  };

  const deleteMilestoneHandler = (mId, mTasks) => async event => {
    event.preventDefault();
    event.stopPropagation();

    setDeletedMilestoneId(mId);

    if (mTasks.length === 0) {
      // remove right now
      await deleteMilestone({ variables: { _id: mId } });
    } else {
      // show modal
      setMilestoneTaskCount(mTasks.length);
    }
  };

  const deleteMilestoneAndMoveTasks = async _ => {
    await editTasks({
      variables: {
        ids: tasks
          .filter(t => t.milestoneId === deletedMilestoneId)
          .map(t => t._id)
      }
    });
    await deleteMilestone({ variables: { _id: deletedMilestoneId } });

    setMilestoneTaskCount(0);
    setDeletedMilestoneId("");
  };

  const deleteMilestoneAndTasks = async _ => {
    await deleteTasks({
      variables: {
        ids: tasks
          .filter(t => t.milestoneId === deletedMilestoneId)
          .map(t => t._id)
      }
    });
    await deleteMilestone({ variables: { _id: deletedMilestoneId } });

    setMilestoneTaskCount(0);
    setDeletedMilestoneId("");
  };
  const editTaskHandler = task => () => {
    setEditedTaskId(task._id);
  };

  const deleteTaskHandler = task => async () => {
    await deleteTask({ variables: { _id: task._id } });
  };

  const onCollapseChange = val => {
    if (!val[0]) navigate("/tasks/list");
    else {
      navigate(`/tasks/list/${val[val.length - 1]}`);
    }
  };

  async function updateState(task) {
    const spacesStates = space.states;
    const index = spacesStates.findIndex(state => state.text === task.state);

    await editTask({
      variables: {
        _id: task._id,
        state: spacesStates[(index + 1) % spacesStates.length].text
      }
    });
  }

  return (
    <div className="TasksList">
      <Modal
        title="Delete milestone"
        destroyOnClose
        visible={milestoneTaskCount > 0}
        onCancel={_ => setMilestoneTaskCount(0)}
        width="max-content"
        footer={
          <div>
            <Button type="primary" onClick={deleteMilestoneAndMoveTasks}>
              Delete milestone and move{" "}
              {milestoneTaskCount === 1 ? "task" : "tasks"} to backlog
            </Button>
            <Button type="danger" onClick={deleteMilestoneAndTasks}>
              Delete milestone and{" "}
              {milestoneTaskCount === 1
                ? "task"
                : `all ${milestoneTaskCount} tasks`}
            </Button>
            <Button onClick={_ => setMilestoneTaskCount(0)}>Cancel</Button>
          </div>
        }
      >
        <p>
          There {milestoneTaskCount === 1 ? "is" : "are"}{" "}
          <span style={{ fontWeight: "bold" }}>{milestoneTaskCount}</span>{" "}
          {milestoneTaskCount === 1 ? "task" : "tasks"} in this milestone.
        </p>
      </Modal>
      <Modal
        title="Edit milestone"
        visible={!!editedSpaceId}
        onCancel={() => setEditedSpaceId("")}
        className="edit-milestone-modal"
        footer={null}
        destroyOnClose
      >
        <MilestoneForm
          {...props}
          mode={editedSpaceId}
          initialData={milestones.find(m => m._id === editedSpaceId)}
          afterSubmit={() => setEditedSpaceId("")}
        />
      </Modal>
      <Modal
        title={`${editedTaskId === "create" ? "Create" : "Edit"} task`}
        visible={!!editedTaskId}
        onCancel={hideTaskModal}
        className="edit-task-modal"
        footer={null}
        destroyOnClose
        width={750}
      >
        <TaskForm
          {...props}
          mode={editedTaskId}
          files={files}
          initialData={
            tasks.find(t => t._id === editedTaskId) || {
              milestoneId: clickedMilestoneId,
              spaceId: selectedSpaceId
            }
          }
          afterSubmit={hideTaskModal}
          afterDelete={hideTaskModal}
        />
      </Modal>

      <FilteredTasks
        filter={filter}
        setFilter={setFilter}
        tasks={tasks}
        tags={tags}
        space={space}
        selectedSpaceId={selectedSpaceId}
        milestones={milestones}
        renderContent={(filteredTasks, changeOrderInState) => (
          <div className="milestones">
            <Collapse
              onChange={onCollapseChange}
              bordered={false}
              activeKey={props.milestoneId}
            >
              {milestones.map(m => (
                <ListMilestone
                  key={m._id}
                  milestone={m}
                  tasks={filteredTasks}
                  notifications={notifications}
                  space={space}
                  addTaskToMilestone={addTaskToMilestone}
                  onEditClick={onEditClick}
                  deleteMilestoneHandler={deleteMilestoneHandler}
                  user={user}
                  tags={tags}
                  selectedCompanyId={selectedCompanyId}
                  editTaskHandler={editTaskHandler}
                  deleteTaskHandler={deleteTaskHandler}
                  updateState={updateState}
                  changeOrderInState={changeOrderInState}
                />
              ))}
            </Collapse>
          </div>
        )}
      />
    </div>
  );
}
