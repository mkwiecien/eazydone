import React, { useState } from "react";
import { Form, Input, Button, Icon, message } from "antd";
import { navigate } from "@reach/router";
import { apiUrl } from "../../index";
import untitled from "../../logo.svg";
import { handleError } from "../../utils";

export function ForgotPassword(props) {
  const getFieldDecorator = props.form.getFieldDecorator;
  const token = props.token;
  const [confirmDirty, setConfirmDirty] = useState(false);

  const handleBlur = e =>
    setConfirmDirty({
      confirmDirty: confirmDirty || !!e.target.value
    });

  const compareToFirstPassword = (rule, value, cb) => {
    const form = props.form;
    if (value && value !== form.getFieldValue("password")) {
      cb("Passwords don't match.");
    } else {
      cb();
    }
  };

  const validateToNextPassword = (rule, value, cb) => {
    const form = props.form;
    if (value && confirmDirty) {
      form.validateFields(["confirm"], { force: true });
    }
    cb();
  };

  const postNewPassword = event => {
    event.preventDefault();

    props.form.validateFields(async (err, values) => {
      if (!err) {
        const { email } = values;
        try {
          const res = await fetch(`${apiUrl}reset-password/${token}`, {
            method: "POST",
            headers: {
              "Content-Type": "application/json; charset=utf-8"
            },
            body: JSON.stringify({ email, newPassword: values.password })
          });

          if (res.status !== 200) {
            return message.error("Invalid token");
          }

          message.success(`
            Password was updated.
          `);

          navigate("/login");
        } catch (err) {
          handleError(err);
        }
      }
    });
  };

  if (token)
    return (
      <div className="Login">
        <img src={untitled} className="my-logo my-logo-login" alt="logo" />
        <Form className="login-form" onSubmit={postNewPassword}>
          <Form.Item label="Enter new password">
            {getFieldDecorator("password", {
              rules: [
                { required: true, message: "Password is required." },
                { validator: validateToNextPassword },
                {
                  min: 7,
                  message: "Password must containt minimum of 7 characters."
                }
              ]
            })(
              <Input
                prefix={
                  <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                placeholder="Password"
                type="password"
              />
            )}
          </Form.Item>
          <Form.Item label="Repeat password">
            {getFieldDecorator("confirm", {
              rules: [
                { required: true, message: "Please confirm your password." },
                { validator: compareToFirstPassword }
              ]
            })(
              <Input
                prefix={
                  <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                placeholder="Password"
                type="password"
                onBlur={handleBlur}
              />
            )}
          </Form.Item>
          <Button
            style={{ width: 200 }}
            className="login-button"
            htmlType="submit"
          >
            Change password
          </Button>
        </Form>
      </div>
    );

  const handleSubmit = e => {
    e.preventDefault();
    props.form.validateFields(async (err, values) => {
      if (!err) {
        const { email } = values;
        try {
          await fetch(`${apiUrl}reset-password`, {
            method: "POST",
            headers: {
              "Content-Type": "application/json; charset=utf-8"
            },
            body: JSON.stringify({ email })
          });

          message.success(
            `
            An email was sent with intructions to reset your password.\n
            Please remember to check SPAM folder if needed.
          `,
            5000
          );
        } catch (err) {
          handleError(err);
        }
      }
    });
  };

  return (
    <div className="Login">
      <img src={untitled} className="my-logo my-logo-login" alt="logo" />
      <Form className="login-form" onSubmit={handleSubmit}>
        <Form.Item>
          {getFieldDecorator("email", {
            rules: [{ required: true, message: "Email is required." }]
          })(
            <Input
              prefix={<Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />}
              placeholder="email"
            />
          )}
        </Form.Item>
        <Button
          style={{ width: 200 }}
          className="login-button"
          htmlType="submit"
        >
          Reset password
        </Button>
      </Form>
    </div>
  );
}

export default Form.create()(ForgotPassword);
