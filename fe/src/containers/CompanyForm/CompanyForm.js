import React, { useState, useRef } from "react";
import { navigate } from "@reach/router";
import { Form, Input, Upload, Button, Icon, notification } from "antd";
import { useMutation } from "@apollo/react-hooks";
import { apiUrl } from "../..";
import {
  handleError,
  validateEmail,
  generatePassword,
  copyToClipboard
} from "../../utils";
import {
  ADD_COMPANY,
  USER,
  USER_SPACES,
  EDIT_COMPANY,
  CREATE_COMPANY_SPACE
} from "../../queries";
import "./CompanyForm.css";
import InputWithList from "../../components/InputWithList/InputWithList";

function CompanyForm(props) {
  const [uploadedLogo, setLogo] = useState(null);
  const form = useRef(null);

  const [addCompany, { loading: addCompanyLoading }] = useMutation(ADD_COMPANY);
  const [editCompany, { loading: editCompanyLoading }] = useMutation(
    EDIT_COMPANY
  );
  const [
    createCompanySpace,
    { loading: createCompanySpaceLoading }
  ] = useMutation(CREATE_COMPANY_SPACE);

  if (addCompanyLoading || editCompanyLoading || createCompanySpaceLoading)
    return "Loading...";

  const {
    initialData = {
      name: "",
      description: "",
      logo: "",
      accounts: []
    },
    afterSubmit = () => {}
  } = props;

  const onSubmit = (mutation, createCompanySpace) => e => {
    const creating = props.mode === "create";

    e.preventDefault();
    props.form.validateFields(async (err, values) => {
      if (!err) {
        const data = {
          ...initialData,
          ...values,
          logo: values.logo ? values.logo.file.response.Location : ""
        };

        if (creating) {
          data.ownerId = props.user._id;
          data.accounts = [
            ...data.accounts,
            {
              _id: props.user._id,
              username: props.user.username,
              email: props.user.email
            }
          ];
        }

        try {
          const createdCompany = await mutation({
            variables: data,
            update: (cache, { data }) => {
              if (creating) {
                const { user } = cache.readQuery({
                  query: USER,
                  variables: {
                    username: props.user.username,
                    email: props.user.email
                  }
                });

                let updatedCompanies = [...user.companies];
                updatedCompanies.push(data.addCompany);

                cache.writeQuery({
                  query: USER,
                  variables: {
                    username: props.user.username,
                    email: props.user.email
                  },
                  data: {
                    user: {
                      ...user,
                      companies: updatedCompanies
                    }
                  }
                });
              }
            }
          });

          // create space for the new company
          if (creating) {
            const addedCompany = createdCompany.data.addCompany;

            await createCompanySpace({
              variables: {
                _id: addedCompany._id,
                name: "Company"
              },
              update: (cache, { data }) => {
                const cacheParams = {
                  query: USER_SPACES,
                  variables: {
                    username: props.user.username
                  }
                };
                const { userSpaces } = cache.readQuery(cacheParams);

                const newOne = {
                  ...data.createSpace,
                  company: addedCompany
                };

                cache.writeQuery({
                  ...cacheParams,
                  data: { userSpaces: [...userSpaces, newOne] }
                });
              }
            });
          }

          afterSubmit && afterSubmit();

          notification["success"]({
            message: "Success",
            description: `${creating ? "Created" : "Edited"} company.`
          });
          // navigate("/companies");

          document.location.reload();
        } catch (err) {
          handleError(err);
        }
      }
    });
  };

  const onUploadChange = e => setLogo(e.file);

  const validateEmailField = email =>
    validateEmail(email) ? null : "Email address is incorrect.";

  const copyUserPassword = account => {
    copyToClipboard(account.password, "Password copied.");
  };

  const addPasswordToAccount = email => ({
    email,
    password: generatePassword()
  });

  const { getFieldDecorator } = props.form;
  const { hideSubmitButton, mode } = props;

  const formItemLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 14 }
  };

  let fileList =
    uploadedLogo !== null
      ? uploadedLogo
      : initialData.logo
      ? {
          url: initialData.logo,
          name: "",
          thumbUrl: initialData.logo,
          uid: initialData.logo
        }
      : null;
  fileList = [fileList].filter(el => !!el);

  const mutation = mode === "create" ? addCompany : editCompany;

  return (
    <div className="CompanyForm">
      <Form
        ref={form}
        labelAlign="left"
        {...formItemLayout}
        onSubmit={onSubmit(mutation, createCompanySpace)}
      >
        <Form.Item label="Company name">
          {getFieldDecorator("name", {
            rules: [{ required: true, message: "Name is required." }],
            initialValue: initialData.name
          })(<Input placeholder="Name" />)}
        </Form.Item>
        <Form.Item label="Description">
          {getFieldDecorator("description", {
            initialValue: initialData.description
          })(<Input placeholder="Description" />)}
        </Form.Item>

        <Form.Item label="Logo">
          {getFieldDecorator("logo")(
            <Upload
              className="logo-upload"
              onChange={onUploadChange}
              fileList={fileList}
              name="logo"
              action={`${apiUrl}pics`}
              listType="picture"
              onRemove={() => setLogo("")}
            >
              <Button>
                <Icon type="upload" />
                Upload
              </Button>
            </Upload>
          )}
        </Form.Item>

        <Form.Item label="Your team">
          {getFieldDecorator("accounts", {
            initialValue: initialData.accounts
          })(
            <InputWithList
              validateFunc={validateEmailField}
              optionIconVisible={item => !!item.password}
              optionIcon="copy"
              optionTooltip="Copy password"
              onOptionClick={copyUserPassword}
              getItem={addPasswordToAccount}
              renderItem={item => item.username || item.email || item}
            />
          )}
        </Form.Item>

        {!hideSubmitButton && (
          <Form.Item wrapperCol={{ span: 12, offset: 6 }}>
            <Button type="primary" htmlType="submit" className="submit-button">
              Submit
            </Button>
          </Form.Item>
        )}
      </Form>
    </div>
  );
}

export default Form.create()(CompanyForm);
