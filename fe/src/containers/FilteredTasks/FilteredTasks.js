import React, { useState, useEffect } from "react";
import lodash from "lodash";
import moment from "moment";
import { Typography, Mentions, Divider } from "antd";

import "./FilteredTasks.css";

export default function FilteredsTasks(props) {
  const {
    tasks = [],
    tags = [],
    space,
    selectedSpaceId,
    milestones: milestonesFromProps = [],
    filter,
    setFilter,
    renderContent = _ => null,
    hideDivider = false,
    disableMilestones = false
  } = props;
  const [hashOptions, setHashOptions] = useState([]);
  const [prefix, setPrefix] = useState("@");

  let milestones = [...milestonesFromProps];

  function onFilterSelect({ value }) {
    if (value === 'status: "') {
      setHashOptions(space.states.map(s => s.text + '"'));
    } else if (value === 'assignee: "') {
      let tasksInner = props.tasks.filter(t => t.spaceId === selectedSpaceId);
      const users = lodash
        .uniq(tasksInner.map(t => t.assignee && t.assignee + '"'))
        .filter(el => !!el);
      setHashOptions(users);
    } else if (!disableMilestones && value === 'milestone: "') {
      setHashOptions(milestones.map(m => m.name + '"'));
    } else if (value === 'tag: "') {
      setHashOptions(tags.map(t => t.name + '"'));
    } else {
      setHashOptions([]);
    }
  }

  const filterOptions = {
    "@": [
      'status: "',
      'assignee: "',
      !disableMilestones && 'milestone: "',
      'tag: "'
    ].filter(el => !!el),
    '"': hashOptions || []
  };

  function filterTasks(tasks) {
    const filterRe = /@(\w+):\s"([^"]+)/g;
    const searchString = filter
      .replace(/[\s+]?@\w+:\s".*"[\s+]?/g, "")
      .toLowerCase();

    let filteredTasks = [...tasks];
    let match = null;
    const tagsFilters = [];

    while ((match = filterRe.exec(filter))) {
      const filterProp = match[1];
      const filterVal = match[2];

      if (filterProp === "status") {
        filteredTasks = filteredTasks.filter(t => t.state === filterVal);
      } else if (filterProp === "assignee") {
        filteredTasks = filteredTasks.filter(t => t.assignee === filterVal);
      } else if (filterProp === "milestone") {
        if (disableMilestones) continue;
        const milestone = milestones.find(m => m.name === filterVal);

        if (!milestone) continue;

        filteredTasks = filteredTasks.filter(
          t => t.milestoneId === milestone._id
        );
      } else if (filterProp === "tag") {
        tagsFilters.push(filterVal);
      }
    }

    if (tagsFilters.length > 0) {
      filteredTasks = filteredTasks.filter(
        t =>
          t.tagsNames &&
          tagsFilters.every(filterTag => t.tagsNames.includes(filterTag))
      );
    }

    if (searchString) {
      filteredTasks = filteredTasks.filter(
        t =>
          (t.title && t.title.toLowerCase().indexOf(searchString) > -1) ||
          (t.description &&
            t.description.toLowerCase().indexOf(searchString) > -1)
      );
    }
    return filteredTasks;
  }

  const filteredTasks = filterTasks(tasks);

  const [stateTasks, setTasks] = useState(filteredTasks);

  let timestamp = moment().valueOf();

  useEffect(_ => setTasks(filteredTasks), [tasks]);

  function changeOrderInState(dragTask, hoverTask, insertAfter = true) {
    const now = moment().valueOf();
    if (now - timestamp < 50) return;
    timestamp = now;

    let arr = [...tasks];
    const task = arr.find(t => t._id === dragTask._id);

    task.state = hoverTask.state;

    if (hoverTask._id) {
      const dIndex = arr.findIndex(t => t._id === dragTask._id);
      arr.splice(dIndex, 1);
      const hIndex = arr.findIndex(t => t._id === hoverTask._id);

      if (insertAfter) {
        arr = [...arr.slice(0, hIndex + 1), task, ...arr.slice(hIndex + 1)];
      } else {
        arr = [
          ...arr.slice(0, hIndex),
          task,
          hoverTask,
          ...arr.slice(hIndex + 1)
        ];
      }

      arr.forEach((task, index) => (task.order = index));
    }

    setTasks(arr);
  }
  return (
    <div className="FilteredTasks">
      <Typography.Title level={4}>
        <Mentions
          onSelect={onFilterSelect}
          value={filter}
          onSearch={(_, prefix) => setPrefix(prefix)}
          prefix={["@", '"']}
          onChange={filter => setFilter(filter)}
          split=""
        >
          {filterOptions[prefix].map(o => (
            <Mentions.Option key={o} value={o}>
              {o}
            </Mentions.Option>
          ))}
        </Mentions>
        {!filter && (
          <span className="filter-sumup">Type: '@' to use filter</span>
        )}
        {filter && <span className="filter-sumup"></span>}
      </Typography.Title>
      {!hideDivider && <Divider />}
      {renderContent(stateTasks, changeOrderInState)}
    </div>
  );
}
