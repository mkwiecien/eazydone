import React, { Component } from "react";
import PropTypes from "prop-types";
import { DELETE_COMPANY, USER } from "../../queries";
import PageNotFound from "../../components/PageNotFound/PageNotFound";
import CompanyForm from "../CompanyForm/CompanyForm";
import CrudList from "../../components/CrudList/CrudList";

import "./Companies.css";

export default class Companies extends Component {
  static propTypes = {
    user: PropTypes.object.isRequired
  };

  getInitialData = company => {
    if (!company) {
      return { accounts: [] };
    }
    return {
      ...company,
      accounts: [
        ...company.users.map(u => ({ username: u.username, _id: u._id })),
        ...company.inactiveUsers.map(user => ({
          email: user.email,
          password: user.password
        }))
      ]
    };
  };

  updateAfterDelete = (cache, { data: { deleteCompany } }) => {
    const { user } = cache.readQuery({
      query: USER,
      variables: {
        username: this.props.user.username,
        email: this.props.user.email
      }
    });

    let updatedCompanies = [...user.companies];
    updatedCompanies.splice(
      updatedCompanies.findIndex(el => el._id === deleteCompany._id),
      1
    );

    cache.writeQuery({
      query: USER,
      variables: {
        username: this.props.user.username,
        email: this.props.user.email
      },
      data: {
        user: {
          ...user,
          companies: updatedCompanies
        }
      }
    });
  };

  shouldRender = () =>
    this.props.user.accountType !== "OWNER" ? <PageNotFound /> : null;

  render = () => (
    <CrudList
      className="Companies"
      addButtonTooltip="Add company"
      listData={this.props.user.companies}
      shouldRender={this.shouldRender}
      deleteMutation={DELETE_COMPANY}
      updateAfterDelete={this.updateAfterDelete}
      canDelete={c => c.ownerId === this.props.user._id}
      canEdit={c => c.ownerId === this.props.user._id}
      canAdd={!!this.props.user.stripeSubscriptionId}
      getInitialData={this.getInitialData}
    >
      <CompanyForm user={this.props.user} />
    </CrudList>
  );
}
