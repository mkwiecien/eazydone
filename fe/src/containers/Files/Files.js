import React, { useState, useMemo } from "react";
import ReactTable from "react-table";
import { Upload, Button, Empty, message, Progress } from "antd";
import { apiUrl } from "../..";
import { FILES } from "../../queries";
import Loader from "../../components/Loader/Loader";
import { idToDate, handleError } from "../../utils";

import "react-table/react-table.css";
import "./Files.css";

export default function Files(props) {
  const { files, refetch, client, loading, user, selectedCompanyId } = props;
  const [fileList, setFileList] = useState([]);

  const company = user.companies.find(c => c._id === selectedCompanyId);

  let renderedFiles = useMemo(() => {
    if (files.length === 0) {
      return <Empty />;
    }

    const deleteFile = _id => async _ => {
      try {
        const res = await fetch(`${apiUrl}files`, {
          method: "DELETE",
          credentials: "include",
          headers: {
            "Content-Type": "application/json; charset=utf-8"
          },
          body: JSON.stringify({ _id })
        });
        if (res.status !== 200) return handleError(res);

        // update cache
        await refetch();
        message.success("File removed");
        // const cacheParams = {
        //   query: FILES,
        //   variables: { spacesIds: props.spaces.map(s => s._id) }
        // };
        // const { files } = client.cache.readQuery(cacheParams);
        // const updatedFiles = [...files];
        // updatedFiles.splice(updatedFiles.findIndex(f => f._id === _id), 1);
        // client.cache.writeQuery({
        //   ...cacheParams,
        //   data: { files: updatedFiles }
        // });
      } catch (err) {
        handleError(err);
      }
    };

    const columns = [
      {
        id: "name",
        Header: "Name",
        accessor: d => ({ name: d.name, _id: d._id }),
        Cell: props => (
          <a
            target="_blank"
            rel="noopener noreferrer"
            href={`${apiUrl}files/${props.value._id}`}
          >
            {props.value.name}
          </a>
        ),
        filterMethod: (filter, row) =>
          row.name.name.toLowerCase().indexOf(filter.value.toLowerCase()) > -1
      },
      {
        Header: "Uploaded by",
        accessor: "uploadedBy"
      },
      {
        id: "size",
        Header: "Size",
        accessor: d => Math.round(d.size / 1024) + " kb"
      },
      {
        id: "uploadDate",
        Header: "Upload date",
        accessor: d => idToDate(d._id).format("LLLL")
      },
      {
        id: "delete",
        Header: "",
        accessor: "_id",
        Cell: props => (
          <Button type="danger" onClick={deleteFile(props.value)}>
            Delete
          </Button>
        )
      }
    ];

    return <ReactTable columns={columns} data={files} filterable />;
  }, [files, refetch]);

  if (loading) return <Loader />;

  const usageInMB =
    files
      .filter(f => f.uploadedBy === user.username)
      .reduce((a, b) => a + b.size, 0) /
    1024 /
    1024;
  let usagePercentage = (usageInMB * 100) / company.storagePerUserMb;

  async function onUploadChange({ file }) {
    setFileList([file]);

    if (file.response && file.response.message) {
      return handleError(file.response);
    }

    if (file.response) {
      const cacheParams = {
        query: FILES,
        variables: { spacesIds: props.spaces.map(s => s._id) }
      };
      const { files } = client.cache.readQuery(cacheParams);
      const updatedFiles = [...files, { ...file.response, __typename: "File" }];
      client.cache.writeQuery({
        ...cacheParams,
        data: { files: updatedFiles }
      });
      await refetch();
    }
  }

  let strokeColor = "#73d13d";
  if (70 < usagePercentage && usagePercentage <= 90) {
    strokeColor = "#ffec3d";
  } else if (90 < usagePercentage) {
    strokeColor = "#ff4d4f";
  }

  return (
    <div className="Files">
      <div className="files-header">
        <Progress
          type="line"
          percent={usagePercentage}
          format={percent =>
            `${percent.toFixed(2)}%  of ${company.storagePerUserMb}MB`
          }
          strokeColor={strokeColor}
          strokeWidth={15}
          style={{ width: "60%", margin: "auto" }}
        />
        <div className="upload-container">
          <Upload
            className="file-upload"
            withCredentials
            multiple
            action={`${apiUrl}files`}
            onChange={onUploadChange}
            fileList={fileList}
            data={{ spaceId: props.selectedSpaceId }}
            beforeUpload={file => {
              const inSizeLimit = file.size / 1024 / 1024 < 500;

              if (!inSizeLimit) {
                message.error("File must be smaller than 500MB.");
              }

              return inSizeLimit;
            }}
          >
            <Button icon="plus" type="primary">
              Add file
            </Button>
          </Upload>
        </div>
      </div>

      <div className="files-container">{renderedFiles}</div>
    </div>
  );
}
