import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "@reach/router";
import { Menu, Icon, PageHeader } from "antd";
import { Router, navigate } from "@reach/router";
import MilestoneForm from "../MilestoneForm/MilestoneForm";
import TaskChart from "../TaskChart/TaskChart";
import TasksList from "../TasksList/TasksList";
import TaskForm from "../TaskForm/TaskForm";
import TaskDetails from "../TaskDetails/TaskDetails";
import Board from "../Board/Board";
import TimeChart from "../TimeChart/TimeChart";
import BurnChart from "../BurnChart/BurnChart";
import "./Tasks.css";
import TimeReport from "../TimeReport/TimeReport";
import Tags from "../Tags/Tags";
import { idToDate } from "../../utils";

export default class Tasks extends Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
    selectedSpaceId: PropTypes.string,
    selectedCompanyId: PropTypes.string,
    menuCollapsed: PropTypes.bool,
    toggleMenuCollapsed: PropTypes.func,
    tasks: PropTypes.array.isRequired,
    spaces: PropTypes.array.isRequired,
    milestones: PropTypes.array.isRequired,
    notifications: PropTypes.array,
    files: PropTypes.array,
    client: PropTypes.object,
    refetchFiles: PropTypes.func,
    tags: PropTypes.array
  };

  static defaultProps = {
    menuCollapsed: false,
    toggleMenuCollapsed: () => {}
  };

  constructor(props) {
    super(props);
    this.state = {
      menuCollapsed: false,
      taskListFilter: ""
    };

    this.items = [
      { key: "add-milestone", icon: "flag", caption: "Add milestone" },
      { key: "add-task", icon: "check-square", caption: "Add task" },
      { key: "list", icon: "bars", caption: "List" },
      { key: "board", icon: "table", caption: "Board" },
      { key: "tags", icon: "tags", caption: "Manage tags" },
      {
        key: "chart",
        icon: "area-chart",
        caption: "Charts",
        subItems: [
          {
            key: "chart/time-chart",
            icon: "clock-circle",
            caption: "Time spent"
          },
          { key: "chart/burn-chart", icon: "fire", caption: "Burn down chart" }
        ]
      },
      {
        key: "time-report",
        icon: "clock-circle",
        caption: "Time report"
      }
    ];
  }

  toggleMenuCollapsed = () => {
    this.setState({ menuCollapsed: !this.state.menuCollapsed });
  };

  renderItem = ({ key, icon, caption, onClick, subItems }) => {
    const title = (
      <span>
        {icon && <Icon type={icon} />}
        {!this.props.menuCollapsed && caption}
      </span>
    );

    if (!subItems)
      return (
        <Menu.Item key={key} onClick={onClick} title={caption}>
          <Link to={`/tasks/${key}`}>{title}</Link>
        </Menu.Item>
      );

    return (
      <Menu.SubMenu title={title} key={key}>
        {subItems.map(this.renderItem)}
      </Menu.SubMenu>
    );
  };

  getSelectedTab = () => {
    const match = window.location.pathname.match(
      /(?:\/tasks\/)([\w|-]*)(?:$|\/.*|)/
    );
    const key = match ? match[1] : "";
    const selectedTab = this.items.find(el => el.key === key);

    if (!selectedTab) {
      return {
        key: "view-task",
        icon: "eye",
        caption: "Task details"
      };
    }

    return selectedTab;
  };

  renderPageTitle = selectedTab => (
    <span className="page-title">
      <Icon type={selectedTab.icon} />
      {selectedTab.caption}
    </span>
  );

  onTaskCreated = async res => {
    const task = res.data.createTask;
    await this.props.selectSpace(task.spaceId);
    const url = `/tasks/list/${task.milestoneId || ""}`;
    navigate(url);
  };

  render() {
    const {
      menuCollapsed,
      toggleMenuCollapsed,
      tasks,
      spaces,
      tags
    } = this.props;
    const selectedTab = this.getSelectedTab();

    const milestonesArr = [...this.props.milestones]
      .filter(m => m.spaceId === this.props.selectedSpaceId)
      .map(m => ({ ...m, created: idToDate(m._id).unix() }))
      .sort((a, b) => a.created - b.created);

    const onGoing = milestonesArr.filter(m => !m.completed);
    const completed = milestonesArr.filter(m => m.completed);

    const milestones = [
      ...onGoing,
      ...completed,
      { _id: "backlog", name: "Other tasks" }
    ];

    return (
      <div className="Tasks">
        <PageHeader
          onBack={() => window.history.back()}
          title={this.renderPageTitle(selectedTab)}
        />
        <Menu
          mode="inline"
          theme="dark"
          inlineCollapsed={menuCollapsed}
          defaultSelectedKeys={["list"]}
          selectedKeys={[selectedTab.key]}
        >
          {this.items.map(this.renderItem)}
          <li
            className="ant-menu-item collapse-button"
            onClick={toggleMenuCollapsed}
          >
            <Icon type={menuCollapsed ? "double-right" : "double-left"} />
            {!menuCollapsed && "Collapse"}
          </li>
        </Menu>
        <Router className="content">
          <TaskDetails
            path="/:_id"
            selectedSpaceId={this.props.selectedSpaceId}
            user={this.props.user}
            tasks={tasks}
            spaces={spaces}
            selectSpace={this.props.selectSpace}
            milestones={milestones}
            notifications={this.props.notifications}
            files={this.props.files}
            client={this.props.client}
            refetchFiles={this.props.refetchFiles}
            tags={tags}
          />
          <MilestoneForm
            path="add-milestone"
            user={this.props.user}
            selectedSpaceId={this.props.selectedSpaceId}
            spaces={spaces}
          />
          <TaskForm
            path="add-task"
            user={this.props.user}
            selectedSpaceId={this.props.selectedSpaceId}
            selectedCompanyId={this.props.selectedCompanyId}
            tasks={tasks}
            spaces={spaces}
            afterSubmit={this.onTaskCreated}
            milestones={milestones}
            files={this.props.files}
            client={this.props.client}
            refetchFiles={this.props.refetchFiles}
            tags={tags}
          />
          <TasksList
            path="list"
            selectedSpaceId={this.props.selectedSpaceId}
            selectedCompanyId={this.props.selectedCompanyId}
            user={this.props.user}
            tasks={tasks}
            spaces={spaces}
            milestones={milestones}
            notifications={this.props.notifications}
            files={this.props.files}
            filter={this.state.taskListFilter}
            setFilter={taskListFilter => this.setState({ taskListFilter })}
            tags={tags}
          />
          <TasksList
            path="list/:milestoneId"
            selectedSpaceId={this.props.selectedSpaceId}
            selectedCompanyId={this.props.selectedCompanyId}
            user={this.props.user}
            tasks={tasks}
            spaces={spaces}
            milestones={milestones}
            notifications={this.props.notifications}
            files={this.props.files}
            filter={this.state.taskListFilter}
            setFilter={taskListFilter => this.setState({ taskListFilter })}
            tags={tags}
          />
          <Board
            path="board"
            user={this.props.user}
            selectedSpaceId={this.props.selectedSpaceId}
            selectedCompanyId={this.props.selectedCompanyId}
            tasks={tasks}
            spaces={spaces}
            milestones={milestones}
            notifications={this.props.notifications}
            files={this.props.files}
            filter={this.state.taskListFilter}
            setFilter={taskListFilter => this.setState({ taskListFilter })}
            tags={tags}
          />
          <Board
            path="board/:milestoneId"
            user={this.props.user}
            selectedSpaceId={this.props.selectedSpaceId}
            selectedCompanyId={this.props.selectedCompanyId}
            tasks={tasks}
            spaces={spaces}
            milestones={milestones}
            notifications={this.props.notifications}
            files={this.props.files}
            filter={this.state.taskListFilter}
            setFilter={taskListFilter => this.setState({ taskListFilter })}
            tags={tags}
          />
          <Tags
            path="tags"
            tags={tags.filter(t => t.spaceId === this.props.selectedSpaceId)}
            spacesIds={spaces.map(s => s._id)}
          />
          <TaskChart path="chart" />
          <TimeChart
            tasks={tasks}
            path="chart/time-chart"
            selectedSpaceId={this.props.selectedSpaceId}
          />
          <BurnChart
            path="chart/burn-chart"
            selectedSpaceId={this.props.selectedSpaceId}
          />
          <TimeReport path="time-report" user={this.props.user} />
        </Router>
      </div>
    );
  }
}
