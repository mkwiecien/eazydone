import React, { useState } from "react";
import moment from "moment";
import { useMutation } from "@apollo/react-hooks";
import { navigate } from "@reach/router";
import { Form, Input, Select, DatePicker, Button, message } from "antd";
import {
  ADD_MILESTONE,
  EDIT_MILESTONE,
  SPACES_MILESTONES
} from "../../queries";
import "./MilestoneForm.css";
import { handleError } from "../../utils";

export function MilestoneForm(props) {
  const [spaceFilter, setSpaceFilter] = useState("");
  const {
    // user,
    selectedSpaceId,
    spaces = [],
    initialData = {},
    mode = "create",
    afterSubmit = () => {}
  } = props;

  const { getFieldDecorator } = props.form;
  const formItemLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 14 }
  };

  const [createMutation, { loading: createLoading }] = useMutation(
    ADD_MILESTONE
  );
  const [editMutation, { loading: editLoading }] = useMutation(EDIT_MILESTONE);

  const mutation = mode === "create" ? createMutation : editMutation;

  if (createLoading || editLoading) return "Loading...";

  const onSubmit = async e => {
    e.preventDefault();
    props.form.validateFields(async (err, values) => {
      const { date, ...variables } = values;

      if (mode !== "create") {
        variables._id = initialData._id;
      }

      if (!err) {
        try {
          await mutation({
            variables: {
              ...variables,
              startDate: date && date[0] && date[0].unix(),
              endDate: date && date[1] && date[1].unix()
            },
            update: (cache, { data }) => {
              if (mode === "create") {
                const cacheParams = {
                  query: SPACES_MILESTONES,
                  variables: { spaceIds: spaces.map(s => s._id) }
                };
                const { spacesMilestones } = cache.readQuery(cacheParams);
                cache.writeQuery({
                  ...cacheParams,
                  data: {
                    spacesMilestones: [...spacesMilestones, data.addMilestone]
                  }
                });
              }
            }
          });
          message.info(
            `${mode === "create" ? "Created" : "Updated"} milestone.`
          );
          afterSubmit();
          navigate("/tasks/list");
        } catch (err) {
          handleError(err);
        }
      }
    });
  };

  const filteredSpaces = spaces.filter(
    s => s.name.toLowerCase().indexOf(spaceFilter.toLowerCase()) > -1
  );

  return (
    <div className="MilestoneForm">
      <Form labelAlign="left" {...formItemLayout} onSubmit={onSubmit}>
        <Form.Item label="Milestone name">
          {getFieldDecorator("name", {
            rules: [{ required: true, message: "Name is required" }],
            initialValue: initialData.name
          })(<Input placeholder="Name" />)}
        </Form.Item>

        <Form.Item label="Description">
          {getFieldDecorator("description", {
            initialValue: initialData.description
          })(<Input placeholder="(optional)" />)}
        </Form.Item>

        <Form.Item label="Space">
          {getFieldDecorator("spaceId", {
            initialValue: selectedSpaceId,
            rules: [{ required: true, message: "Please select relevant space" }]
          })(
            <Select showSearch onSearch={setSpaceFilter} filterOption={false}>
              {filteredSpaces.map(s => (
                <Select.Option key={s._id} value={s._id}>
                  {s.name}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>

        <Form.Item label="Start / end date">
          {getFieldDecorator("date", {
            initialValue:
              initialData.startDate && initialData.endDate
                ? [
                    moment.unix(initialData.startDate),
                    moment.unix(initialData.endDate)
                  ]
                : null
          })(<DatePicker.RangePicker />)}
        </Form.Item>

        <Form.Item wrapperCol={{ span: 7, offset: 4 }}>
          <Button type="primary" htmlType="submit" className="submit-button">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}

export default Form.create()(MilestoneForm);
