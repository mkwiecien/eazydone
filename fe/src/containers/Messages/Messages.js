import React, {
  useState,
  useRef,
  useEffect,
  useCallback,
  useMemo
} from "react";
import lodash from "lodash";
import moment from "moment";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { navigate } from "@reach/router";
import { Picker, emojiIndex } from "emoji-mart";
import ReactTextareaAutocomplete from "@webscopeio/react-textarea-autocomplete";
import {
  PageHeader,
  Menu,
  Icon,
  Tooltip,
  Empty,
  Button,
  Comment,
  Avatar,
  Badge,
  message
} from "antd";
import {
  ADD_MESSAGE,
  MARK_SEEN_MANY,
  NOTIFICATIONS,
  TYPE,
  CHAT
} from "../../queries";
import {
  idToDate,
  linkifyAndHoverify,
  isUserMentioned,
  isMongoId
} from "../../utils";
import UserPopover from "../../components/UserPopover/UserPopover";
import { LAST_SEEN_DELAY } from "../..";
import Loader from "../../components/Loader/Loader";

import "emoji-mart/css/emoji-mart.css";
import "./Messages.css";

const SCROLL_TOP_CHECKPOINT = 100;

export default function Messages(props) {
  let { user, chatId, notifications, spaces, whoIsTyping } = props;
  const [msg, setMsg] = useState("");
  const [offset, setMessagesOffset] = useState(0);
  const [prevOffset, setPrevOffset] = useState(0);
  const [emojiPickerVisible, setEmojiPickerVisible] = useState(false);
  const [lastIndex, setLastIndex] = useState(0);
  const listRef = useRef(null);

  const setOffset = val => {
    setPrevOffset(offset);
    setMessagesOffset(val);
  };

  const debouncedInfo = useCallback(
    lodash.debounce(_ => message.info("No more messages."), 1000),
    []
  );

  chatId = chatId || "";

  const setChat = chat => {
    navigate(`/messages/${chat.value}`);
    markSeenInChat(chat.value);
  };

  const [addMessage] = useMutation(ADD_MESSAGE);
  const [markSeenMany] = useMutation(MARK_SEEN_MANY);
  const [type] = useMutation(TYPE);

  const getUsernameFromNotification = n => {
    return n.link.match(/(?:messages)\/(.*)$/)[1];
  };

  const newMessagesByUsername = useMemo(
    () =>
      lodash.countBy(notifications.map(getUsernameFromNotification), el => el),
    [notifications]
  );

  const users = useMemo(() => {
    return lodash.uniqBy(
      lodash.flattenDeep(
        [].concat(user.companies.map(c => [...c.users, c.owner]))
      ),
      "_id"
    );
  }, [user.companies]);

  const items = useMemo(() => {
    return spaces
      .map(s => ({
        caption: s.name,
        value: s._id,
        type: "space",
        count: newMessagesByUsername[s._id]
      }))
      .concat(
        users
          .filter(u => u.username !== user.username)
          .map(u => ({
            caption: u.username,
            username: u.username,
            name: u.name,
            surname: u.surname,
            jobTitle: u.jobTitle,
            value: u.username,
            type: "user",
            count: newMessagesByUsername[u.username],
            active: moment().unix() - u.lastSeen < LAST_SEEN_DELAY,
            avatar: u.avatar
          }))
      );
  }, [spaces, newMessagesByUsername, user.username, users]);

  let chat = items.find(i => i.value === chatId) || { value: "" };

  const markSeenInChat = async chatId => {
    const chatNotifications = notifications.filter(
      n => getUsernameFromNotification(n) === chatId
    );

    if (chatNotifications.length === 0) return;
    await markSeenMany({
      variables: {
        ids: chatNotifications.map(n => n._id)
      },
      update: cache => {
        const params = {
          query: NOTIFICATIONS,
          variables: { username: user.username }
        };
        const { notifications } = cache.readQuery(params);
        const updated = [...notifications].map(n => {
          if (n.link.match(/\/messages\//)) {
            return getUsernameFromNotification(n) === chatId
              ? { ...n, seen: true }
              : n;
          }
          return n;
        });
        cache.writeQuery({ ...params, data: { notifications: updated } });
      }
    });
  };

  const memoizedMarkSeen = useCallback(markSeenInChat, []);

  if (!chatId) {
    setChat(items[0]);
  }

  let from = "";
  let to = "";
  let spaceId = "";

  if (chatId && isMongoId(chatId)) {
    spaceId = chatId;
  } else {
    from = user.username;
    to = chatId;
  }

  const { data: chatMessages, loading: chatLoading, fetchMore } = useQuery(
    CHAT,
    {
      variables: { from, to, spaceId, offset: 0 },
      skip: !chatId,
      notifyOnNetworkStatusChange: true
    }
  );

  useEffect(() => {
    memoizedMarkSeen(chatId);
  }, [newMessagesByUsername, chatId, memoizedMarkSeen]);

  // send notification every second
  const onKeyDown = useMemo(() => {
    return lodash.throttle(async _ => {
      await type({ variables: { username: user.username, to: chatId } });
    }, 1000);
  }, [type, user.username, chatId]);

  let messages = useMemo(() => {
    return (chatMessages && chatMessages.chat) || [];
  }, [chatMessages]);

  useEffect(
    _ => {
      setMessagesOffset(0);
      setPrevOffset(0);
      setLastIndex(0);
    },
    [chatId]
  );

  useEffect(() => {
    const listEl = listRef.current;
    if (listEl) {
      if (offset - prevOffset > 1) {
        const el = document.getElementById(
          messages[messages.length - lastIndex - 1]._id
        );
        const elTop = el.getBoundingClientRect().top;
        if (elTop > listEl.scrollTop) {
          el.scrollIntoView();
        }
        return;
      }

      listEl.scrollTop = listEl.scrollHeight;
    }
  }, [messages, offset, prevOffset, lastIndex]);

  const tareaTrigger = {
    ":": {
      dataProvider: token =>
        emojiIndex.search(token).map(o => ({
          colons: o.colons,
          native: o.native
        })),
      component: ({ entity: { native, colons } }) => (
        <div>{`${colons} ${native}`}</div>
      ),
      output: item => `${item.native}`
    }
  };
  const match = to.match(/^[0-9a-fA-F]{24}$/);
  if (match) {
    const spaceId = match[0];
    const space = spaces.find(s => s._id === spaceId);
    const usersnamesForMentions = space.users.map(u => u.username);

    tareaTrigger["@"] = {
      dataProvider: text =>
        text
          ? usersnamesForMentions.filter(
              username =>
                username.toLowerCase().indexOf(text.toLowerCase()) > -1
            )
          : usersnamesForMentions,
      component: ({ entity }) => <span>{entity}</span>,
      output: (item, trigger) => trigger + item
    };
  }

  async function sendMessage() {
    if (!msg) return;
    await addMessage({
      variables: { from: user.username, to: spaceId || to, text: msg },
      update: (cache, { data }) => {
        const params = {
          query: CHAT,
          variables: { from, to, spaceId, offset: 0 }
        };
        const { chat } = cache.readQuery(params);
        cache.writeQuery({
          ...params,
          data: { chat: [...chat, data.addMessage] }
        });
      }
    });

    setMsg("");
  }

  const typers = whoIsTyping.filter(el => {
    return (
      (el.to.match(/^[0-9a-fA-F]{24}$/) && el.to === chatId) ||
      (chatId === el.username && el.to === user.username)
    );
  });

  function toggleEmojiPicker() {
    setEmojiPickerVisible(!emojiPickerVisible);
  }

  function addEmoji(emoji) {
    setMsg(msg + emoji.native);
    setEmojiPickerVisible(false);
  }

  function handleKeyPress(event) {
    if (event.shiftKey) return;
    if (event.key === "Enter") {
      event.preventDefault();
      event.stopPropagation();
      sendMessage();
    }
  }

  const conversation = useMemo(() => {
    const renderAvatar = msg => {
      const user = users.find(u => u.username === msg.from);
      return (
        <UserPopover user={user} mouseEnterDelay={0.5}>
          <Avatar src={user.avatar}>{user.username.slice(0, 2)}</Avatar>
        </UserPopover>
      );
    };

    const renderAuthor = from => {
      const user = users.find(u => u.username === from);
      return (
        <UserPopover user={user} mouseEnterDelay={0.5}>
          <span>{user.username}</span>
        </UserPopover>
      );
    };

    return (
      <div style={{ position: "relative" }}>
        {chatLoading && <Loader size={50} />}
        {!chatLoading && chatId && messages.length === 0 && (
          <Empty description="No meesages found. Say hi!" />
        )}
        {chatId && messages.length > 0 && (
          <div className="list-wrapper" ref={listRef}>
            <div className="messages">
              {messages.map(item => (
                <Comment
                  id={item._id}
                  style={{
                    backgroundColor: isUserMentioned(user.username, item.text)
                      ? "#3790ff0d"
                      : "white"
                  }}
                  key={item._id}
                  author={renderAuthor(item.from)}
                  avatar={renderAvatar(item)}
                  content={<span>{linkifyAndHoverify(item.text, users)}</span>}
                  datetime={idToDate(item._id).fromNow()}
                />
              ))}
            </div>
          </div>
        )}
      </div>
    );
  }, [messages, chatId, user.username, users, chatLoading]);

  let lastScrollTop = 0;
  const onScroll = async event => {
    const scrollTop = event.target.scrollTop;
    if (scrollTop > lastScrollTop) return; // no second notification

    lastScrollTop = scrollTop;
    if (scrollTop < SCROLL_TOP_CHECKPOINT) {
      if (chatLoading) {
        event.preventDefault();
        event.stopPropagation();
        return;
      }
      if (!chatLoading && offset > 0 && offset === messages.length) {
        event.preventDefault();
        event.stopPropagation();
        debouncedInfo();
        return;
      }
      setLastIndex(messages.length - 1);
      setOffset(messages.length);

      let updatedMsgs = [];
      await fetchMore({
        variables: {
          offset: messages.length
        },
        updateQuery: (prev, { fetchMoreResult: { chat: newChat } }) => {
          updatedMsgs = [...newChat, ...prev.chat];
          return {
            chat: updatedMsgs
          };
        }
      });

      if (updatedMsgs.length > 0 && updatedMsgs.length === messages.length) {
        debouncedInfo();
      }
    }
  };

  return (
    <div className="Messages">
      <PageHeader onBack={() => window.history.back()} title={chat.name} />
      <Menu mode="inline" theme="dark">
        {items.map(i => (
          <Menu.Item key={i.value} onClick={() => setChat(i)}>
            {i.type === "user" && (
              <UserPopover user={i} mouseEnterDelay={0.5}>
                <Badge count={i.count}>
                  <Badge
                    color={i.active ? "green" : "red"}
                    text={<span style={{ marginRight: 5 }}>{i.caption} </span>}
                  />
                </Badge>
              </UserPopover>
            )}
            {i.type === "space" && (
              <Tooltip title={i.caption}>
                <Badge count={i.count}>
                  <Badge
                    color={"transparent"}
                    text={<span style={{ marginRight: 5 }}>{i.caption} </span>}
                  />
                </Badge>
              </Tooltip>
            )}
            {i.type === "space" && <Icon type="team" />}
          </Menu.Item>
        ))}
      </Menu>
      <div
        className={"conversation" + (chatLoading ? " loading" : "")}
        onScroll={onScroll}
      >
        {conversation}
        <div className="input-container" onKeyDown={onKeyDown}>
          <div className="toggle-emoji-button">
            <Icon type="smile" onClick={toggleEmojiPicker} />
            {emojiPickerVisible && (
              <Picker
                set="emojione"
                onSelect={addEmoji}
                showPreview={false}
                showSkinTones={false}
                title="Hover over an emoji"
              />
            )}
          </div>
          <ReactTextareaAutocomplete
            className="message-input my-textarea"
            rows={3}
            value={msg}
            loadingComponent={() => <Loader size={20} />}
            onKeyPress={handleKeyPress}
            onChange={e => {
              setMsg(e.target.value);
            }}
            placeholder="Type your message..."
            trigger={tareaTrigger}
          />
          <Button type="primary" onClick={sendMessage}>
            Send
          </Button>
        </div>
        {typers.length > 0 && (
          <span>
            {typers.map(el => el.username).join(", ") +
              ` ${whoIsTyping.length > 1 ? "are" : "is"} typing...`}
          </span>
        )}
      </div>
    </div>
  );
}
