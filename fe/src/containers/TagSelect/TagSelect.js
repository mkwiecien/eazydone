import React, { forwardRef } from "react";
import { useMutation } from "@apollo/react-hooks";
import { Select } from "antd";
import { ADD_TAG, TAGS } from "../../queries";

export default forwardRef((props, ref) => {
  let {
    tags = [],
    selectedSpaceIdInForm,
    selectedSpaceId,
    spacesIds,
    value
  } = props;
  const spaceId = selectedSpaceIdInForm || selectedSpaceId;
  tags = tags.filter(t => t.spaceId === spaceId);
  const [addTag] = useMutation(ADD_TAG);

  const onTagsChange = async newTags => {
    if (newTags.length > (value || []).length) {
      const newTag = newTags[newTags.length - 1];
      const newTagObj = tags.find(t => t.name === newTag);

      if (!newTagObj) {
        await addTag({
          variables: {
            name: newTag,
            spaceId: selectedSpaceId,
            color: "#0693ed" // default color
          },
          update: (cache, { data }) => {
            const cacheParams = { query: TAGS, variables: { spacesIds } };
            const { tags } = cache.readQuery(cacheParams);
            cache.writeQuery({
              ...cacheParams,
              data: { tags: [...tags, data.addTag] }
            });
          }
        });
      }
    }

    if (props.onChange) {
      props.onChange(newTags);
    }
  };

  return (
    <Select
      {...props}
      mode="tags"
      onChange={onTagsChange}
      ref={ref}
      valueProp={"name"}
    >
      {tags.map(tag => (
        <Select.Option key={tag._id} value={tag.name}>
          {tag.name}
        </Select.Option>
      ))}
    </Select>
  );
});
