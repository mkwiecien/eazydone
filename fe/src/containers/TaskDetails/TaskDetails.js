import React, { useState, useRef, useEffect, useCallback } from "react";
import moment from "moment";
import { useQuery, useMutation } from "@apollo/react-hooks";
import {
  Modal,
  Progress,
  Icon,
  List,
  Comment,
  Input,
  Button,
  Select,
  Empty,
  Avatar,
  Tooltip,
  Mentions,
  Tag
} from "antd";
import InputWithList from "../../components/InputWithList/InputWithList";
import TaskForm from "../TaskForm/TaskForm";
import {
  EDIT_TASK,
  TIME_LOG,
  LOG_TIME,
  DELETE_LAST_TIME_LOG,
  MARK_SEEN_MANY,
  NOTIFICATIONS
} from "../../queries";
import "./TaskDetails.css";
import {
  renderState,
  getTaskProgress,
  formatTime,
  handleError,
  linkify,
  linkifyAndHoverify,
  isUserMentioned
} from "../../utils";
import UserPopover from "../../components/UserPopover/UserPopover";
import Loader from "../../components/Loader/Loader";
import { apiUrl } from "../..";
import TaskHistory from "../../components/TaskHistory/TaskHistory";

export default function TaskDetails(props) {
  const {
    tasks,
    spaces,
    selectedSpaceId,
    selectSpace,
    notifications,
    user,
    files,
    tags
  } = props;
  const [modalVisible, setModalVisible] = useState(false);
  const [comment, setComment] = useState("");
  const [time, setTime] = useState("");
  const [userSelectVisible, setUserSelectVisible] = useState(false);
  const [lastClickedTodo, setLastClickedTodo] = useState("");
  const select = useRef(null);

  const { data: timeLogData, loading: timeLogLoading } = useQuery(TIME_LOG, {
    variables: { userId: props.user._id, taskId: props._id }
  });
  const [editTaskMutation, { loading: editLoading }] = useMutation(EDIT_TASK);
  const [logTime, { loading: logTimeLoading }] = useMutation(LOG_TIME);
  const [markSeenMany] = useMutation(MARK_SEEN_MANY);
  const [
    deleteLastTimeLog,
    { loading: loadingDeleteLastTimeLog }
  ] = useMutation(DELETE_LAST_TIME_LOG);

  const editTask = async ({ variables }) => {
    try {
      await editTaskMutation({
        variables: {
          ...variables,
          todos: variables.todos.map(todo => ({
            text: todo.text,
            done: todo.done
          })),
          comments: variables.comments.map(comment => ({
            author: comment.author,
            text: comment.text,
            date: comment.date
          })),
          updatedBy: props.user.username
        }
      });
    } catch (err) {
      handleError(err);
    }
  };

  const markSeenTask = async taskId => {
    const filteredNotifications = notifications.filter(n => n =>
      n.link.match(/\/tasks\/(.*)$/)[1] === taskId
    );
    if (filteredNotifications.length === 0) return;
    const filteredIds = filteredNotifications.map(n => n._id);
    await markSeenMany({
      variables: { ids: filteredIds },
      update: cache => {
        const params = {
          query: NOTIFICATIONS,
          variables: { username: user.username }
        };
        const { notifications } = cache.readQuery(params);
        const updated = [...notifications].map(n =>
          filteredIds.includes(n._id) ? { ...n, seen: true } : n
        );
        cache.writeQuery({ ...params, data: { notifications: updated } });
      }
    });
  };

  const memoizedMarkSeen = useCallback(markSeenTask);

  useEffect(() => {
    memoizedMarkSeen(props._id);
  }, [props._id, memoizedMarkSeen]);

  useEffect(() => {
    if (select.current) select.current.focus();
  }, [userSelectVisible]);

  const task = tasks.find(t => t._id === props._id);

  useEffect(
    _ => {
      if (task && task.title) {
        document.title = task.title;
      }
    },
    [task]
  );

  if (timeLogLoading || loadingDeleteLastTimeLog) return <Loader />;

  if (task && task.spaceId !== selectedSpaceId) {
    return (
      <div className="another-space-message">
        <span>The task you are trying to open is in another space.</span>
        <Button onClick={() => selectSpace(task.spaceId)}>Switch space</Button>
      </div>
    );
  }

  if (!task) return <Empty />;

  const space = spaces.find(s => s._id === props.selectedSpaceId);
  const users = space.users;
  const assigneeObj = users.find(u => u.username === task.assignee);

  const timeLog = {};

  timeLogData.timeLog.forEach(log => {
    const { username } = users.find(u => u._id === log.userId);
    timeLog[username] = (timeLog[username] || 0) + log.minutes;
  });

  const toggleTodo = ({ text }, index) => async () => {
    setLastClickedTodo(text);
    const todos = [...task.todos];
    todos[index].done = !!!todos[index].done;
    await editTask({
      variables: { ...task, todos }
    });
    setLastClickedTodo("");
  };

  const onTodosChange = async todos => {
    await editTask({
      variables: {
        ...task,
        todos
      }
    });
  };

  const addComment = async () => {
    await editTask({
      variables: {
        ...task,
        comments: [
          ...task.comments,
          {
            author: props.user.username,
            date: moment().unix(),
            text: comment
          }
        ]
      }
    });
    setComment("");
  };

  const setAssignee = async username => {
    await editTask({
      variables: {
        ...task,
        assignee: username || ""
      }
    });
    setUserSelectVisible(false);
  };

  const showSelect = () => {
    setUserSelectVisible(true);
  };

  const updateState = async () => {
    const spacesStates = space.states;
    const index = spacesStates.findIndex(state => state.text === task.state);

    await editTask({
      variables: {
        ...task,
        state: spacesStates[(index + 1) % spacesStates.length].text
      }
    });
  };

  const progress = getTaskProgress(task, space.states);

  const logTimeHandler = async () => {
    const variables = {
      userId: props.user._id,
      taskId: task._id,
      minutes: parseInt(time, 10) || 0
    };

    if (variables.minutes < 1) return;

    await logTime({
      variables,
      update: (cache, { data }) => {
        const cacheParams = {
          query: TIME_LOG,
          variables: { userId: props.user._id, taskId: task._id }
        };
        const { timeLog } = cache.readQuery(cacheParams);
        cache.writeQuery({
          ...cacheParams,
          data: {
            timeLog: [...timeLog, data.logTime]
          }
        });
      }
    });
    setTime(null);
  };

  const timeKeyDown = e => {
    if (e.key === "Backspace") return;
    if (e.key === "Enter") {
      e.preventDefault();
      logTimeHandler();
      return;
    }
    if (!/^[0-9]$/i.test(e.key)) {
      e.preventDefault();
    }
  };

  const deleteLastTimeLogHandler = async () =>
    await deleteLastTimeLog({
      variables: { userId: props.user._id, taskId: task._id },
      update: (cache, { data }) => {
        const cacheParams = {
          query: TIME_LOG,
          variables: { userId: props.user._id, taskId: task._id }
        };
        const { timeLog } = cache.readQuery(cacheParams);
        const updated = [...timeLog];
        updated.splice(
          updated.findIndex(el => el._id === data.deleteLastTimeLog._id),
          1
        );
        cache.writeQuery({ ...cacheParams, data: { timeLog: updated } });
      }
    });

  function renderAvatar(author) {
    const user = space.users.find(u => u.username === author);
    return (
      <UserPopover user={user} mouseEnterDelay={0.5}>
        <Avatar src={user.avatar}>{author.slice(0, 2)}</Avatar>
      </UserPopover>
    );
  }

  function renderAuthor(author) {
    const user = space.users.find(u => u.username === author);
    return (
      <UserPopover user={user} mouseEnterDelay={0.5}>
        <span>{author}</span>
      </UserPopover>
    );
  }

  return (
    <div className="TaskDetails">
      <Modal
        title="Edit task"
        visible={modalVisible}
        onCancel={() => setModalVisible(false)}
        className="edit-task-modal"
        footer={null}
        destroyOnClose
        width={750}
      >
        <TaskForm
          {...props}
          mode={task._id}
          initialData={task}
          files={files}
          afterSubmit={() => setModalVisible(false)}
          afterDelete={() => window.history.back()}
        />
      </Modal>
      <div className="first-part">
        <span className="task-title">
          {task.tagsNames && (
            <span className="task-title-tags">
              {task.tagsNames.map(tName => {
                const tag = tags.find(t => t.name === tName);
                if (!tag) return null;

                return (
                  <Tag className="task-tag" key={tag._id} color={tag.color}>
                    {tag.name}
                  </Tag>
                );
              })}
            </span>
          )}
          {task.title}
        </span>
        {task.description && (
          <p className="task-description">{linkify(task.description)}</p>
        )}
        {!task.description && (
          <p className="task-description empty">(No description.)</p>
        )}
        <Button
          className="edit-button"
          type="ghost"
          onClick={() => setModalVisible(true)}
        >
          Edit
        </Button>
        <div className="first-right-column">
          <span className="data-wrapper">
            <span className="data-label">created by </span>
            <span className="created-by data-item">{task.createdBy}</span>
          </span>
          <span className="data-wrapper">
            <span className="data-label">status: </span>
            <span className="data-item" onClick={updateState}>
              {renderState(task, space)}
            </span>
          </span>

          <span className="assignee-span data-wrapper" onClick={showSelect}>
            <span className="data-label">assignee: </span>
            {!userSelectVisible && task.assignee && (
              <UserPopover user={assigneeObj} mouseEnterDelay={0.5}>
                <span className="task-assignee data-item">
                  <Avatar
                    className="assignee-avatar"
                    size="small"
                    src={
                      (users.find(u => u.username === task.assignee) || {})
                        .avatar
                    }
                  >
                    {task.assignee.slice(0, 2)}
                  </Avatar>
                  <span className="assignee-text">{task.assignee}</span>
                </span>
              </UserPopover>
            )}
            <Select
              value={task.assignee}
              className={userSelectVisible ? "" : "hidden"}
              ref={select}
              onChange={setAssignee}
              onBlur={() => setUserSelectVisible(false)}
              allowClear
            >
              {users.map(user => (
                <Select.Option key={user.username} value={user.username}>
                  {user.username}
                </Select.Option>
              ))}
            </Select>
          </span>
        </div>
      </div>
      <div className="second-part">
        <div className="todos">
          <span>Todos</span>
          <InputWithList
            value={task.todos}
            onChange={onTodosChange}
            removeCaption="Remove todo"
            uniqueProp="text"
            getItem={item => ({ text: item, done: false })}
            renderItem={(item, index) => (
              <span className="todo">
                {(!editLoading || item.text !== lastClickedTodo) && (
                  <Icon
                    type={item.done ? "check-square" : "border"}
                    onClick={toggleTodo(item, index)}
                  />
                )}
                {editLoading && item.text === lastClickedTodo && (
                  <Icon type="loading" />
                )}
                <span
                  className="todo-text"
                  style={{ textDecoration: item.done ? "line-through" : "" }}
                >
                  {item.text}
                </span>
              </span>
            )}
            getItemCaption={item => item.text}
            enableEdit
            editItemCaption={(item, newVal) => ({ ...item, text: newVal })}
          />
        </div>
        <div className="right-middle">
          {task.deadline && (
            <span
              style={{
                color: task.deadline < moment().unix() ? "red" : "initial"
              }}
            >
              <Icon type="calendar" />
              {" " + moment.unix(task.deadline).format("ll")}
            </span>
          )}
          {!editLoading && (
            <Progress type="circle" percent={progress} width={120} />
          )}
          {editLoading && (
            <Icon
              className="progress-loader"
              type="loading"
              style={{ fontSize: 50 }}
            />
          )}
        </div>
      </div>
      <div className="third-part">
        <div className="comments">
          <List
            className="comments-list"
            header={`${task.comments.length} comments`}
            itemLayout="horizontal"
            dataSource={task.comments}
            renderItem={item => (
              <li>
                <Comment
                  author={renderAuthor(item.author)}
                  avatar={renderAvatar(item.author)}
                  content={<p>{linkifyAndHoverify(item.text, users)}</p>}
                  datetime={moment.unix(item.date).fromNow()}
                  style={{
                    backgroundColor: isUserMentioned(user.username, item.text)
                      ? "#3790ff0d"
                      : "white"
                  }}
                />
              </li>
            )}
          />
          <Mentions rows={2} onChange={setComment} value={comment}>
            {users.map(u => (
              <Mentions.Option key={u._id} value={u.username}>
                {u.username}
              </Mentions.Option>
            ))}
          </Mentions>
          <Button type="primary" onClick={addComment}>
            Add comment
          </Button>
        </div>
        <div className="third-right">
          {task.files.length > 0 && (
            <div className="files">
              <span style={{ textDecoration: "underline" }}>
                Files attached
              </span>
              <div className="files-list">
                {task.files
                  .map((fileId, index) => {
                    const file = files.find(f => f._id === fileId);

                    if (!file) return null;

                    return (
                      <a
                        key={file._id}
                        target="_blank"
                        rel="noopener noreferrer"
                        href={`${apiUrl}files/${file._id}`}
                      >
                        {index + 1 + ") " + file.name}
                      </a>
                    );
                  })
                  .filter(el => !!el)}
              </div>
            </div>
          )}
          <div className="time-log">
            <span className="timelog-title">Timelog</span>
            <div className="timelog-entries">
              {Object.keys(timeLog).map(username => (
                <div key={username}>
                  <span className="timelog-username">{username}: </span>
                  <span>{formatTime(timeLog[username])}</span>
                </div>
              ))}
            </div>
            <Input
              value={time}
              onChange={e => setTime(e.target.value)}
              onKeyDown={timeKeyDown}
              placeholder="Log your time in minutes"
            />
            <div className="log-time-buttons">
              <Button
                type="primary"
                onClick={logTimeHandler}
                loading={logTimeLoading}
              >
                Log time
              </Button>
              {Object.keys(timeLog).includes(props.user.username) && (
                <Tooltip title="Cancel your last time log">
                  <Button
                    icon="double-left"
                    size="small"
                    shape="circle"
                    onClick={deleteLastTimeLogHandler}
                  />
                </Tooltip>
              )}
            </div>
          </div>

          <TaskHistory task={task} users={users} />
        </div>
      </div>
    </div>
  );
}
