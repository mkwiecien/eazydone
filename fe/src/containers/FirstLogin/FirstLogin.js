import React from "react";
import { useMutation } from "@apollo/react-hooks";
import SignInForm from "../../components/SignInForm/SignInForm";
import { EDIT_USER } from "../../queries";
import "./FirstLogin.css";

export default function FirstLogin(props) {
  const { user } = props;

  const initialData = {
    email: user.email
  };

  const [mutation, { loading }] = useMutation(EDIT_USER);

  if (loading) return "Loading...";

  return (
    <div className="FirstLogin">
      <SignInForm
        initialData={initialData}
        signIn={async variables => {
          await mutation({ variables });
          window.location.reload();
        }}
      />
    </div>
  );
}
