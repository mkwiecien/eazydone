import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import {
  Form,
  Input,
  Upload,
  Button,
  Icon,
  message,
  Collapse,
  Popconfirm
} from "antd";
import { apiUrl } from "../../";
import { handleError } from "../../utils";
import { EDIT_USER } from "../../queries";
import "./EditProfile.css";

export function EditProfile(props) {
  const {
    user,
    form: { getFieldDecorator }
  } = props;
  const [avatar, setAvatar] = useState(null);
  const [changePassword, toggleChangePassword] = useState(false);
  const [confirmDirty, setConfirmDirty] = useState(false);
  const [editUser, { loading }] = useMutation(EDIT_USER);

  if (loading) return "Loading...";

  const initialData = { ...user };
  const formItemLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 14 }
  };

  let fileList =
    avatar !== null
      ? avatar
      : initialData.avatar
      ? {
          url: initialData.avatar,
          name: "",
          thumbUrl: initialData.avatar,
          uid: initialData.avatar
        }
      : null;
  fileList = [fileList].filter(el => !!el);

  const onSubmit = e => {
    e.preventDefault();
    props.form.validateFields(async (err, values) => {
      if (
        (err && changePassword) ||
        (err &&
          !changePassword &&
          !Object.keys(err).every(
            (el, index) => ["password", "confirm"][index] === el
          ))
      ) {
        return;
      }

      const { password, confirm, ...withoutPassword } = values;

      let variables = {
        ...initialData,
        ...withoutPassword
      };

      if (values.avatar) {
        variables.avatar = values.avatar.file.response.Location;
      }

      if (changePassword) {
        variables.password = password;
      }

      try {
        await editUser({ variables });
        message.success("Updated profile.");
      } catch (err) {
        handleError(err);
      }
    });
  };

  const handleBlur = e =>
    setConfirmDirty({
      confirmDirty: confirmDirty || !!e.target.value
    });

  const compareToFirstPassword = (rule, value, cb) => {
    const form = props.form;
    if (value && value !== form.getFieldValue("password")) {
      cb("Passwords don't match.");
    } else {
      cb();
    }
  };

  const validateToNextPassword = (rule, value, cb) => {
    const form = props.form;
    if (value && confirmDirty) {
      form.validateFields(["confirm"], { force: true });
    }
    cb();
  };

  const clearAvatar = _ => {
    // set somth different than null
    setAvatar("");
  };

  async function cancelSubscribtion() {
    try {
      const res = await fetch(`${apiUrl}cancel`, {
        method: "DELETE",
        credentials: "include"
      });

      if (res.status !== 200) {
        return handleError(res);
      }
      message.info("Subscription has been canceled.");
      setTimeout(_ => document.location.reload(), 500);
    } catch (err) {
      handleError(err);
    }
  }

  return (
    <Form {...formItemLayout} className="EditProfile" onSubmit={onSubmit}>
      <Form.Item label="Name">
        {getFieldDecorator("name", {
          initialValue: initialData.name
        })(<Input placeholder="Name" />)}
      </Form.Item>
      <Form.Item label="Surname">
        {getFieldDecorator("surname", {
          initialValue: initialData.surname
        })(<Input placeholder="Surname" />)}
      </Form.Item>
      <Form.Item label="Job title">
        {getFieldDecorator("jobTitle", {
          initialValue: initialData.jobTitle
        })(<Input placeholder="Job title" />)}
      </Form.Item>

      <Form.Item label="Avatar">
        {getFieldDecorator("avatar")(
          <Upload
            className="logo-upload"
            onChange={e => setAvatar(e.file)}
            fileList={fileList}
            name="logo"
            action={`${apiUrl}pics`}
            listType="picture"
            onRemove={clearAvatar}
          >
            <Button>
              <Icon type="upload" />
              Upload
            </Button>
          </Upload>
        )}
      </Form.Item>

      <Collapse
        bordered={false}
        className="change-password"
        onChange={() => toggleChangePassword(!changePassword)}
        destroyInactivePanel={true}
        defaultActiveKey={[]}
      >
        <Collapse.Panel header="Change password">
          <Form.Item label="Password">
            {getFieldDecorator("password", {
              rules: [
                { required: true, message: "Password is required." },
                { validator: validateToNextPassword },
                {
                  min: 7,
                  message: "Password must containt minimum of 7 characters."
                }
              ]
            })(
              <Input
                prefix={
                  <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                placeholder="Password"
                type="password"
              />
            )}
          </Form.Item>
          <Form.Item label="Repeat password">
            {getFieldDecorator("confirm", {
              rules: [
                { required: true, message: "Please confirm your password." },
                { validator: compareToFirstPassword }
              ]
            })(
              <Input
                prefix={
                  <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                placeholder="Password"
                type="password"
                onBlur={handleBlur}
              />
            )}
          </Form.Item>
        </Collapse.Panel>
      </Collapse>

      {user.accountType === "OWNER" && !!user.stripeSubscriptionId && (
        <Form.Item wrapperCol={{ span: 10, offset: 8 }}>
          <Popconfirm title="Are you sure?" onConfirm={cancelSubscribtion}>
            <Button type="danger">Cancel subscription</Button>
          </Popconfirm>
        </Form.Item>
      )}

      <Form.Item wrapperCol={{ span: 15, offset: 11 }}>
        <Button type="primary" htmlType="submit" className="submit-button">
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
}

export default Form.create()(EditProfile);
