import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Alert } from 'antd';
import './Objection.css';


export default class Objection extends Component {

  static propTypes = {
    msg: PropTypes.string,
    renderContent: PropTypes.func
  }
  
  render() {
    return (
      <div className="Objection">
        <Alert type='info' showIcon message={this.props.msg} />
        { this.props.renderContent() }
      </div>
    )
  }

}
