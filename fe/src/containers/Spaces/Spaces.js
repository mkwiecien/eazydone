import React from "react";
import { useMutation } from "@apollo/react-hooks";
import { navigate } from "@reach/router";
import { Tag, Button, message, Tooltip } from "antd";
import CrudList from "../../components/CrudList/CrudList";
import SpaceForm from "../SpaceForm/SpaceForm";
import { USER_SPACES, EDIT_SPACE, DELETE_SPACE } from "../../queries";
import { handleError, getIndexAsMember } from "../../utils";
import "./Spaces.css";
import Loader from "../../components/Loader/Loader";

// static propTypes = {
//   user: PropTypes.object.isRequired,
//   selectedCompanyId: PropTypes.string.isRequired
// };

export default function Spaces(props) {
  const { spaces, selectedSpaceId, selectSpace } = props;
  const updateAfterDelete = (cache, { data: { deleteSpace } }) => {
    const cacheParams = {
      query: USER_SPACES,
      variables: {
        username: props.user.username
      }
    };
    const { userSpaces } = cache.readQuery(cacheParams);

    const updated = [...userSpaces];
    updated.splice(
      userSpaces.findIndex(el => el._id === deleteSpace._id),
      1
    );

    cache.writeQuery({
      ...cacheParams,
      data: { userSpaces: updated }
    });
  };

  const [editSpace, { loading: editLoading }] = useMutation(EDIT_SPACE);

  if (editLoading) return <Loader />;

  const signInSpace = space => async e => {
    try {
      await editSpace({
        variables: {
          _id: space._id,
          accounts: [...space.accounts, props.user.username]
        }
      });
      message.success("Subscribed to the space.");
    } catch (err) {
      handleError(err);
    }
  };

  const signOutSpace = space => async e => {
    const accounts = [...space.accounts];
    const userIndex = getIndexAsMember(props.user, space);

    accounts.splice(userIndex, 1);

    try {
      await editSpace({
        variables: {
          _id: space._id,
          accounts
        }
      });

      message.success("Unsubscriped to the space.");
    } catch (err) {
      handleError(err);
    }
  };

  const getActions = space => {
    if (space.frozen) return [];
    if (space.owner === props.user._id) return [];
    if (getIndexAsMember(props.user, space) > -1) {
      return [
        <Button size="small" type="ghost" onClick={signOutSpace(space)}>
          sign out
        </Button>
      ];
    }

    return [
      <Button size="small" type="ghost" onClick={signInSpace(space)}>
        sign in
      </Button>
    ];
  };

  const renderTitle = space => {
    const spaceTitleProps = {
      className: "space-title",
      onClick: async () => {
        await selectSpace(space._id);
        navigate("/tasks/list");
      }
    };

    let title = null;

    if (space.name === "Personal") {
      title = <span {...spaceTitleProps}>{space.name}</span>;
    } else if (space.ownerId === props.user._id) {
      // check if he's an owner
      title = (
        <span {...spaceTitleProps}>
          {space.name}
          <Tag color="gold">owner</Tag>
        </span>
      );
    } else if (
      space.accounts.includes(props.user._id) ||
      space.accounts.includes(props.user.username) ||
      space.accounts.includes(props.user.email)
    ) {
      // check is user is a member
      title = (
        <span {...spaceTitleProps}>
          {space.name}
          <Tag color="green">member</Tag>
        </span>
      );
    } else {
      // return default
      title = <span {...spaceTitleProps}>{space.name}</span>;
    }
    return (
      <Tooltip title="Select space" mouseEnterDelay={0.5}>
        {title}
      </Tooltip>
    );
  };

  // cannot edit company's space if user is not an owner
  const canEdit = s =>
    !(
      s.frozen &&
      s.companyId &&
      props.user.companies.find(c => c._id === s.companyId).ownerId !==
        props.user._id
    );

  return (
    <CrudList
      className="Spaces"
      addButtonTooltip="Add space"
      listData={spaces}
      deleteMutation={DELETE_SPACE}
      updateAfterDelete={updateAfterDelete}
      renderTitle={renderTitle}
      canDelete={item => !item.frozen}
      canEdit={canEdit}
      getActions={getActions}
      isItemSelected={item => item._id === selectedSpaceId}
    >
      <SpaceForm {...props} selectedCompanyId={props.selectedCompanyId} />
    </CrudList>
  );
}
