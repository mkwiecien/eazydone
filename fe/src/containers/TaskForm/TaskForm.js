import React, { useState } from "react";
import moment from "moment";
import { unionBy } from "lodash";
import { useMutation } from "@apollo/react-hooks";
import {
  Form,
  message,
  Input,
  Select,
  Button,
  Radio,
  Slider,
  Tooltip,
  Icon,
  DatePicker,
  Upload
} from "antd";
import {
  CREATE_TASK,
  EDIT_TASK,
  DELETE_TASK,
  TASKS,
  FILES
} from "../../queries";
import { handleError, isUsersSpace } from "../../utils";
import "./TaskForm.css";
import InputWithList from "../../components/InputWithList/InputWithList";
import { apiUrl } from "../..";
import Loader from "../../components/Loader/Loader";
import TagSelect from "../TagSelect/TagSelect";

export function TaskForm(props) {
  // props
  const {
    user,
    selectedSpaceId,
    initialData = {},
    mode = "create",
    afterSubmit = () => {},
    afterDelete = () => {},
    spaces,
    milestones: milestonesFromProps,
    files,
    client,
    refetchFiles,
    tags
  } = props;
  const { getFieldDecorator } = props.form;

  // state
  const [milestoneFilter, setMilestoneFilter] = useState("");
  const [spaceFilter, setSpaceFilter] = useState("");
  const [assigneeFilter, setAssigneeFilter] = useState("");
  const [fileList, setFileList] = useState([]);

  // queries & mutations

  const [addTask, { loading: addTaskLoading }] = useMutation(CREATE_TASK);
  const [editTask, { loading: editTaskLoading }] = useMutation(EDIT_TASK);
  const [deleteTask, { loading: deleteTaskLoading }] = useMutation(DELETE_TASK);

  if (addTaskLoading || editTaskLoading || deleteTaskLoading) return <Loader />;

  const mutation = mode === "create" ? addTask : editTask;
  let milestones = [...milestonesFromProps];
  milestones = milestones.filter(
    m => m.name.toLowerCase().indexOf(milestoneFilter.toLowerCase()) > -1
  );
  const filteredSpaces = spaces.filter(
    s => s.name.toLowerCase().indexOf(spaceFilter.toLowerCase()) > -1
  );

  let users = unionBy(...user.companies.map(c => [...c.users, c.owner]), "_id");
  const spaceId = props.form.getFieldValue("spaceId");
  let space = null;
  if (spaceId) {
    space = spaces.find(s => s._id === spaceId);
    users = users.filter(u => isUsersSpace(u, space));
  } else {
    space = spaces.find(s => s._id === selectedSpaceId);
  }
  users = users.filter(
    u => u.username.toLowerCase().indexOf(assigneeFilter.toLowerCase()) > -1
  );

  const spacesIds = spaces.map(s => s._id);

  // handlers
  async function onSubmit(e) {
    e.preventDefault();
    props.form.validateFields(async (err, values) => {
      if (err) return;

      const { deadline, ...variables } = values;

      // getting rid of __typename property
      variables.todos = variables.todos
        ? variables.todos.map(todo => ({
            text: todo.text,
            done: todo.done
          }))
        : [];
      variables.milestoneId = variables.milestoneId || "backlog";
      variables.assignee = variables.assignee || "";

      variables.files = (variables.files || []).map(f => f.value);

      if (mode === "create") {
        variables.createdBy = user.username;
        variables.deadline = deadline && deadline.unix();
      } else {
        variables._id = mode;
        variables.deadline = deadline && deadline.unix();
        variables.updatedBy = user.username;
      }

      try {
        const res = await mutation({
          variables,
          update: (cache, { data }) => {
            if (mode === "create") {
              const cacheParams = {
                query: TASKS,
                variables: { spacesIds }
              };
              const { tasks } = cache.readQuery(cacheParams);
              cache.writeQuery({
                ...cacheParams,
                data: { tasks: [...tasks, data.createTask] }
              });
            }
          }
        });
        message.info(`${mode === "create" ? "Created" : "Updated"} task.`);
        afterSubmit(res);
      } catch (err) {
        handleError(err);
      }
    });
  }

  const onMilestoneSelect = _id => {
    if (!_id) return;
    props.form.setFieldsValue({
      spaceId: milestones.find(m => m._id === _id).spaceId
    });
  };
  const onSpaceSelect = () => {
    props.form.setFieldsValue({
      milestoneId: ""
    });
  };
  const toggleTodo = item => () => {
    const todos = [...(props.form.getFieldValue("todos") || [])];

    const el = todos.find(el => el.text === item.text);
    el.done = !el.done;

    props.form.setFieldsValue({
      todos
    });
  };

  async function onUploadChange({ file }) {
    setFileList([file]);

    if (file.response) {
      const cacheParams = {
        query: FILES,
        variables: { spacesIds }
      };
      const { files } = client.cache.readQuery(cacheParams);
      const updatedFiles = [...files, { ...file.response, __typename: "File" }];
      client.cache.writeQuery({
        ...cacheParams,
        data: { files: updatedFiles }
      });
      await refetchFiles();

      const updatedTaskFiles = [...props.form.getFieldValue("files")];
      updatedTaskFiles.push({
        value: file.response._id,
        text: file.response.name
      });
      props.form.setFieldsValue({ files: updatedTaskFiles });
    }
  }

  const deleteTaskHandler = async () => {
    await deleteTask({ variables: { _id: initialData._id } });
    afterDelete();
  };

  // other
  const formItemLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 14 }
  };

  const filesAutoComplete = files.map(file => ({
    value: file._id,
    text: file.name
  }));

  const additionalButton = (
    <Upload
      className="file-upload"
      withCredentials
      multiple
      fileList={fileList}
      action={`${apiUrl}files`}
      onChange={onUploadChange}
      data={{ spaceId: selectedSpaceId }}
      beforeUpload={file => {
        const inSizeLimit = file.size / 1024 / 1024 < 500;

        if (!inSizeLimit) {
          message.error("File must be smaller than 500MB.");
        }

        return inSizeLimit;
      }}
    >
      <Button icon="plus" type="ghost" size="small" style={{ width: "100%" }}>
        Upload new file...
      </Button>
    </Upload>
  );

  const initialFiles = (initialData.files || [])
    .map(fileId => filesAutoComplete.find(f => f.value === fileId))
    .filter(el => !!el);

  return (
    <div className="TaskForm">
      <Form labelAlign="left" {...formItemLayout} onSubmit={onSubmit}>
        <Form.Item label="Task title">
          {getFieldDecorator("title", {
            rules: [{ required: true, message: "Title is required" }],
            initialValue: initialData.title
          })(<Input placeholder="Title" />)}
        </Form.Item>

        <Form.Item label="State">
          {getFieldDecorator("state", {
            initialValue: initialData.state || space.states[0].text
          })(
            <Radio.Group buttonStyle="solid">
              {space.states.map(state => (
                <Radio.Button key={state.text} value={state.text}>
                  {state.text}
                </Radio.Button>
              ))}
            </Radio.Group>
          )}
        </Form.Item>

        <Form.Item label="Description">
          {getFieldDecorator("description", {
            initialValue: initialData.description
          })(
            <Input.TextArea
              placeholder="Type task description here..."
              rows={4}
            />
          )}
        </Form.Item>

        <Form.Item label="Milestone">
          {getFieldDecorator("milestoneId", {
            initialValue: initialData.milestoneId
          })(
            <Select
              onChange={onMilestoneSelect}
              allowClear
              showSearch
              onSearch={setMilestoneFilter}
              filterOption={false}
            >
              {milestones.map(m => (
                <Select.Option key={m._id} value={m._id}>
                  {m.name}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>

        <Form.Item label="Space">
          {getFieldDecorator("spaceId", {
            rules: [{ required: true, message: "Space is required" }],
            initialValue: initialData.spaceId
          })(
            <Select
              onChange={onSpaceSelect}
              showSearch
              onSearch={setSpaceFilter}
              filterOption={false}
            >
              {filteredSpaces.map(s => (
                <Select.Option key={s._id} value={s._id} title={s.name}>
                  {s.name}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>

        <Form.Item label="Todos">
          {getFieldDecorator("todos", {
            initialValue: initialData.todos
          })(
            <InputWithList
              removeCaption="Remove todo"
              uniqueProp="text"
              getItem={item => ({ text: item, done: false })}
              renderItem={item => (
                <span className="todo">
                  <Icon
                    type={item.done ? "check-square" : "border"}
                    onClick={toggleTodo(item)}
                  />
                  <span
                    className="todo-text"
                    style={{ textDecoration: item.done ? "line-through" : "" }}
                  >
                    {item.text}
                  </span>
                </span>
              )}
              getItemCaption={item => item.text}
              enableEdit
              editItemCaption={(item, newVal) => ({ ...item, text: newVal })}
            />
          )}
        </Form.Item>

        <Form.Item
          label={
            <span>
              Difficulty (optional)
              <Tooltip
                title={`Difficulty of the task will be used to compute the
                progress of the milestone. 1 is easy, where 5 is very difficult
                and time-consuming`}
              >
                <Icon type="info-circle" />
              </Tooltip>
            </span>
          }
        >
          {getFieldDecorator("difficulty", {
            initialValue: initialData.difficulty || 1
          })(<Slider min={1} max={5} step={1} dots />)}
        </Form.Item>

        <Form.Item label="Assignee">
          {getFieldDecorator("assignee", {
            initialValue: initialData.assignee
          })(
            <Select
              showSearch
              onSearch={setAssigneeFilter}
              filterOption={false}
              allowClear
            >
              {users.map(u => (
                <Select.Option key={u.username} value={u.username}>
                  {u.username}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>

        <Form.Item label="Deadline">
          {getFieldDecorator("deadline", {
            initialValue:
              initialData.deadline && moment.unix(initialData.deadline)
          })(<DatePicker />)}
        </Form.Item>

        <Form.Item label="Tags">
          {getFieldDecorator("tagsNames", {
            initialValue: initialData.tagsNames || []
          })(
            <TagSelect
              tags={tags}
              selectedSpaceIdInForm={props.form.getFieldValue("spaceId")}
              selectedSpaceId={selectedSpaceId}
              spacesIds={spacesIds}
            />
          )}
        </Form.Item>

        <Form.Item label="Files" key="files">
          {getFieldDecorator("files", {
            initialValue: initialFiles
          })(
            <InputWithList
              inputAutoComplete={filesAutoComplete}
              uniqueProp="value"
              filterOption={(inputValue, option) =>
                option.props.children
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) > -1
              }
              getItem={fileId =>
                filesAutoComplete.find(f => f.value === fileId)
              }
              renderItem={file => (
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href={`${apiUrl}files/${file.value}`}
                >
                  {file.text}
                </a>
              )}
              additionalButton={additionalButton}
            />
          )}
        </Form.Item>

        <Form.Item wrapperCol={{ span: 7, offset: 6 }} className="footer">
          <Button type="primary" htmlType="submit" className="submit-button">
            Submit
          </Button>
          {mode !== "create" && (
            <Button
              type="danger"
              style={{ marginLeft: 5 }}
              onClick={deleteTaskHandler}
            >
              Delete
            </Button>
          )}
        </Form.Item>
      </Form>
    </div>
  );
}

export default Form.create()(TaskForm);
