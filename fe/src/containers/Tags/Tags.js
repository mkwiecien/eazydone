import React, { useState, useRef, useEffect } from "react";
import { useMutation } from "@apollo/react-hooks";
import { TwitterPicker } from "react-color";
import { EDIT_TAG, DELETE_TAG, TAGS } from "../../queries";
import { Tag, Icon, Alert, Empty } from "antd";

import "./Tags.css";

export default function(props) {
  const { tags: tagsFromProps, spacesIds } = props;
  const wrapper = useRef(null);
  const [colorPickerPos, setColorPickerPos] = useState({ x: -1, y: -1 });
  const [tagId, setTagId] = useState("");
  const [tags, setTags] = useState(tagsFromProps);

  useEffect(() => {
    setTags([...tagsFromProps]);
  }, [tagsFromProps]);

  const [editTag] = useMutation(EDIT_TAG);
  const [deleteTag] = useMutation(DELETE_TAG);

  const onColorSpanClick = _id => event => {
    setTagId(_id);
    event.stopPropagation();
    const { left, top } = wrapper.current.getBoundingClientRect();

    setColorPickerPos({
      x: event.pageX - left - 15,
      y: event.pageY - top + 20
    });
  };

  const onColorChange = colorObj => {
    const updatedTags = [...tags];
    const color = colorObj.hex;
    const index = updatedTags.findIndex(t => t._id === tagId);
    updatedTags[index].color = color;
    setTags(updatedTags);
  };

  const closePicker = async _ => {
    if (colorPickerPos.x === -1) return;
    setColorPickerPos({ x: -1, y: -1 });

    const tag = tags.find(t => t._id === tagId);

    await editTag({
      variables: { _id: tagId, color: tag.color }
    });
    setTagId("");
  };

  const handleDelete = tagId => async _ =>
    await deleteTag({
      variables: { _id: tagId },
      update: cache => {
        const cacheParams = { query: TAGS, variables: { spacesIds } };
        const { tags } = cache.readQuery(cacheParams);
        const updatedTags = [...tags];
        const index = updatedTags.findIndex(t => t._id === tagId);

        updatedTags.splice(index, 1);
        cache.writeQuery({
          ...cacheParams,
          data: { tags: updatedTags }
        });
      }
    });

  return (
    <div className="Tags" ref={wrapper} onClick={closePicker}>
      <Alert
        type="info"
        showIcon
        className="tag-info"
        message="Tags can be added when creating task"
      />
      {colorPickerPos.x > -1 && (
        <div
          className="tag-color-picker"
          onClick={e => e.stopPropagation()}
          style={{
            left: colorPickerPos.x,
            top: colorPickerPos.y
          }}
        >
          <TwitterPicker onChangeComplete={onColorChange} />
        </div>
      )}
      {tags.length === 0 && <Empty />}
      {tags.length > 0 &&
        tags.map(tag => {
          return (
            <div className="tag" key={tag._id}>
              <div className="tag-color">
                {
                  <span
                    onClick={onColorSpanClick(tag._id)}
                    className="color-span"
                    style={{ backgroundColor: tag.color }}
                  />
                }
              </div>
              <div className="tag-name">
                {/* <span>{tag.name}</span> */}
                <Tag className="example-tag" color={tag.color}>
                  {tag.name}
                </Tag>
              </div>
              <div className="tag-actions">
                <Icon
                  className="tag-delete"
                  type="delete"
                  onClick={handleDelete(tag._id)}
                />
              </div>
            </div>
          );
        })}
    </div>
  );
}
