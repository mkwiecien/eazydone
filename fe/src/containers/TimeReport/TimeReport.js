import React, { useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import { Link } from "@reach/router";
import { AutoComplete, DatePicker } from "antd";
import { TIME_REPORT } from "../../queries";

import "./TimeReport.css";
import { formatTime } from "../../utils";

export default function TimeReport(props) {
  const [selectedUsername, selectUsername] = useState("");
  const [dateRange, setDateRange] = useState([null, null]);
  const { user } = props;

  let companyUsers = [];
  user.companies.forEach(c => (companyUsers = companyUsers.concat(c.users)));

  const usernames = companyUsers.map(u => (
    <AutoComplete.Option key={u.username}>{u.username}</AutoComplete.Option>
  ));
  const selectedUser = companyUsers.find(u => u.username === selectedUsername);

  const [startDate, endDate] = dateRange;

  const { data, loading } = useQuery(TIME_REPORT, {
    variables: {
      userId: (selectedUser || {})._id,
      startDate: startDate && startDate.unix(),
      endDate: endDate && endDate.unix()
    },
    skip: !!!selectedUser || !!!startDate || !!!endDate,
    fetchPolicy: "network-only"
  });

  if (loading) return "Loading...";

  const timeReport = data && data.timeReport.filter(el => !!el.task);

  const totalMinutes =
    (timeReport &&
      timeReport.length > 0 &&
      timeReport.reduce((sum, el) => sum + el.minutes, 0)) ||
    0;

  return (
    <div className="TimeReport">
      <div className="time-report-header">
        <span className="prop-title">Select user: </span>
        <AutoComplete
          placholder="Select user"
          allowClear
          value={selectedUsername}
          onChange={val => selectUsername(val)}
          dataSource={usernames}
        />
      </div>
      <div className="time-report-header">
        <span className="prop-title">Select date range: </span>
        <DatePicker.RangePicker
          value={dateRange}
          onChange={val => setDateRange(val)}
        />
      </div>
      {selectedUser && startDate && endDate && timeReport && (
        <div className="log-entries">
          {timeReport.map(taskEntry => (
            <div key={taskEntry._id} className="log-entry">
              <span className="timelog" style={{ fontWeight: "bold" }}>
                {formatTime(taskEntry.minutes)}:{" "}
              </span>
              <Link className="task-link" to={`/tasks/${taskEntry._id}`}>
                {taskEntry.task.title}
              </Link>
            </div>
          ))}
          <div className="log-entry">
            <span style={{ fontWeight: "bold" }}>
              TOTAL: {formatTime(totalMinutes)}
            </span>
          </div>
        </div>
      )}
    </div>
  );
}
