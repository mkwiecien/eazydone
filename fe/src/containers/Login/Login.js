import React, { Component } from "react";
import PropTypes from "prop-types";
// import { Link } from "react-router-dom";
import { Link, navigate } from "@reach/router";
import { Form, Input, Button, Icon, notification } from "antd";
import { apiUrl } from "../../index";
import { validateEmail } from "../../utils";
import untitled from "../../logo.svg";
import "./Login.css";

export class MyLoginForm extends Component {
  static propTypes = {
    onLogin: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }

  componentDidMount() {
    fetch(`${apiUrl}logout`, {
      method: "POST",
      credentials: "include"
    }).catch(err => console.error(err));
  }

  postLogin = user => {
    this.setState({ loading: true });

    const data = {
      password: user.password
    };

    if (validateEmail(user.login)) {
      data.email = user.login;
    } else {
      data.username = user.login;
    }

    fetch(`${apiUrl}login`, {
      method: "POST",
      credentials: "include",
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      },
      body: JSON.stringify(data)
    })
      .catch(err => console.error(err))
      .then(response => {
        response.json().then(res => {
          this.setState({ loading: false });
          if (400 <= response.status && response.status <= 600) {
            notification["error"]({
              message: "Error",
              description: res.msg
            });
          } else {
            this.props.onLogin(res);
            // this.props.history.push("/");
            navigate("/");
          }
        });
      });
  };

  handleSubmit = (e, login) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.postLogin(values);
      }
    });
  };

  onForgotClick = () => navigate("/reset-password");

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <div className="Login">
        <img src={untitled} className="my-logo my-logo-login" alt="logo" />
        <Form className="login-form" onSubmit={this.handleSubmit}>
          <Form.Item>
            {getFieldDecorator("login", {
              rules: [
                { required: true, message: "Username or email is required." }
              ]
            })(
              <Input
                prefix={
                  <Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                placeholder="Username or email"
              />
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("password", {
              rules: [{ required: true, message: "Password is required." }]
            })(
              <Input
                prefix={
                  <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                placeholder="Password"
                type="password"
              />
            )}
          </Form.Item>
          <Button
            className="login-button"
            htmlType="submit"
            loading={this.state.loading}
          >
            Log in
          </Button>

          <Link to="/signin">
            <Button
              className="login-button signin-button"
              htmlType="submit"
              loading={this.state.loading}
            >
              Create account
            </Button>
          </Link>
        </Form>
        <p className="forgot-password">
          <Button onClick={this.onForgotClick}>I forgot my password</Button>
        </p>
      </div>
    );
  }
}

export default Form.create()(MyLoginForm);
