import React, { useState } from "react";
import { navigate } from "@reach/router";
import { Tooltip, Empty, Button, Modal, Select } from "antd";
import BoardColumn from "../../components/BoardColumn/BoardColumn";
import TaskForm from "../TaskForm/TaskForm";
import FilteredsTasks from "../FilteredTasks/FilteredTasks";
import MilestoneCompletedControl from "../../components/MilestoneCompletedControl/MilestoneCompletedControl";

import "./Board.css";

export default function Board(props) {
  const {
    selectedSpaceId,
    spaces,
    tasks: tasksFromProps,
    milestones,
    notifications,
    files,
    tags,
    filter,
    setFilter
  } = props;

  const milestone =
    (props.milestoneId
      ? milestones.find(m => m._id === props.milestoneId)
      : milestones[0]) || milestones[0];

  let mTasks = [...tasksFromProps].filter(t => t.spaceId === selectedSpaceId);
  mTasks = mTasks.filter(t => t.milestoneId === milestone._id);

  const [modalVisible, setModalVisible] = useState(false);

  if (milestones.length === 0) return <Empty description="No milestones." />;

  const filterFunc = (filter, el) => {
    const milestone = milestones.find(m => m._id === el.props.value);
    return milestone.name.toLowerCase().indexOf(filter.toLowerCase()) > -1;
  };

  const space = spaces.find(s => s._id === selectedSpaceId);

  return (
    <div className="Board">
      <Modal
        title={"Create task"}
        visible={modalVisible}
        onCancel={() => setModalVisible(false)}
        className="edit-task-modal"
        footer={null}
        destroyOnClose
        width={750}
      >
        <TaskForm
          {...props}
          files={files}
          mode={"create"}
          initialData={{
            milestoneId: milestone._id,
            spaceId: selectedSpaceId
          }}
          afterSubmit={() => setModalVisible(false)}
        />
      </Modal>
      <FilteredsTasks
        hideDivider
        disableMilestones
        filter={filter}
        setFilter={setFilter}
        tasks={mTasks}
        tags={tags}
        space={space}
        selectedSpaceId={selectedSpaceId}
        milestones={milestones}
        renderContent={(filteredTasks, changeOrderInState) => (
          <div>
            <div className="board-header">
              <div className="board-navigation">
                {milestone._id !== "backlog" && (
                  <MilestoneCompletedControl milestone={milestone} />
                )}
                <Tooltip title="Select milestone" mouseEnterDelay={0.7}>
                  <Select
                    showSearch
                    filterOption={filterFunc}
                    value={milestone._id}
                    className="board-milestone-select"
                    onChange={_id =>
                      navigate(`/tasks/board/${_id}`, {
                        replace: true
                      })
                    }
                  >
                    {milestones.map(m => (
                      <Select.Option key={m._id} value={m._id}>
                        {m.name}
                      </Select.Option>
                    ))}
                  </Select>
                </Tooltip>
              </div>
              <Button
                shape="circle"
                icon="plus"
                type="primary"
                onClick={() => setModalVisible(true)}
              />
            </div>
            <div className="board-body">
              {space.states.map(state => {
                const stateTasks = filteredTasks.filter(
                  t => t.state === state.text
                );
                return (
                  <BoardColumn
                    key={state.text}
                    tasks={stateTasks}
                    space={space}
                    state={state}
                    notifications={notifications}
                    tags={tags}
                    changeOrderInState={changeOrderInState}
                  />
                );
              })}
            </div>
          </div>
        )}
      />
    </div>
  );
}
