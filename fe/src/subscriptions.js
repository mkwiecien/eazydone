import { gql } from "apollo-boost";

export const ADDED_MESSAGE = gql`
  subscription onAddedMessage($username: String!, $spaces: [String!]!) {
    addedMessage(username: $username, spaces: $spaces) {
      _id
      text
      from
      to
    }
  }
`;

export const MEETING_UPDATED = gql`
  subscription MeetingUpdated($username: String!, $companyId: String!) {
    meetingUpdated(username: $username, companyId: $companyId) {
      _id
      name
      description
      startDate
      endDate
      private
      repetition
      companyId
      invitations {
        username
        state
      }
      invitedSpaces
      color
      createdBy
      updatedBy
    }
  }
`;

export const MEETING_DELETED = gql`
  subscription MeetingDeleted($companyId: String!) {
    meetingDeleted(companyId: $companyId) {
      _id
    }
  }
`;

export const TASK_UPDATED = gql`
  subscription TaskUpdated($spacesIds: [String]!, $username: String!) {
    taskUpdated(spacesIds: $spacesIds, username: $username) {
      _id
      title
      createdBy
      updatedBy
      description
      todos {
        text
        done
      }
      milestoneId
      spaceId
      files
      assignee
      state
      difficulty
      comments {
        text
        author
        date
      }
      deadline
      order
      history {
        text
        icon
        date
        diff
        updatedBy
      }
      tagsNames
    }
  }
`;

export const TASKS_UPDATED = gql`
  subscription TasksUpdated($spacesIds: [String]!) {
    tasksUpdated(spacesIds: $spacesIds) {
      _id
      milestoneId
      order
      history {
        text
        icon
        date
        diff
        updatedBy
      }
    }
  }
`;

export const TASK_DELETED = gql`
  subscription TaskDeleted($spacesIds: [String]!) {
    taskDeleted(spacesIds: $spacesIds) {
      _id
    }
  }
`;

export const NEW_NOTIFICATION = gql`
  subscription NewNotification($username: String!) {
    newNotification(username: $username) {
      _id
      title
      text
      link
      seen
      username
      important
    }
  }
`;

export const TYPING = gql`
  subscription Typing($username: String!, $spaces: [String!]!) {
    typing(username: $username, spaces: $spaces) {
      username
      to
    }
  }
`;

export const CHECKED_IN = gql`
  subscription CheckedIn($username: String!, $companyId: String!) {
    checkedIn(username: $username, companyId: $companyId) {
      username
      lastSeen
    }
  }
`;

export const MILESTONE_DELETED = gql`
  subscription MilestoneDeleted($spacesIds: [String]!) {
    milestoneDeleted(spacesIds: $spacesIds) {
      _id
    }
  }
`;
