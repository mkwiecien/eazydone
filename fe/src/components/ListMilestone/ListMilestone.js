import React from "react";
import lodash from "lodash";
import moment from "moment";
import { Link } from "@reach/router";
import { useMutation } from "@apollo/react-hooks";
import {
  Tooltip,
  Badge,
  Tag,
  Progress,
  Button,
  Icon,
  Collapse,
  List
} from "antd";
import ListTask from "../ListTask/ListTask";
import MilestoneCompletedControl from "../MilestoneCompletedControl/MilestoneCompletedControl";
import { getTaskProgress } from "../../utils";
import { CHANGE_ORDER } from "../../queries";

export default function ListMilestone(props) {
  const {
    milestone,
    tasks,
    notifications,
    space,
    addTaskToMilestone,
    onEditClick,
    deleteMilestoneHandler,
    user,
    tags,
    selectedCompanyId,
    editTaskHandler,
    deleteTaskHandler,
    updateState,
    changeOrderInState
  } = props;
  const mTasks = tasks.filter(t => t.milestoneId === milestone._id);
  const notificationsIds = notifications.map(
    n => n.link.match(/\/tasks\/(.*)$/)[1]
  );

  const [changeOrder] = useMutation(CHANGE_ORDER);

  const changeOrderOnServer = async _ => {
    await changeOrder({
      variables: {
        tasks: mTasks.map(t => ({
          _id: t._id,
          order: t.order,
          state: t.state
        }))
      }
    });
  };

  const getItemCount = item =>
    notificationsIds.filter(_id => _id === item._id).length;

  const renderMilestoneHeader = (mstone, tasks) => {
    const startDate = moment.unix(mstone.startDate).format("ll");
    const endDate = moment.unix(mstone.endDate).format("ll");

    const notificationsCount =
      tasks.length -
      lodash.difference(
        tasks.map(t => t._id),
        notifications.map(n => n.link.match(/\/tasks\/(.*)$/)[1])
      ).length;

    const firstState = space.states[0].text;
    const lastState = space.states[space.states.length - 1].text;

    const allCount = tasks.length;
    let inProgressTasks = tasks.filter(
      t => t.state !== firstState && t.state !== lastState
    );
    let inProgress = inProgressTasks.length;
    let doneTasks = tasks.filter(t => t.state === lastState);
    let done = doneTasks.length;

    const totalDifficulty = tasks.reduce((sum, t) => sum + t.difficulty, 0);

    const doneProgress = Math.round(
      (100 * doneTasks.reduce((sum, t) => sum + t.difficulty, 0)) /
        totalDifficulty
    );

    const totalProgress =
      Math.round(
        (100 *
          inProgressTasks.reduce(
            (sum, t) =>
              sum + t.difficulty * (getTaskProgress(t, space.states) / 100),
            0
          )) /
          totalDifficulty
      ) + doneProgress;

    const title = `
        ${done} done / ${inProgress} in progress / ${allCount -
      done -
      inProgress}
         left
      `;

    const filteredCount = tasks.length;

    return (
      <div className="milestone-header">
        <Tooltip title="modified">
          <Badge count={notificationsCount}></Badge>
        </Tooltip>
        <span style={{ marginLeft: 15 }} className="title">
          {mstone._id !== "backlog" && (
            <MilestoneCompletedControl milestone={mstone} />
          )}
          <span>{mstone.name}</span>
        </span>
        {filteredCount > 0 && (
          <Tag color="lime">{`${filteredCount} task${
            filteredCount === 1 ? "" : "s"
          }`}</Tag>
        )}
        {filteredCount === 0 && <Tag>{""}</Tag>}

        <span className="daterange">
          {mstone.startDate && mstone.endDate && (
            <span>
              {startDate} - {endDate}
            </span>
          )}
        </span>
        {mstone._id !== "backlog" && (
          <Tooltip title={title}>
            <Progress percent={totalProgress} successPercent={doneProgress} />
          </Tooltip>
        )}

        <div className="milestone-actions">
          <Button onClick={addTaskToMilestone(mstone._id)}>
            <Icon type="plus-circle" style={{ color: "green" }} /> Task
          </Button>
          {mstone._id !== "backlog" && (
            <Button onClick={onEditClick(mstone)}>Edit</Button>
          )}
          <Tooltip title="View as board">
            <Link to={`/tasks/board/${mstone._id}`}>
              <Button onClick={e => e.stopPropagation()} icon="table"></Button>
            </Link>
          </Tooltip>
          <Tooltip title="Delete milestone">
            <Button
              icon="delete"
              onClick={deleteMilestoneHandler(mstone._id, tasks)}
            ></Button>
          </Tooltip>
        </div>
      </div>
    );
  };

  return (
    <Collapse.Panel
      {...props}
      className="ListMilestone"
      header={renderMilestoneHeader(milestone, mTasks)}
      style={{ opacity: milestone.completed ? 0.5 : 1 }}
    >
      <List
        className="tasks-list"
        itemLayout="horitzontal"
        dataSource={mTasks}
        renderItem={item => {
          return (
            <ListTask
              item={item}
              user={user}
              tags={tags}
              selectedCompanyId={selectedCompanyId}
              space={space}
              editTaskHandler={editTaskHandler}
              deleteTaskHandler={deleteTaskHandler}
              getItemCount={getItemCount}
              updateState={updateState}
              changeOrder={changeOrderInState}
              changeOrderOnServer={changeOrderOnServer}
            />
          );
        }}
      />
    </Collapse.Panel>
  );
}
