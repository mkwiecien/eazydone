import React, { useState } from "react";
import { CardElement, injectStripe } from "react-stripe-elements";
import { Form, Input, Alert, Button } from "antd";
import { navigate } from "@reach/router";
import { handleError } from "../../utils";
import { apiUrl } from "../..";
import Loader from "../Loader/Loader";

import "./PremiumSignin.css";

function PremiumSignin(props) {
  const { user } = props;
  const [loading, setLoading] = useState(false);

  const getFieldDecorator = props.form.getFieldDecorator;
  const formItemLayout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 14 }
  };

  if (loading) return <Loader size={20} />;

  function onSubmit(event) {
    event.preventDefault();

    props.form.validateFields(async (err, values) => {
      if (!err) {
        const { confirm, ...data } = values;

        try {
          const { token, error } = await props.stripe.createToken({
            name: data.fullName
          });
          if (error) throw error;
          setLoading(true);
          data.stripeTokenId = token.id;
          data.username = user.username;
          const ress = await fetch(`${apiUrl}subscribe`, {
            method: "POST",
            credentials: "include",
            headers: {
              "Content-Type": "application/json; charset=utf-8"
            },
            body: JSON.stringify(data)
          });
          if (ress.status !== 200) {
            return handleError(ress);
          }
          // await ress.json();
          await fetch(`${apiUrl}logout`, {
            method: "POST",
            credentials: "include"
          });
          navigate("login");
          document.location.reload();
        } catch (err) {
          handleError(err);
          setLoading(false);
        }
      }
    });
  }

  return (
    <div className="payments-form PremiumSignin">
      <Alert
        type="info"
        className="billing-info"
        showIcon
        message={`
            You are subscribing to our service. You will receive a free 7-day
            trial to check if our software suits your needs. You will be able
            to cancel the subscription in profile settings in our app. After
            trial period you will be charged $3 for every active user. If you
            have any questions/feedback, please contact us: help@eazydone.com
          `}
        style={{ marginBottom: 15 }}
      />
      <Form onSubmit={onSubmit} {...formItemLayout}>
        <CardElement style={{ marginBottom: 5 }} />
        <Form.Item label="Company name">
          {getFieldDecorator("companyName")(
            <Input placeholder="Company name" />
          )}
        </Form.Item>

        {/* <Form.Item label="Full name">
          {getFieldDecorator("fullName", {
            rules: [{ required: true, message: "Full name is required." }]
          })(<Input placeholder="Full name" />)}
        </Form.Item> */}
        <Form.Item label="Address">
          {getFieldDecorator("address")(<Input placeholder="Address" />)}
        </Form.Item>
        <Form.Item wrapperCol={{ span: 12, offset: 6 }}>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}

export default injectStripe(Form.create()(PremiumSignin));
