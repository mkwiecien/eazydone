import React, { useState } from "react";
// import PropTypes from "prop-types";
import { useMutation } from "@apollo/react-hooks";
import { Modal, Tooltip, Button, List, Popconfirm, Avatar } from "antd";
import "./CrudList.css";
import Loader from "../Loader/Loader";

// static propTypes = {
//   deleteMutation: PropTypes.object.isRequired,
//   updateAfterDelete: PropTypes.func.isRequired,
//   shouldRender: PropTypes.func.isRequired,
//   className: PropTypes.string,
//   addButtonTooltip: PropTypes.string,
//   listData: PropTypes.array,
//   getInitialData: PropTypes.func,
//   canEdit: PropTypes.func
// };

// static defaultProps = {
//   updateAfterDelete: () => {},
//   shouldRender: () => null,
//   className: "",
//   addButtonTooltip: "Add item",
//   listData: [],
//   getInitialData: item => item,
//   canEdit: item => true
// };

export default function(props) {
  const {
    canEdit = _ => true,
    canDelete = _ => true,
    canAdd = true,
    updateAfterDelete,
    getInitialData = item => item
  } = props;
  const [modalMode, setModalMode] = useState(""); // 'create' / 'id_of_an_item' / ''
  const showModal = mode => () => setModalMode(mode);
  const hideModal = () => setModalMode("");
  const getModalTitle = () => (modalMode === "create" ? "Create" : "Edit");
  const getItemInitials = item => item.name.slice(0, 2).toUpperCase();

  const getData = () => {
    const item = props.listData.find(el => el._id === modalMode);
    return getInitialData(item);
  };

  const [deleteMutation, { loading }] = useMutation(props.deleteMutation, {
    update: updateAfterDelete
  });

  if (loading) return <Loader />;

  const { shouldRender = () => null } = props;
  const shouldRenderResult = shouldRender();
  if (shouldRenderResult) return shouldRenderResult;

  const className = `CrudList main-wrapper ${props.className}`;
  const {
    addButtonTooltip,
    listData,
    getActions = () => [],
    renderTitle = item => item.name,
    isItemSelected = _ => false
  } = props;

  return (
    <div className={className}>
      <Modal
        title={getModalTitle()}
        visible={!!modalMode}
        onCancel={hideModal}
        className={props.className}
        footer={null}
        destroyOnClose
        width={600}
      >
        {React.cloneElement(props.children, {
          mode: modalMode === "create" ? "create" : "edit",
          initialData: getData(),
          afterSubmit: modalMode !== "create" && hideModal
        })}
      </Modal>
      <div className="top-bar">
        {canAdd && (
          <Tooltip title={addButtonTooltip}>
            <Button
              shape="circle"
              icon="plus"
              type="primary"
              onClick={showModal("create")}
            />
          </Tooltip>
        )}
      </div>
      <div className="content">
        <List
          itemLayout="horizontal"
          dataSource={listData}
          renderItem={item => (
            <List.Item
              key={item._id}
              className={isItemSelected(item) ? "selected" : ""}
              actions={[
                ...getActions(item),
                canEdit(item) && (
                  <Button
                    onClick={showModal(item._id)}
                    type="ghost"
                    size="small"
                  >
                    Edit
                  </Button>
                ),
                canDelete(item) && (
                  <Popconfirm
                    title="Are you sure?"
                    onConfirm={() =>
                      deleteMutation({
                        variables: { _id: item._id },
                        update: updateAfterDelete
                      })
                    }
                  >
                    <Button size="small" type="danger">
                      Delete
                    </Button>
                  </Popconfirm>
                )
              ]}
            >
              <List.Item.Meta
                avatar={
                  <Avatar src={item.logo}>{getItemInitials(item)}</Avatar>
                }
                title={renderTitle(item)}
                description={item.description}
              />
            </List.Item>
          )}
        />
      </div>
    </div>
  );
}
