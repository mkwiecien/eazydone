import React, { useRef } from "react";
import moment from "moment";
import { useDrag, useDrop } from "react-dnd";
import { Link } from "@reach/router";
import { Avatar, List, Progress, Badge, Icon, Tag } from "antd";
import UserPopover from "../UserPopover/UserPopover";
import { getTaskProgress, renderState } from "../../utils";

export default function ListTask(props) {
  const {
    item,
    user,
    selectedCompanyId,
    space,
    editTaskHandler,
    deleteTaskHandler,
    getItemCount,
    tags,
    updateState,
    changeOrder,
    changeOrderOnServer
  } = props;
  const ref = useRef(null);
  const task = item;

  const [, drag] = useDrag({
    item: {
      _id: task._id,
      type: "task"
    },
    collect: monitor => ({
      isDragging: !!monitor.isDragging()
    })
  });

  const [, drop] = useDrop({
    accept: "task",
    async hover(dragTask, monitor) {
      if (!ref.current) return;
      if (dragTask._id === task._id) return;

      const { left, right, top, bottom } = ref.current.getBoundingClientRect();
      const clientOffset = monitor.getClientOffset();

      if (!clientOffset) return;

      const { x, y } = clientOffset;
      const hoversTheTask = left <= x && x <= right && top <= y && y <= bottom;

      if (!hoversTheTask) return;

      // From now on we know we hover over one specific task.

      const middleY = (bottom + top) / 2;
      if (y <= middleY) {
        changeOrder(dragTask, task, false);
      } else {
        changeOrder(dragTask, task, true);
      }
    },
    drop: async _ => {
      await changeOrderOnServer();
    }
  });

  function renderAssignee(username) {
    const company = user.companies.find(c => c._id === selectedCompanyId);
    const users = [...company.users, company.owner];
    const assignee = users.find(u => u.username === username);

    return (
      <UserPopover user={assignee} mouseEnterDelay={0.5}>
        <Avatar size="small" src={assignee.avatar}>
          {username.slice(0, 2)}
        </Avatar>
        <span style={{ marginLeft: 5 }}>{username}</span>
      </UserPopover>
    );
  }

  const renderTaskDescription = task => (
    <div>
      <p>{task.description}</p>
      {task.tagsNames && (
        <div className="tags-container">
          {task.tagsNames.map(tName => {
            const tag = tags.find(t => t.name === tName);
            if (!tag) return null;

            return (
              <Tag className="task-tag" key={tag._id} color={tag.color}>
                {tag.name}
              </Tag>
            );
          })}
        </div>
      )}
    </div>
  );

  drag(drop(ref));

  return (
    <div ref={ref}>
      <List.Item
        key={item._id}
        extra={
          <Progress
            type="circle"
            percent={getTaskProgress(item, space.states)}
            width={70}
          />
        }
        actions={[
          <Link className="view-action" to={`/tasks/${item._id}`}>
            view
          </Link>,
          <span
            className="view-action edit-action"
            onClick={editTaskHandler(item)}
          >
            edit
          </span>,
          <span className="view-action" onClick={deleteTaskHandler(item)}>
            delete
          </span>
        ]}
      >
        <List.Item.Meta
          title={
            <span>
              <Badge count={getItemCount(item)} />
              <span>
                <Link to={`/tasks/${item._id}`} state={{ title: item.title }}>
                  {item.title}{" "}
                </Link>
                {renderState(item, space, () => updateState(item))}
              </span>
            </span>
          }
          description={renderTaskDescription(item)}
        />
        <div className="task-item">
          <span>
            Assigned to:{" "}
            <span style={{ fontWeight: "bold" }}>
              {item.assignee && renderAssignee(item.assignee)}
            </span>
          </span>
          {item.deadline && (
            <span
              style={{
                color: item.deadline < moment().unix() ? "red" : "initial"
              }}
            >
              <Icon type="calendar" />
              {" " + moment.unix(item.deadline).format("ll")}
            </span>
          )}
        </div>
      </List.Item>
    </div>
  );
}
