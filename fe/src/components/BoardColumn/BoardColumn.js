import React from "react";
import { useDrop } from "react-dnd";
import { useMutation } from "@apollo/react-hooks";
import BoardTask from "../BoardTask/BoardTask";
import { CHANGE_ORDER, EDIT_TASK } from "../../queries";
import Loader from "../Loader/Loader";
import { Tag } from "antd";

export default function BoardColumn(props) {
  const {
    tasks: tasksFromProps,
    space,
    notifications,
    state,
    tags,
    changeOrderInState
  } = props;

  let tasks = tasksFromProps;
  tasks = tasks.sort((a, b) => a.order - b.order);

  const [changeOrder, { loading: changeOrderLoading }] = useMutation(
    CHANGE_ORDER
  );
  const [editTask, { loading: editTaskLoading }] = useMutation(EDIT_TASK);

  const [{ canDrop, isOver }, drop] = useDrop({
    accept: "task",
    drop: async (item, monitor) => {
      const hoveringAnotherTask = !monitor.isOver();

      if (hoveringAnotherTask) {
        await changeOrder({
          variables: {
            tasks: tasks.map(t => ({
              _id: t._id,
              order: t.order,
              state: t.state
            }))
          }
        });
        return;
      }
      if (item.state !== state.text) {
        await editTask({
          variables: {
            _id: item._id,
            state: state.text
          }
        });
        changeOrderInState(item, { state: state.text });
        return;
      }
    },
    collect: monitor => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop()
    })
  });
  const isActive = canDrop && isOver;
  let backgroundColor = "white";
  if (isActive) {
    backgroundColor = "rgba(0,0,0, 0.05";
  } else if (canDrop) {
    backgroundColor = "rgba(0,0,0, 0.01";
  }

  if (editTaskLoading || changeOrderLoading) return <Loader />;

  return (
    <div ref={drop} className="state-container" style={{ backgroundColor }}>
      <div className="state-header">
        <span>{state.text}</span>
        {tasks.length > 0 && (
          <Tag style={{ marginLeft: 5 }} color="lime">
            {tasks.length + " task"}
          </Tag>
        )}
      </div>
      <div className="state-content">
        {tasks.map(task => (
          <BoardTask
            key={task._id + "" + task.order}
            task={task}
            space={space}
            notifications={notifications}
            changeOrder={changeOrderInState}
            tags={tags}
          />
        ))}
      </div>
    </div>
  );
}
