import React, { Component } from "react";
import PropTypes from "prop-types";
import { Form, Input, Button, Icon, notification } from "antd";
import { navigate } from "@reach/router";
import { handleError } from "../../utils";
import "./SignInForm.css";

export class MySigninForm extends Component {
  static propTypes = {
    signIn: PropTypes.func,
    onSignIn: PropTypes.func,
    className: PropTypes.string,
    initialData: PropTypes.object,
    renderAdditionalFields: PropTypes.func
  };

  static defaultProps = {
    initialData: {},
    onSignIn: () => {},
    renderAdditionalFields: () => null
  };

  constructor(props) {
    super(props);
    this.state = {
      confirmDirty: false,
      loading: false
    };
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        const { confirm, ...data } = values;
        this.setState({ loading: true });

        try {
          const res = await this.props.signIn(data);
          if (res.status !== 200) {
            return handleError(res);
          }
          navigate("login");
          this.handleComplete();
        } catch (err) {
          handleError(err);
          this.setState({ loading: false });
        }
      }
    });
  };

  handleBlur = e =>
    this.setState({
      confirmDirty: this.state.confirmDirty || !!e.target.value
    });

  compareToFirstPassword = (rule, value, cb) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue("password")) {
      cb("Passwords don't match.");
    } else {
      cb();
    }
  };

  validateToNextPassword = (rule, value, cb) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(["confirm"], { force: true });
    }
    cb();
  };

  handleComplete = () => {
    notification["success"]({
      message: "Success",
      description: "Your account has been created."
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const className = `singin-form ${this.props.className}`;
    const { initialData } = this.props;

    const formItemLayout = {
      labelCol: { span: 8 },
      wrapperCol: { span: 14 }
    };

    return (
      <Form
        className={className}
        onSubmit={this.handleSubmit}
        labelAlign="left"
        {...formItemLayout}
      >
        <Form.Item label="Username">
          {getFieldDecorator("username", {
            rules: [{ required: true, message: "Username is required." }]
          })(
            <Input
              prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />}
              placeholder="Username"
            />
          )}
        </Form.Item>
        <Form.Item label="Email address">
          {getFieldDecorator("email", {
            rules: [
              { required: true, message: "Email is required." },
              { type: "email", message: "It's not a valid e-mail address." }
            ],
            initialValue: initialData.email
          })(
            <Input
              prefix={<Icon type="mail" style={{ color: "rgba(0,0,0,.25)" }} />}
              placeholder="Email"
            />
          )}
        </Form.Item>
        <Form.Item label="Password">
          {getFieldDecorator("password", {
            rules: [
              { required: true, message: "Password is required." },
              { validator: this.validateToNextPassword },
              {
                min: 7,
                message: "Password must containt minimum of 7 characters."
              }
            ]
          })(
            <Input
              prefix={<Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />}
              placeholder="Password"
              type="password"
            />
          )}
        </Form.Item>
        <Form.Item label="Repeat password">
          {getFieldDecorator("confirm", {
            rules: [
              { required: true, message: "Please confirm your password." },
              { validator: this.compareToFirstPassword }
            ]
          })(
            <Input
              prefix={<Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />}
              placeholder="Password"
              type="password"
              onBlur={this.handleBlur}
            />
          )}
        </Form.Item>
        {this.props.renderAdditionalFields(this.props)}
        <Button
          className="login-button"
          htmlType="submit"
          loading={this.state.loading}
        >
          Create account
        </Button>
      </Form>
    );
  }
}

export default Form.create()(MySigninForm);
