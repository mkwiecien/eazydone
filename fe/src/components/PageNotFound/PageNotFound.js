import React, { Component } from 'react';
import './PageNotFound.css';

export default class PageNotFound extends Component {
  render() {
    return (
      <div className='not-found'>
        Page not found.
      </div>
    )
  }
}
