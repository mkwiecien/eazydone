import React from "react";
import moment from "moment";
import { Popover, Avatar, Badge } from "antd";
import { LAST_SEEN_DELAY } from "../..";

import "./UserPopover.css";

export default function UserPopover(props) {
  const { user } = props;

  if (!user) return <div>{props.children}</div>;

  const isActive = moment().unix() - user.lastSeen < LAST_SEEN_DELAY;

  function renderTitle() {
    return (
      <span>
        <Badge color={isActive ? "green" : "red"} />
        <Avatar size="large" src={user.avatar}>
          {user.username.slice(0, 2)}
        </Avatar>
        <span style={{ marginLeft: 5, fontWeight: "bold" }}>
          {user.username}
        </span>
      </span>
    );
  }

  function renderContent() {
    return (
      <div className="user-popover-content">
        <div className="user-prop">
          <span className="prop-name">Full name: </span>
          <span className="prop-value">{`${user.name || ""} ${user.surname ||
            ""}`}</span>
        </div>
        <div className="user-prop">
          <span className="prop-name">Job title: </span>
          <span className="prop-value">{user.jobTitle}</span>
        </div>
      </div>
    );
  }

  return (
    <Popover {...props} title={renderTitle()} content={renderContent()}>
      {props.children}
    </Popover>
  );
}
