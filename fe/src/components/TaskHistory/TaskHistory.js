import React, { useMemo, useState } from "react";
import moment from "moment";
import { Timeline, Icon, Tooltip, Modal } from "antd";
import UserPopover from "../UserPopover/UserPopover";
import { idToDate } from "../../utils";

import "./TaskHistory.css";

export default function TaskHistory(props) {
  const { task, users = [] } = props;
  const [modalVisible, setModalVisible] = useState(false);

  const iconColors = useMemo(
    _ => ({
      "play-circle": "lightgreen",
      "user-add": "lightgreen"
    }),
    []
  );

  const history = [
    {
      text: "Task created",
      icon: "play-circle",
      date: idToDate(task._id).unix(),
      updatedBy: task.createdBy
    },
    ...(task.history || [])
  ];

  const getIcon = entry =>
    entry.icon ? (
      <Icon
        type={entry.icon}
        style={{ color: iconColors[entry.icon] || "unset" }}
      />
    ) : (
      ""
    );

  const getText = entry => {
    let text = entry.text;
    const blocks = text.split(/\s/).filter(el => !!el);

    return (
      <span>
        <span style={{ fontWeight: "bold" }}>{`${moment
          .unix(entry.date)
          .format("MM/D/YY")} `}</span>
        {blocks.map((block, index) => {
          if (block[0] === "@")
            return (
              <UserPopover
                key={index}
                user={users.find(u => u.username === block.slice(1))}
              >
                <span style={{ fontWeight: "bold" }}>{block + " "}</span>
              </UserPopover>
            );
          return <span key={index}>{block + " "}</span>;
        })}
      </span>
    );
  };

  const getKey = entry => `${entry.date}-${entry.text}`;

  const getUser = username => users.find(u => u.username === username);

  return (
    <div className="task-history">
      <Modal
        className="history-modal"
        visible={modalVisible}
        onCancel={_ => setModalVisible(false)}
        footer={null}
        destroyOnClose
        title="History"
        width="60%"
      >
        {history.map(entry => (
          <div className="modal-entry" key={getKey(entry)}>
            <span className="date">{moment.unix(entry.date).format("LL")}</span>
            <UserPopover user={getUser(entry.updatedBy)}>
              <span className="username">{entry.updatedBy}</span>
            </UserPopover>
            {entry.diff && entry.diff.length > 0 && (
              <div className="diff">
                <span>{entry.diff[0]}</span>
                <Icon type="double-right" />
                <span>{entry.diff[1]}</span>
              </div>
            )}
          </div>
        ))}
      </Modal>
      <span className="task-history-title">
        History{" "}
        <Tooltip title="Show more detailed difference">
          <Icon type="diff" onClick={_ => setModalVisible(true)} />
        </Tooltip>
      </span>
      <Timeline mode="alternate" className="task-history-timeline">
        {history.map(entry => (
          <Timeline.Item key={getKey(entry)} dot={getIcon(entry)}>
            {getText(entry)}
          </Timeline.Item>
        ))}
      </Timeline>
    </div>
  );
}
