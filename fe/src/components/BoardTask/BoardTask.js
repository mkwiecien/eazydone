import React, { useRef } from "react";
import moment from "moment";
import { useDrag, useDrop } from "react-dnd";
import { useMutation } from "@apollo/react-hooks";
import { Link } from "@reach/router";
import { Progress, Card, Badge, Icon, Avatar, Tag, Tooltip } from "antd";
import { EDIT_TASK } from "../../queries";
import { renderState, linkify } from "../../utils";
import UserPopover from "../UserPopover/UserPopover";
import Loader from "../Loader/Loader";

import "./BoardTask.css";

export default function BoardTask(props) {
  const { task, notifications, space, changeOrder = () => {}, tags } = props;
  const ref = useRef(null);

  const [editTask, { loading: editTaskLoding }] = useMutation(EDIT_TASK);

  const [{ isDragging }, drag] = useDrag({
    item: {
      _id: task._id,
      type: "task"
    },
    collect: monitor => ({
      isDragging: !!monitor.isDragging()
    })
  });

  const [, drop] = useDrop({
    accept: "task",
    async hover(dragTask, monitor) {
      if (!ref.current) return;
      if (dragTask._id === task._id) return;

      const { left, right, top, bottom } = ref.current.getBoundingClientRect();
      const clientOffset = monitor.getClientOffset();

      if (!clientOffset) return;

      const { x, y } = clientOffset;
      const hoversTheTask = left <= x && x <= right && top <= y && y <= bottom;

      if (!hoversTheTask) return;

      // From now on we know we hover over one specific task.

      const middleY = (bottom + top) / 2;
      if (y <= middleY) {
        changeOrder(dragTask, task, false);
      } else {
        changeOrder(dragTask, task, true);
      }
    }
  });

  if (editTaskLoding) return <Loader />;

  async function updateState(e, task) {
    e.preventDefault();
    const spacesStates = space.states;
    const index = spacesStates.findIndex(state => state.text === task.state);

    await editTask({
      variables: {
        _id: task._id,
        state: spacesStates[(index + 1) % spacesStates.length].text
      }
    });
  }

  const commentsCount = task.comments.length;

  const doneCount = task.todos.filter(t => t.done).length;
  const progress = Math.round((doneCount * 100) / task.todos.length);

  const notificationsIds = notifications.map(
    n => n.link.match(/\/tasks\/(.*)$/)[1]
  );

  const getItemCount = item =>
    notificationsIds.filter(_id => _id === item._id).length;

  function renderAssignee(username) {
    const assignee = space.users.find(u => u.username === username);
    if (!assignee) return null;

    return (
      <UserPopover user={assignee} mouseEnterDelay={0.5}>
        <Avatar size="small" src={assignee.avatar}>
          {username.slice(0, 2)}
        </Avatar>
        <span style={{ marginLeft: 5 }}>{username}</span>
      </UserPopover>
    );
  }

  drag(drop(ref));

  return (
    <div
      ref={ref}
      key={task._id}
      style={{ opacity: isDragging ? 0.5 : 1 }}
      className="BoardTask"
    >
      <Card
        title={
          <span>
            <Badge count={getItemCount(task)} />
            <Tooltip title={task.title}>
              <Link to={`/tasks/${task._id}`} style={{ marginLeft: 5 }}>
                {task.title}
                {renderState(task, space, e => updateState(e, task))}
              </Link>
            </Tooltip>
          </span>
        }
        size="small"
        bordered={false}
        // headStyle={{ backgroundColor: "#f0f5ff" }}
      >
        <div className="left-part">
          {task.description && <p>{linkify(task.description)}</p>}
          {!task.assignee &&
            commentsCount === 0 &&
            !task.deadline &&
            !task.description && (
              <span className="no-data">
                <Icon type="meh" />
                <span className="no-data-text">No data</span>
              </span>
            )}
          {task.assignee && renderAssignee(task.assignee)}
          {commentsCount > 0 && (
            <span>
              {commentsCount} {commentsCount === 1 ? "comment" : "comments"}
            </span>
          )}
          {task.deadline && (
            <span
              style={{
                color: task.deadline < moment().unix() ? "red" : "initial"
              }}
            >
              <Icon type="calendar" />
              {" " + moment.unix(task.deadline).format("ll")}
            </span>
          )}
          {task.tagsNames && (
            <div>
              {task.tagsNames.map(tName => {
                const tag = tags.find(t => t.name === tName);
                if (!tag) return null;

                return (
                  <Tag className="task-tag" key={tag._id} color={tag.color}>
                    {tag.name}
                  </Tag>
                );
              })}
            </div>
          )}
        </div>
        <Progress percent={progress} type="circle" width={50} />
      </Card>
    </div>
  );
}
