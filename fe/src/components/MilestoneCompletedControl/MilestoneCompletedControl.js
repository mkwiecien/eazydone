import React from "react";
import { useMutation } from "@apollo/react-hooks";
import { EDIT_MILESTONE } from "../../queries";
import { Tooltip, Icon } from "antd";

import "./MilestoneCompletedControl.css";

export default function(props) {
  const { milestone } = props;
  const [editMilestone] = useMutation(EDIT_MILESTONE);

  const onClick = async event => {
    event.preventDefault();
    event.stopPropagation();

    await editMilestone({
      variables: {
        _id: milestone._id,
        completed: !!!milestone.completed
      }
    });
  };

  return (
    <Tooltip
      className="MilestoneCompletedControl"
      title="Completed"
      mouseEnterDelay={0.5}
    >
      <Icon
        className={`complete-icon ${milestone.completed ? "complete" : ""}`}
        type="check-circle"
        style={{ marginRight: 5 }}
        onClick={onClick}
      />
    </Tooltip>
  );
}
