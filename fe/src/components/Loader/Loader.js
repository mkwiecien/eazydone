import React from "react";
import { Icon } from "antd";

import "./Loader.css";

export default function Loader(props) {
  const { size = 50 } = props;
  return (
    <Icon
      type="loading"
      color="darkblue"
      style={{ fontSize: size }}
      className="loader app-loader"
    />
  );
}
