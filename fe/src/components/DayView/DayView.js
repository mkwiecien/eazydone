import React from "react";
import moment from "moment";
import { Badge } from "antd";

import "./DayView.css";

export default function DayView(props) {
  const { events, spaces } = props;

  const getEventName = event => {
    const startDate = moment.unix(event.startDate);
    return `${startDate.format("HH:mm")} ${event.name}`;
  };

  const getSpaceName = spaceId => spaces.find(s => s._id === spaceId).name;

  return (
    <div className="DayView">
      {events.map(event => (
        <div
          className="event"
          key={event._id}
          style={{ backgroundColor: event.color }}
        >
          <span className="event-title">{getEventName(event)}</span>
          <span>{event.description}</span>
          {event.invitations && event.invitations.length > 0 && (
            <div className="invitations">
              {event.invitations.map(invitation => (
                <span key={invitation.username}>
                  {
                    <Badge
                      text={invitation.username}
                      title={invitation.state.toUpperCase()}
                      status={
                        invitation.state === "pending"
                          ? "processing"
                          : invitation.state === "accepted"
                          ? "success"
                          : "error"
                      }
                    />
                  }
                </span>
              ))}
            </div>
          )}
          <div className="invitations">
            {event.intvitedSpaces &&
              event.intvitedSpaces.length > 0 &&
              event.intvitedSpaces.map(spaceId => (
                <Badge key={spaceId} text={getSpaceName(spaceId)} />
              ))}
          </div>
        </div>
      ))}
    </div>
  );
}
