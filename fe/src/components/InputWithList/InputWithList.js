import React, { Component } from "react";
import PropTypes from "prop-types";
import { AutoComplete, Button, message } from "antd";
import { differenceWith, isEqual } from "lodash";
import InputWithListItem from "./InputWithListItem";
import "./InputWithList.css";

export default class InputWithList extends Component {
  static propTypes = {
    placeholder: PropTypes.string,
    addButtonCaption: PropTypes.string,
    validateFunc: PropTypes.func, // should return error string or null
    onChange: PropTypes.func,
    value: PropTypes.array,
    optionIcon: PropTypes.string,
    optionTooltip: PropTypes.string,
    onOptionClick: PropTypes.func,
    // prepare item for adding  -> str / { name: str, ... }
    getItem: PropTypes.func,
    renderItem: PropTypes.func,
    getItemCaption: PropTypes.func,
    editItemCaption: PropTypes.func,
    optionIconVisible: PropTypes.func,
    removeCaption: PropTypes.string,
    uniqueProp: PropTypes.string,
    enableEdit: PropTypes.bool
  };

  static defaultProps = {
    validateFunc: () => {},
    optionIcon: "",
    optionTooltip: "",
    onOptionClick: () => {},
    getItem: item => item,
    renderItem: item => item.title || item,
    optionIconVisible: () => false,
    removeCaption: "Remove user",
    uniqueProp: "",
    inputAutoComplete: [],
    filterOption: true,
    additionalButton: null,
    enableEdit: false
  };

  constructor(props) {
    super(props);
    this.state = {
      items: [],
      currItem: null,
      err: ""
    };
  }

  static getDerivedStateFromProps = (props, state) => {
    if (props.value) {
      const diff = differenceWith(props.value, state.items, isEqual);
      return {
        items:
          // props.value.length === state.items.length ? state.items : props.value
          diff.length === 0 ? state.items : props.value
      };
    }
    return null;
  };

  validate = () =>
    this.state.currItem && this.props.validateFunc(this.state.currItem);

  setCurrItem = item => this.setState({ currItem: item });

  addItem = () => {
    if (this.state.err) return;

    const err = this.validate();
    if (err) {
      this.setState({ err });
      return;
    }

    const newItem = this.props.getItem(this.state.currItem);

    if (!newItem) return;

    const { uniqueProp } = this.props;

    if (
      this.props.uniqueProp &&
      this.state.items.find(el => el[uniqueProp] === newItem[uniqueProp])
    ) {
      message.info("Such item already exists.");
      return;
    }

    const items = [...this.state.items, newItem];
    this.setState({
      items,
      currItem: null,
      err: ""
    });

    if (this.props.onChange) {
      this.props.onChange(items);
    }
  };

  onKeyDown = e => {
    if (e.key === "Enter") {
      e.preventDefault();
      e.stopPropagation();
      this.addItem();
    }
  };

  removeItem = index => () => {
    const updatedItems = this.state.items;
    updatedItems.splice(index, 1);

    this.setState({ items: updatedItems });

    if (this.props.onChange) {
      this.props.onChange(updatedItems);
    }
  };

  moveItem = (dragIndex, hoverIndex) => {
    const dragItem = this.state.items[dragIndex];
    let updatedItems = [...this.state.items];

    updatedItems.splice(dragIndex, 1);
    updatedItems.splice(hoverIndex, 0, dragItem);

    this.setState({
      ...this.state,
      items: updatedItems
    });
  };

  editItem = (index, item, newVal) => {
    if (
      this.props.uniqueProp &&
      this.state.items.find(el => el[this.props.uniqueProp] === newVal)
    ) {
      message.info("Such item already exists.");
      return;
    }

    const updatedItems = [...this.state.items];
    const { editItemCaption, onChange } = this.props;

    updatedItems[index] =
      (editItemCaption && editItemCaption(item, newVal)) || newVal;

    this.setState({
      ...this.state,
      items: updatedItems
    });

    if (onChange) onChange(updatedItems);
  };

  render() {
    const {
      placeholder,
      optionIcon,
      optionTooltip,
      onOptionClick,
      renderItem,
      getItemCaption,
      optionIconVisible,
      removeCaption,
      inputAutoComplete,
      onChange,
      additionalButton,
      enableEdit
    } = this.props;
    const { items, currItem, err } = this.state;
    const addButtonCaption = this.props.addButtonCaption || "Add item";

    return (
      <div className={`InputWithList ${err && "err"}`}>
        <div className="items">
          {items.map((item, index) => (
            <InputWithListItem
              key={index}
              item={item}
              index={index}
              optionTooltip={optionTooltip}
              optionIcon={optionIcon}
              onOptionClick={onOptionClick}
              renderItem={renderItem}
              getItemCaption={getItemCaption}
              optionIconVisible={optionIconVisible}
              editItem={this.editItem}
              removeCaption={removeCaption}
              removeItem={this.removeItem}
              moveItem={this.moveItem}
              onDrop={() => onChange && onChange(this.state.items)}
              enableEdit={enableEdit}
            />
          ))}
        </div>
        <div className="input-wrapper" onKeyDown={this.onKeyDown}>
          <AutoComplete
            placeholder={placeholder}
            value={currItem}
            onChange={this.setCurrItem}
            onBlur={() => this.setState({ err: this.validate() })}
            dataSource={inputAutoComplete}
            filterOption={this.props.filterOption}
          />
          {err && <span className="err">{err}</span>}
        </div>
        <Button
          type="default"
          size="small"
          disabled={!currItem || err}
          onClick={this.addItem}
        >
          {addButtonCaption}
        </Button>
        {additionalButton}
      </div>
    );
  }
}
