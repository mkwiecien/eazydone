import React, { useRef, useState, useEffect } from "react";
import { Tooltip, Icon, Input } from "antd";
import { useDrag, useDrop } from "react-dnd";

export default function InputWithListItem(props) {
  let {
    item,
    index,
    optionTooltip = "",
    optionIcon = "",
    onOptionClick,
    removeCaption = "",
    removeItem = () => {},
    renderItem,
    getItemCaption,
    optionIconVisible,
    moveItem = () => {},
    onDrop = () => {},
    editItem,
    enableEdit
  } = props;

  getItemCaption = getItemCaption || renderItem;

  const [editMode, setEditMode] = useState(false);

  const ref = useRef(null);
  const inputRef = useRef(null);

  useEffect(() => {
    if (editMode) inputRef.current.focus();
  }, [editMode]);

  const [{ isDragging }, drag] = useDrag({
    item: { type: "input-item", index },
    collect: monitor => ({
      isDragging: !!monitor.isDragging()
    })
  });

  const [, drop] = useDrop({
    accept: "input-item",
    hover(item, monitor) {
      if (!ref.current) return;

      const dragIndex = item.index;
      const hoverIndex = index;

      if (dragIndex === hoverIndex) return;

      const hoverBoundingRect = ref.current.getBoundingClientRect();
      const hoverMiddleY =
        (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;
      const clientOffset = monitor.getClientOffset();
      const hoverClientY = clientOffset.y - hoverBoundingRect.top;

      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) return;
      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) return;

      moveItem(dragIndex, hoverIndex);

      item.index = hoverIndex;
    },
    drop: onDrop
  });

  drag(drop(ref));

  function onInputBlur() {
    const newValue = inputRef.current.state.value;
    if (newValue !== getItemCaption(item)) editItem(index, item, newValue);
    setEditMode(false);
  }

  const onKeyDown = event => {
    if (event.key === "Enter" || event.key === "Escape") {
      event.preventDefault();
      event.stopPropagation();
      document.activeElement.blur();
    }
  };

  return (
    <div
      className="item"
      key={index}
      ref={ref}
      style={{ opacity: isDragging ? 0.7 : 1 }}
    >
      {!editMode && (
        <span className="item-name">{renderItem(item, index)}</span>
      )}
      {enableEdit && editMode && (
        <Input
          ref={inputRef}
          className="item-input"
          defaultValue={getItemCaption(item)}
          onBlur={onInputBlur}
          onKeyDown={onKeyDown}
        />
      )}
      {optionIconVisible(item) && (
        <Tooltip title={optionTooltip} className="option">
          <Icon type={optionIcon} onClick={() => onOptionClick(item)} />
        </Tooltip>
      )}

      {enableEdit && (
        <Tooltip title="Edit item">
          <Icon
            type="edit"
            className="edit-icon option"
            onClick={_ => setEditMode(true)}
          />
        </Tooltip>
      )}
      <Tooltip title={removeCaption} className="remove-item">
        <Icon
          className="remove-icon"
          type="minus-circle"
          onClick={removeItem(index)}
        />
      </Tooltip>
    </div>
  );
}
