import React, { Component } from "react";
import PropTypes from "prop-types";
import { message, Tooltip, Icon, Input, Button, Tag } from "antd";
import "./StateInput.css";

export default class StateInput extends Component {
  static propTypes = {
    placeholder: PropTypes.string,
    addButtonCaption: PropTypes.string,
    validateFunc: PropTypes.func, // should return error string or null
    onChange: PropTypes.func,
    value: PropTypes.array,
    // prepare item for adding  -> str / { name: str, ... }
    getItem: PropTypes.func,
    removeCaption: PropTypes.string,
    uniqueProp: PropTypes.string
  };

  static defaultProps = {
    validateFunc: () => {},
    getItem: item => item,
    removeCaption: `
      All the tasks with removed status will be updated with a first available
      status.
    `,
    uniqueProp: "text",
    placeholder: "Type status name",
    addButtonCaption: "Add status"
  };

  constructor(props) {
    super(props);
    this.state = {
      items: [],
      currText: "",
      currColor: "blue",
      err: ""
    };
  }

  static getDerivedStateFromProps = (props, state) => {
    if (props.value) {
      return {
        items: props.value
      };
    }
    return null;
  };

  validate = () =>
    this.setState({
      err: this.state.currItem && this.props.validateFunc(this.state.currItem)
    });

  setCurrText = e => this.setState({ currText: e.target.value });

  setCurrColor = color => () => this.setState({ currColor: color });

  addItem = () => {
    if (this.state.err) return;

    const newItem = { text: this.state.currText, color: this.state.currColor };
    const { uniqueProp } = this.props;

    if (
      this.props.uniqueProp &&
      this.state.items.find(el => el[uniqueProp] === newItem[uniqueProp])
    ) {
      message.info("Such state already exists.");
      return;
    }

    const items = [...this.state.items, newItem];
    this.setState({
      items,
      currText: "",
      // currColor: "",
      err: ""
    });

    if (this.props.onChange) {
      this.props.onChange(items);
    }
  };

  onKeyDown = e => {
    if (e.key === "Enter") {
      e.preventDefault();
      this.addItem();
    }
  };

  removeItem = index => () => {
    if (this.state.items.length === 1) {
      message.warn("There has to be at least one status");
      return;
    }
    const updatedItems = this.state.items;
    updatedItems.splice(index, 1);

    this.setState({ items: updatedItems });

    if (this.props.onChange) {
      this.props.onChange(updatedItems);
    }
  };

  renderColor = color => (
    <Tag
      key={color}
      onClick={this.setCurrColor(color)}
      color={color}
      className={this.state.currColor === color ? "selected" : ""}
    >
      {this.state.currColor === color && <Icon type="check" />}
    </Tag>
  );

  render() {
    const { placeholder, removeCaption, addButtonCaption } = this.props;
    const { items, currText, currColor, err } = this.state;

    return (
      <div className={`StateInput ${err && "err"}`}>
        <div className="items">
          {items.map((item, index) => (
            <div className="item" key={index}>
              <span className="item-color">
                <Tag color={item.color} />
              </span>
              <span className="item-name">{item.text}</span>
              <Tooltip title={removeCaption} className="remove-item">
                <Icon
                  className="remove-icon"
                  type="minus-circle"
                  onClick={this.removeItem(index)}
                />
              </Tooltip>
            </div>
          ))}
        </div>
        <div className="input-wrapper">
          <Input
            placeholder={placeholder}
            allowClear
            value={currText}
            onChange={this.setCurrText}
            onBlur={this.validate}
            onKeyDown={this.onKeyDown}
          />
          {err && <span className="err">{err}</span>}

          <div className="colors">
            {[
              "magenta",
              "red",
              "volcano",
              "orange",
              "gold",
              "lime",
              "green",
              "cyan",
              "blue",
              "geekblue",
              "purple"
            ].map(this.renderColor)}
          </div>
        </div>
        <Button
          type="default"
          size="small"
          disabled={!currText || !currColor || err}
          onClick={this.addItem}
        >
          {addButtonCaption}
        </Button>
      </div>
    );
  }
}
