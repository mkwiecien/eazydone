import { apiUrl } from 'index';
;
const getUrl = endpoint => `${apiUrl}/${endpoint}`;

const handleResponse = r => {
  console.log('r.status: ', r.status);
  if (400 <= r.status && r.status <=600) {
    handleError(r);
  }
  return r.json();
}

handleError = r => {
  let err = r.error || r.errors[0] || err;
  err = err.message || err.toString();
  console.log('found err:', err);
}


// graphql
export default (query, variables) => fetch(
    getUrl('graphql'),
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({ query, variables })
    }
)
  .then(handleResponse)
  .then(r => {
    console.log('r: ', r);    
    if (r.errors) {
      handleError(r);
    }
  })
  .catch(err => {
    handleError(r);
    return err;
  })

