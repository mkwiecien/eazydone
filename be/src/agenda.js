const Agenda = require("agenda");

const DB = process.env.DB;

const agenda = new Agenda({ db: { address: DB, collection: "agenda" } });

module.exports = agenda;
