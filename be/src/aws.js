// s3
const AWS = require("aws-sdk");
const bluebird = require("bluebird");

// s3
AWS.config.setPromisesDependency(bluebird);
const s3 = new AWS.S3({
  // signatureVersion: "v4",
  region: "eu-west-3",
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});

const uploadFile = (buffer, name, type, acl = "public-read") => {
  const params = {
    ACL: acl,
    Body: buffer,
    Bucket: process.env.S3_BUCKET,
    ContentType: type.mime,
    Key: `${name}.${type.ext}`
  };
  return s3.upload(params).promise();
};

const deleteFile = key => {
  const params = {
    Bucket: process.env.S3_BUCKET,
    Key: key
  };
  return s3.deleteObject(params).promise();
};

const getFileUrl = file => {
  return s3.getSignedUrl("getObject", {
    Bucket: process.env.S3_BUCKET,
    Expires: 5 * 60, // in seconds (5 minutes)
    Key: file.awsKey
  });
};

module.exports = { uploadFile, deleteFile, getFileUrl };
