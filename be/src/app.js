const path = require("path");
const mongoose = require("mongoose");
const moment = require("moment");
const lodash = require("lodash");
const express = require("express");
const cors = require("cors");
const expressGraphQL = require("express-graphql");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const expressValidator = require("express-validator");
const schedule = require("node-schedule");
const nodemailer = require("nodemailer");
const jwt = require("jwt-simple");

const { createServer } = require("http");
const { SubscriptionServer } = require("subscriptions-transport-ws");
const { execute, subscribe } = require("graphql");
const request = require("request");

const MilestoneProgress = require("./models/milestoneProgress.model");
const Tasks = require("./models/task.model");
const User = require("./models/user.model");
const File = require("./models/file.model");
const Space = require("./models/space.model");
const Company = require("./models/company.model");

const UserResolver = require("./graphql/resolvers/user.resolver");

require("dotenv").config({ path: path.join(__dirname, "../../fe/.env") });

const stripe = require("stripe")(process.env.STRIPE_SECRET);

// s3
const fs = require("fs");
const fileType = require("file-type");
const multiparty = require("multiparty");
const { uploadFile, deleteFile, getFileUrl } = require("./aws");

const schema = require("./graphql/");
const auth = require("./auth");

const app = express();
const PORT = process.env.port || 4000;

const agenda = require("./agenda");
agenda.start();

const DB = process.env.DB;

mongoose
  .connect(DB, { useCreateIndex: true, useNewUrlParser: true })
  .then(() => console.log("MongoDB connected"));

// measure the usage
schedule.scheduleJob("0 15 * * *", async function() {
  try {
    let owners = await Company.aggregate([
      { $group: { _id: "$ownerId", sum: { $sum: { $size: "$accounts" } } } },
      { $addFields: { userId: { $toObjectId: "$_id" } } },
      {
        $lookup: {
          from: "users",
          localField: "userId",
          foreignField: "_id",
          as: "user"
        }
      }
    ]).exec();

    console.log("owners: ", owners);

    const timestamp = moment().unix();
    const promises = owners.map(owner => {
      if (owner.stripeSubscriptionId) {
        stripe.subscriptionItems.createUsageRecord(owner.stripeSubscriptionId, {
          quantity: sum,
          // quantity: owner.accounts.length,
          timestamp
        });
      }
    });

    await Promise.all(promises);
  } catch (err) {
    console.error(err);
  }
});

schedule.scheduleJob("1 0 * * *", async function() {
  try {
    let milestones = await Tasks.aggregate([
      { $match: { milestoneId: { $ne: "backlog" } } },
      {
        $group: {
          _id: "$milestoneId",
          tasks: { $push: "$$ROOT" },
          spaceId: { $first: "$spaceId" }
        }
      },
      { $addFields: { spaceIdP: { $toObjectId: "$spaceId" } } },
      {
        $lookup: {
          from: "spaces",
          localField: "spaceIdP",
          foreignField: "_id",
          as: "space"
        }
      }
    ]).exec();

    // array of objects { milestoneId, progress }
    const res = [];

    milestones.forEach(milestone => {
      const { _id, tasks } = milestone;
      const totalDifficulty = tasks.reduce((sum, t) => sum + t.difficulty, 0);
      const space = milestone.space[0];
      const lastState = space.states[space.states.length - 1].text;

      // for now I am only checking doneProgress
      const doneTasks = tasks.filter(t => t.state === lastState);
      const doneProgress = Math.round(
        (100 * doneTasks.reduce((sum, t) => sum + t.difficulty, 0)) /
          totalDifficulty
      );
      if (doneProgress > 0) {
        res.push({ milestoneId: _id, progress: doneProgress });
      }
    });
    await MilestoneProgress.insertMany(res);
  } catch (err) {
    console.error(err);
  }
});

app.use(
  bodyParser.json({ limit: "50mb", type: "application/json" }),
  cookieParser(),
  cors({
    credentials: true,
    origin: process.env.FE_URL.slice(0, process.env.FE_URL.length - 1)
  }),
  expressValidator(),
  auth.initialize(),
  (req, res, next) => {
    res.header("Content-Type", "application/json; charset=utf-8");

    const excludedPaths = [
      "login",
      "logout",
      "signin",
      "reset-password",
      "pics" // this one has to go, it should be authenticated!
    ];

    if (excludedPaths.some(path => req.path.includes(path))) {
      return next();
    }

    return auth.authenticate((err, user, info) => {
      if (err) return next(err);
      if (!user) {
        if (info.name === "TokenExpiredError") {
          return res.status(401).json({
            msg: "Your token has expired"
          });
        }
        return res.status(401).json({ msg: info.message });
      }
      app.set("user", user);
      req.user = user;
      return next();
    })(req, res, next);
  }
);

app.use(
  "/graphql",
  expressGraphQL({
    schema,
    graphiql: true
  })
);

app.post("/login", auth.login);
app.post("/signin", auth.signin);
app.post("/subscribe", auth.subscribe);
app.post("/logout", (req, res) => {
  res.clearCookie("jwt");
  return res.status(200).send({ msg: "Logged out" });
});

app.post("/pics", async (req, res) => {
  const form = new multiparty.Form();

  form.parse(req, async (error, fields, files) => {
    if (error) throw new Error(error);

    try {
      const path = files.logo[0].path;
      const buffer = fs.readFileSync(path);
      const type = fileType(buffer);

      if (type.ext === "xml") {
        type.ext = "svg";
        type.mime = "image/svg+xml";
      }
      const timestamp = Date.now().toString();
      const fileName = `logos/${timestamp}-lg`;
      const data = await uploadFile(buffer, fileName, type);
      return res.status(200).send(data);
    } catch (err) {
      console.error(err);
      return res.status(400).send(err);
    }
  });
});

const canUserPostFile = async (user, spaceId, fileSize) => {
  const space = await Space.find({ _id: spaceId })
    .lean()
    .exec();
  let companyQuery = space.companyId
    ? { _id: space.companyId }
    : { ownerId: user._id.toString() };

  const company = await Company.findOne(companyQuery)
    .lean()
    .exec();
  const limitInMb = (company && company.storagePerUserMb) || 200;

  // sum up size and compare if less than 10gb
  const res = await File.aggregate([
    {
      $match: { uploadedBy: user.username }
    },
    {
      $group: {
        _id: "$uploadedBy",
        size: { $sum: "$size" }
      }
    }
  ]);
  const diskSpaceTaken =
    res.length > 0 && Math.floor(res[0].size / 1024 / 1024) + fileSize;
  return diskSpaceTaken <= limitInMb;
};

// check if user's username is in files space or space can be public
const canUserAccessFile = async (user, file) => {
  const { username } = user;
  const space = await Space.findOne({ _id: file.spaceId })
    .lean()
    .exec();

  if (!space.private) return true;

  return space.accounts.includes(username) || space.ownerId === user._id;
};

app.get("/files/:fileId", async (req, res) => {
  const fileId = req.params.fileId;
  const file = await File.findOne({ _id: fileId })
    .lean()
    .exec();
  const isOk = await canUserAccessFile(req.user, file);
  if (!isOk) return res.status(403).send({ message: "Permission denied." });
  const url = await getFileUrl(file);
  request(url).pipe(res);
});

app.post("/files", async (req, res) => {
  const form = new multiparty.Form();

  form.parse(req, async (error, { spaceId }, files) => {
    if (error) throw new Error(error);

    try {
      const path = files.file[0].path;

      const size = fs.statSync(path).size / 1000000.0;
      console.log("size: ", size);
      const isOk = await canUserPostFile(req.user, spaceId, size);
      if (!isOk) {
        return res.status(500).send({ message: "Not enough free space" });
      }

      const buffer = fs.readFileSync(path);
      const type = fileType(buffer);

      if (type.ext === "xml") {
        type.ext = "svg";
        type.mime = "image/svg+xml";
      }

      const timestamp = Date.now().toString();
      const fileName = `storage/${timestamp}-file`;
      const data = await uploadFile(
        buffer,
        fileName,
        type,
        "bucket-owner-full-control"
      );

      // here save File documents to mongo
      const file = files.file[0];

      const dbFile = {
        url: data.Location,
        name: file.originalFilename,
        uploadedBy: req.user.username,
        size: file.size,
        spaceId: spaceId[0],
        awsKey: data.Key
      };
      const newFile = new File(dbFile);
      const savedFile = await newFile.save();

      return res.status(200).send(savedFile);
    } catch (err) {
      console.error(err);
      return res.status(400).send(err);
    }
  });
});

app.delete("/files", async (req, res) => {
  try {
    const file = await File.findOne({ _id: req.body._id })
      .lean()
      .exec();

    // 1. remove file from s3
    await deleteFile(file.awsKey);

    // 2. remove file from db
    await File.deleteOne({ _id: req.body._id });

    // 3. send response with id so fe can update
    res.status(200).send(req.body);
  } catch (err) {
    res.status(500).send(err);
  }
});

const sendEmail = (email, token, errorCb = () => {}) => {
  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: "help@eazydone.com",
      pass: "2<2&Cqju"
    }
  });
  const mailOptions = {
    from: "help@eazydone.com",
    to: email,
    subject: "eazydone: password reset",
    html: `
        <div style="width: 100%; height: 70px; background-color: white;">
          <img style="width: 150px; margin: auto;" src="https://s3.eu-west-3.amazonaws.com/www.eazydone.com/logo.svg"/>
        </div>
        <div style="font-family: Arial;">
          <p style="">
            Click 
            <a href="${process.env.FE_URL}reset-password/${token}">here</a> 
            to reset the password.
          </p>
          <br>
          <p> Thanks,</p>
          <p>eazdydone Team</p>
        </div>
      `
    // html: `
    //     <div style="width: 100%; height: 70px; background-color: #001529;">
    //       <img style="filter: invert(1); width: 150px;" src="https://s3.eu-west-3.amazonaws.com/www.eazydone.com/logo.svg"/>
    //     </div>
    //     <div style="font-family: Arial;">
    //       <p style="">
    //         Click
    //         <a href="${process.env.API_URL}reset/${token}">here</a>
    //         to reset the password.
    //       </p>
    //       <br>
    //       <p> Thanks,</p>
    //       <p>eazdydone team</p>
    //     </div>
    //   `
  };

  transporter.sendMail(mailOptions, (err, info) => {
    if (err) {
      errorCb();
    }
  });
};

app.post("/reset-password", async (req, res, next) => {
  // should come in body
  const email = req.body.email;

  let user = null;

  try {
    user = await User.findOne({ email });
  } catch (err) {
    return res.status(400).send(err);
  }

  if (!user) {
    return res.status(404).send("User not found");
  }

  const token = jwt.encode(
    {
      email,
      password: user.password
    },
    process.env.JWT_SECRET
  );

  sendEmail(email, token, () =>
    res.status(500).send(`Error while sending email to: ${email}`)
  );

  res.status(200).send("ok");
});

app.post(`/reset-password/:token`, async (req, res, next) => {
  const newPassword = req.body.newPassword;
  let tokenPayload = null;

  try {
    tokenPayload = jwt.decode(req.params.token, process.env.JWT_SECRET);
  } catch (err) {
    return res.status(401).send("Invalid token");
  }

  let user = null;

  try {
    user = await User.findOne({ email: tokenPayload.email });
  } catch (err) {
    return next(err);
  }

  if (newPassword && tokenPayload && user.password === tokenPayload.password) {
    try {
      await UserResolver.Mutation.editUser(null, {
        username: user.username,
        password: newPassword
      });

      return res.status(200).json({ status: "success", email: user.email });
    } catch (err) {
      return next(err);
    }
  } else {
    return next(new Error("Invalid token."));
  }
});

app.delete("/cancel", async (req, res, next) => {
  try {
    const { username, stripeSubscriptionId } = await User.findOne({
      _id: req.user._id
    });

    await UserResolver.Mutation.editUser(null, {
      username,
      stripeSubscriptionId: ""
    });

    // update all companies limits
    await Company.update(
      { ownerId: req.user._id },
      { $set: { usersCountLimit: 5, storagePerUserMb: 200 } },
      { multi: true }
    ).exec();

    let response = await stripe.subscriptions.del(stripeSubscriptionId);

    res.status(200).send(response);
  } catch (err) {
    return next(new Error("Cancelation failed. Please contact us via email."));
  }
});

app.use(function(err, req, res, next) {
  console.error(err.message);
  if (err.code === 11000 && err.name === "MongoError") {
    return res.status(err.status.code).send("Duplicate username or email");
  }

  return res.status(err.statusCode || 500).send({ message: err.message });
});

app.listen(PORT, () => console.log(`Server is running on port ${PORT}`));

const WS_PORT = 1337;

const websocketServer = createServer((request, response) => {
  response.writeHead(404);
  response.end();
});

// Bind it to port and start listening
websocketServer.listen(WS_PORT, () =>
  console.log(`Websocket Server is now running on port ${WS_PORT}`)
);

const subscriptionServer = SubscriptionServer.create(
  {
    schema,
    execute,
    subscribe
  },
  {
    server: websocketServer,
    path: "/graphql"
  }
);

// (async () => {
//   const paymentIntent = await stripe.paymentIntents.create({
//     amount: 999,
//     currency: "pln",
//     payment_method_types: ["card"],
//     receipt_email: "jenny.rosen@example.com"
//   });

//   console.log("paymentIntent: ", paymentIntent);
// })();

// creating a stripe product
// (async () => {
//   const product = await stripe.products.create({
//     name: "eazydone",
//     type: "service"
//   });

//   console.log("product: ", product);
// })();

// create plan
// (async () => {
//   const plan = await stripe.plans.create({
//     product: "prod_FktwUvnLvmGSIb",
//     nickname: "eazydone subscription granular",
//     currency: "usd",
//     interval: "month",
//     amount: 300,
//     usage_type: "metered",
//     aggregate_usage: "last_during_period"
//   });

//   console.log("plan: ", plan);
// })();
