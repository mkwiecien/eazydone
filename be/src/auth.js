const passport = require("passport");
const jwt = require("jwt-simple");
const moment = require("moment");
const passportJwt = require("passport-jwt");
const stripe = require("stripe")(process.env.STRIPE_SECRET);

const User = require("./models/user.model");
const Space = require("./models/space.model");
const Company = require("./models/company.model");

const getUserQuery = data =>
  data.username ? { username: data.username } : { email: data.email };

const getStrategy = () => {
  const params = {
    secretOrKey: process.env.JWT_SECRET,
    jwtFromRequest: req => (req && req.cookies.jwt ? req.cookies.jwt : null),
    passReqToCallback: true
  };

  return new passportJwt.Strategy(params, async (req, payload, done) => {
    try {
      const user = await User.findOne(getUserQuery(payload));
      if (user === null) {
        done(null, false, {
          message: "The user in the token was not found"
        });
      } else {
        done(null, {
          _id: user._id,
          username: user.username,
          email: user.email
        });
      }
    } catch (err) {
      done(err);
    }
  });
};

const initialize = () => {
  passport.use("jwt", getStrategy());
  return passport.initialize();
};

const authenticate = cb =>
  passport.authenticate("jwt", { session: false, failWithError: true }, cb);

const genToken = user => {
  const expires = moment()
    .add(7, "days")
    .valueOf();
  const token = jwt.encode(
    {
      exp: expires,
      username: user.username,
      email: user.email
    },
    process.env.JWT_SECRET
  );

  return {
    token,
    expires: moment.unix(expires).format(),
    user: user.username
  };
};

const login = (req, res, next) =>
  authenticate(async (authErr, authUser) => {
    if (authUser) {
      return res.status(200).send(authUser);
    }

    req.checkBody("password", "Invalid password").notEmpty();

    if (!req.body.username && !req.body.password) {
      return res.status(401).send({ msg: "You are not logged in" });
    }

    const errors = req.validationErrors();

    if (errors) return next(new Error(errors[0].msg));

    try {
      const user = await User.findOne(getUserQuery(req.body));
      if (user === null) return next(new Error("User not found"));

      let passwordMatch = false;
      if (user.active) {
        passwordMatch = await user.comparePassword(req.body.password);
      } else {
        passwordMatch = req.body.password === user.password;
      }

      if (!passwordMatch) return next(new Error("Incorrect password."));

      const token = genToken(user);
      res.cookie("jwt", token.token, {
        maxAge: 3600000 * 24 * 7,
        httpOnly: true
      });
      return res.status(200).json({
        username: user.username,
        email: user.email,
        lastSelectedSpaceId: user.lastSelectedSpaceId
      });
    } catch (err) {
      return next(err);
    }
  })(req, res, next);

const signin = async (req, res, next) => {
  try {
    const exists = await User.findOne({
      $or: [{ username: req.body.username }, { email: req.body.email }]
    });

    if (!!exists) {
      return next(new Error("Username or email is taken."));
    }

    const userData = {
      ...req.body
    };

    const newUser = new User(userData);

    // const paymentStatus = latest_invoice.payment_intent.status;
    // const paymentStatus = "";
    // const subscriptionStatus = status;

    // console.log("paymentStatus: ", la);
    // console.log("latest invoice: ", latest_invoice);
    // console.log("subsscriptionStatus: ", status);

    // if (subscriptionStatus === "active" && paymentStatus === "succeeded") {
    //   userData.paymentStatus = "success";
    // } else if (subscriptionStatus === "trialing") {
    //   userData.paymentStatus = "success";
    // } else if (
    //   subscriptionStatus === "incomplete" &&
    //   paymentStatus === "requires_payment_method"
    // ) {
    //   userData.paymentStatus = "fail";
    // } else if (
    //   subscriptionStatus === "incomplete" &&
    //   paymentStatus === "requires_action"
    // ) {
    //   userData.paymentStatus = latest_invoice.payment_intent.client_secret;
    // }

    saved = await newUser.save();
    const personalSpace = new Space({
      private: true,
      name: "Personal",
      description: "Your personal space visible only to you.",
      logo:
        "https://eazydone.s3.eu-west-3.amazonaws.com/logos/fingerprints.png",
      accounts: [saved.username],
      frozen: true,
      ownerId: saved._id
    });
    await personalSpace.save();

    res.json({ msg: "Account created", user: req.body });
  } catch (err) {
    console.log("err: ", err);
    return next(err);
  }
};

const subscribe = async (req, res, next) => {
  try {
    const username = req.body.username;
    const user = await User.findOne({ username })
      .lean()
      .exec();

    // // create stripe customer
    const { id: stripeCustomerId } = await stripe.customers.create({
      email: user.email,
      name: req.body.companyName,
      address: {
        line1: req.body.address
      },
      source: req.body.stripeTokenId
    });
    // create stripe subscription
    const {
      id: stripeSubscriptionId,
      latest_invoice,
      status
    } = await stripe.subscriptions.create({
      customer: stripeCustomerId,
      trial_from_plan: true,
      items: [{ plan: process.env.STRIPE_PLAN_ID }],
      expand: ["latest_invoice.payment_intent"]
    });

    await User.findOneAndUpdate(
      { username },
      { $set: { stripeCustomerId, stripeSubscriptionId } },
      { useFindAndModify: false }
    )
      .lean()
      .exec();

    // update all companies limits
    await Company.update(
      { ownerId: user._id },
      { $set: { usersCountLimit: 1000000, storagePerUserMb: 10 * 1024 } },
      { multi: true }
    ).exec();

    res.status(200).json({ message: "Success" });
  } catch (err) {
    console.log("err: ", err);
    return next(err);
  }
};

module.exports = {
  initialize,
  authenticate,
  genToken,
  login,
  signin,
  getStrategy,
  subscribe
};
