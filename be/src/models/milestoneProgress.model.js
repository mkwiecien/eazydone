const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const MilestoneProgressSchema = new Schema({
  milestoneId: {
    required: true,
    type: String
  },
  progress: {
    required: true,
    type: Number,
    default: 0,
    min: 0,
    max: 100
  }
});

module.exports = mongoose.model("MilestoneProgress", MilestoneProgressSchema);
