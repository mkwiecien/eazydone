const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const CompanySchema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  description: String,
  logo: String,
  accounts: [String],
  ownerId: {
    type: String,
    required: true
  },
  storagePerUserMb: {
    required: true,
    type: Number,
    default: 200
  },
  usersCountLimit: {
    required: true,
    type: Number,
    default: 5
  }
});

const Company = mongoose.model("Company", CompanySchema);

module.exports = Company;
