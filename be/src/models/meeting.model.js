const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const MeetingSchema = new Schema({
  name: {
    required: true,
    type: String
  },
  createdBy: String,
  updatedBy: String,
  description: String,
  startDate: {
    reuired: true,
    type: Number
  },
  endDate: Number,
  private: {
    type: Boolean,
    default: false
  },
  companyId: String,
  invitations: [
    {
      username: String,
      state: {
        type: String,
        required: true,
        default: "pending",
        enum: ["pending", "accepted", "declined"]
      }
    }
  ],
  invitedSpaces: [String],
  repetition: {
    type: String,
    enum: [
      "None",
      "Daily",
      "Weekly",
      "Monthly (weekday)",
      "Monthly (day number)"
    ]
  },
  color: String
});

module.exports = mongoose.model("Meeting", MeetingSchema);
