const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const FileSchema = new Schema({
  url: {
    required: true,
    type: String
  },
  name: {
    required: true,
    type: String
  },
  uploadedBy: {
    required: true,
    type: String
  },
  size: {
    required: true,
    type: Number
  },
  spaceId: {
    required: true,
    type: String
  },
  awsKey: String
});

module.exports = mongoose.model("File", FileSchema);
