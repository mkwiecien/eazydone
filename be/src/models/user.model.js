const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const Schema = mongoose.Schema;

const SALT_WORK_FACTOR = 10;

// const PERMISSIONS = ['manage_pages'];

const UserSchema = new Schema({
  username: {
    type: String,
    // required: true,
    unique: true,
    max: 100,
    sparse: true,
    trim: true
  },
  stripeCustomerId: String,
  stripeTokenId: String,
  stripeSubscriptionId: String,
  companyName: String,
  companyAddress: String,
  fullName: String,
  paymentStatus: {
    type: String
    // enum: ["success", "fail", "action_required"], or a payment intent secret
  },
  email: {
    type: String,
    required: true,
    unique: true,
    match: [
      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
      "Please fill a valid email address"
    ],
    max: 200,
    trim: true
  },
  password: {
    type: String,
    required: true
  },
  name: String,
  surname: String,
  avatar: String,
  permissions: [String],
  active: {
    type: Boolean,
    required: true,
    default: false
  },
  accountType: {
    required: true,
    type: String,
    enum: [
      "USER", // accont added by owner
      "OWNER", // the guy who paid for the plan (owner of the company)
      "ADMIN" // same permissions as owner, but not owner. I'm not sure if
      // I'll use this one. Maybe simply USER with special permissions
    ],
    default: "USER"
  },
  lastSelectedSpaceId: String,
  lastSeen: {
    type: Number,
    default: 0
  },
  jobTitle: String,
  isDemo: {
    type: Boolean,
    default: false
  }
});

UserSchema.pre("save", function(next) {
  const user = this;

  if (!user.isModified("password")) return next();
  return bcrypt.genSalt(SALT_WORK_FACTOR, (err, salt) => {
    if (err) return next(err);
    return bcrypt.hash(user.password, salt, (error, hash) => {
      if (error) return next(error);
      user.password = hash;
      return next();
    });
  });
});

UserSchema.methods.comparePassword = async function(candidatePass) {
  try {
    const match = await bcrypt.compare(candidatePass, this.password);
    return match;
  } catch (err) {
    return err;
  }
};

const User = mongoose.model("User", UserSchema);

module.exports = User;
