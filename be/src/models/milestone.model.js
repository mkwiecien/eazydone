const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const MilestoneSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  spaceId: {
    required: true,
    type: String
  },
  description: String,
  startDate: Number,
  endDate: Number,
  completed: {
    type: Boolean,
    default: false
  }
});

const Milestone = mongoose.model("Milestone", MilestoneSchema);
module.exports = Milestone;
