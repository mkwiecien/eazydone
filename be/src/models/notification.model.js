const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const NotificationSchema = new Schema({
  title: String,
  text: String,
  link: String,
  seen: {
    type: Boolean,
    default: false
  },
  username: {
    type: String,
    required: true
  },
  quiet: {
    type: Boolean,
    default: false
  },
  important: {
    type: Boolean,
    default: false
  }
});

module.exports = mongoose.model("Notification", NotificationSchema);
