const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const TimeLogSchema = new Schema({
  taskId: {
    required: true,
    type: String
  },
  userId: {
    required: true,
    type: String
  },
  minutes: {
    required: true,
    type: Number,
    default: 0
  }
});

module.exports = mongoose.model("TimeLog", TimeLogSchema);
