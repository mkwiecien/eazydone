const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const SpaceSchema = new Schema({
  private: {
    type: Boolean,
    default: false
  },
  name: {
    required: true,
    type: String
  },
  description: String,
  accounts: [String],
  states: {
    type: [
      {
        text: String,
        color: String
      }
    ],
    default: [
      { text: "Open", color: "blue" },
      { text: "In progress", color: "orange" },
      { text: "Done", color: "green" }
    ]
  },
  logo: String,
  companyId: String,
  frozen: {
    // tells if space is personal or company's
    required: true,
    type: Boolean,
    default: false
  },
  ownerId: String
});

module.exports = mongoose.model("Space", SpaceSchema);
