const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const TaskSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  createdBy: {
    required: true,
    type: String
  },
  updatedBy: String,
  description: String,
  todos: {
    type: [
      {
        text: {
          required: true,
          type: String
        },
        done: {
          required: true,
          type: Boolean,
          default: false
        }
      }
    ],
    default: []
  },
  milestoneId: String,
  spaceId: {
    type: String,
    required: true
  },
  files: [String],
  state: {
    type: String,
    required: true
  },
  difficulty: {
    type: Number,
    required: true
  },
  assignee: String,
  participantsIds: [String],
  comments: [
    {
      author: {
        type: String,
        required: true
      },
      text: {
        type: String,
        required: true
      },
      date: {
        reuqired: true,
        type: Number
      }
    }
  ],
  deadline: Number,
  order: {
    type: Number,
    default: 0
  },
  tagsNames: [String],
  history: {
    type: [
      {
        text: {
          type: String,
          required: true
        },
        icon: String,
        date: Number,
        updatedBy: String,
        diff: [String]
      }
    ],
    default: []
  }
});

module.exports = mongoose.model("Task", TaskSchema);
