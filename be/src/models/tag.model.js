const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const TagSchema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  color: {
    required: true,
    type: String
  },
  spaceId: {
    required: true,
    type: String
  }
});

module.exports = mongoose.model("Tag", TagSchema);
