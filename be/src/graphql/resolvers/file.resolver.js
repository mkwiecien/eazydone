const File = require("../../models/file.model");

module.exports = {
  Query: {
    files: (_, { spacesIds }) =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await File.find({
            spaceId: { $in: spacesIds }
          })
            .lean()
            .exec();
          resolve(res);
        } catch (err) {
          reject(err);
        }
      })
  }
};
