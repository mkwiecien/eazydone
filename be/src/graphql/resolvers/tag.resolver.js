const Tag = require("../../models/tag.model");

module.exports = {
  Query: {
    tags: (_, { spacesIds }) =>
      new Promise(async (resolve, reject) => {
        try {
          const tags = await Tag.find({ spaceId: spacesIds })
            .lean()
            .exec();
          resolve(tags);
        } catch (err) {
          reject(err);
        }
      })
  },
  Mutation: {
    addTag: (_, payload) =>
      new Promise(async (resolve, reject) => {
        try {
          const newTag = new Tag(payload);
          const res = await newTag.save();
          resolve(res);
        } catch (err) {
          reject(err);
        }
      }),
    editTag: (_, payload) => {
      new Promise(async (resolve, reject) => {
        try {
          const res = await Tag.findOneAndUpdate(
            { _id: payload._id },
            { $set: payload },
            { new: true, useFindAndModify: false }
          ).exec();
          resolve(res);
        } catch (err) {
          reject(err);
        }
      });
    },
    deleteTag: (_, { _id }) =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await Tag.findOneAndRemove({ _id }).exec();
          resolve(res);
        } catch (err) {
          reject(err);
        }
      })
  }
};
