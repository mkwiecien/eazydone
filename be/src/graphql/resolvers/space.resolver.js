const _ = require("lodash");

const Space = require("../../models/space.model");
const User = require("../../models/user.model");
const Company = require("../../models/company.model");
const Task = require("../../models/task.model");

const userResolver = require("./user.resolver");
const taskResolver = require("./task.resolver");

const SpaceRelations = {
  users: space =>
    new Promise(async (resolve, reject) => {
      try {
        if (space.frozen && space.companyId) {
          const company = await Company.findOne({
            _id: space.companyId
          }).exec();
          const accounts = [...company.accounts, company.ownerId];
          const ids = accounts.filter(el => el.match(/^[0-9a-fA-F]{24}$/));

          const res = await User.find({
            $or: [
              { _id: { $in: ids } },
              { username: { $in: accounts } },
              { email: { $in: accounts } },
              { _id: space.ownerId }
            ]
          })
            .lean()
            .exec();
          resolve(res);
          return;
        } else if (space.frozen) {
          // personal space
          const res = await User.find({ _id: space.ownerId })
            .lean()
            .exec();
          resolve(res);
          return;
        }

        const accounts = [...space.accounts, space.ownerId];
        const ids = accounts.filter(el => el.match(/^[0-9a-fA-F]{24}$/));

        const res = await User.find({
          $or: [
            { _id: { $in: ids } },
            { username: { $in: accounts } },
            { email: { $in: accounts } }
          ]
        })
          .lean()
          .exec();
        resolve(res);
      } catch (err) {
        reject(err);
      }
    })
};

module.exports = {
  Query: {
    space: (root, args) =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await Space.findOne(args)
            .populate()
            .lean()
            .exec();

          resolve(res);
        } catch (err) {
          reject(err);
        }
      }),
    userSpaces: (root, args) =>
      new Promise(async (resolve, reject) => {
        try {
          const user = await User.findOne(args)
            .lean()
            .exec();

          const userSpaces = await Space.find({
            $or: [
              { ownerId: user._id },
              { accounts: user.username },
              { accounts: user.email },
              { accounts: user._id }
            ]
          })
            .lean()
            .exec();

          const companies = await userResolver.User.companies(user);
          const companiesIds = companies.map(c => c._id);
          const companiesSpaces = await Space.find({
            companyId: { $in: companiesIds },
            $or: [
              { frozen: false },
              { frozen: true, accounts: user.username },
              { frozen: true, companyId: { $exists: true } }
            ],
            private: false
          })
            .lean()
            .exec();

          const compareIds = (a, b) => a._id.toString() === b._id.toString();
          resolve(_.unionWith(userSpaces, companiesSpaces, compareIds));
        } catch (err) {
          reject(err);
        }
      })
  },
  Space: SpaceRelations,
  Mutation: {
    createSpace: (root, payload) => {
      return new Promise(async (resolve, reject) => {
        const newSpace = new Space(payload);
        try {
          const count = await Space.count({
            companyId: payload.companyId,
            name: payload.name
          });

          if (count > 0) {
            throw new Error("Space with such name already exists.");
          }

          const res = await newSpace.save();
          res.users = await SpaceRelations.users(res);

          resolve(res);
        } catch (err) {
          reject(err);
        }
      });
    },
    editSpace: (root, payload) =>
      new Promise(async (resolve, reject) => {
        try {
          let res = await Space.findByIdAndUpdate(
            { _id: payload._id },
            { $set: { ...payload } },
            { new: false, lean: true }
          );

          // ignore empty array of states
          if (payload.states.length === 0) {
            res = await Space.findByIdAndUpdate(
              { _id: payload._id },
              { $set: { ...payload, states: res.states } },
              { new: false, lean: true }
            );
          }

          // some states were removed
          if (res.states.length > payload.states.length) {
            const removedStates = _.differenceBy(
              res.states,
              payload.states,
              "text"
            );
            const firstState = payload.states[0];
            const tasksToBeUpdated = await Task.find({
              state: { $in: removedStates.map(s => s.text) }
            }).lean();

            const promises = tasksToBeUpdated.map(task =>
              taskResolver.Mutation.editTask(null, {
                ...task,
                state: firstState.text,
                updatedBy: ""
              })
            );
            await Promise.all(promises);
          }
          resolve({ ...res, ...payload });
        } catch (err) {
          reject(err);
        }
      }),
    deleteSpace: (root, args) =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await Space.findOneAndRemove(args).exec();
          resolve(res);
        } catch (err) {
          reject(err);
        }
      })
  }
};
