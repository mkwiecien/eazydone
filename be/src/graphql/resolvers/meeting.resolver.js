const Meeting = require("../../models/meeting.model");
const Space = require("../../models/space.model");
const NotificationResolver = require("./notification.resolver");

const { pubsub } = require("../subscriptions");
const { withFilter } = require("graphql-subscriptions");

const agenda = require("../../agenda");

const { uniq, differenceBy } = require("lodash");
const moment = require("moment");

agenda.define("meetingNotification", async (job, done) => {
  const meeting = await Meeting.findOne({ _id: job.attrs.data }).lean();
  const users = await getUsersForNotification(meeting, true);

  if (!meeting) done();

  const promises = users.map(username =>
    NotificationResolver.Mutation.addNotification(null, {
      title: "Meeting starts in less than 10 minutes",
      text: meeting.name,
      link: `/calendar/${meeting._id}`,
      seen: false,
      username,
      important: true
    })
  );
  await Promise.all(promises);
  done();
});

function scheduleMeetingNotification(meeting) {
  agenda.cancel({
    name: "meetingNotification",
    data: meeting._id.toString()
  });

  const now = moment().unix();
  let startDate = meeting.startDate > now ? meeting.startDate : now;
  startDate = moment
    .unix(startDate)
    .subtract(10, "minutes")
    .toDate();

  let job = null;
  switch (meeting.repetition) {
    case "None":
      agenda.schedule(
        moment
          .unix(meeting.startDate) // this has to be startDate from meeting
          .subtract(10, "minutes")
          .toDate(),
        "meetingNotification",
        meeting._id.toString()
      );
      break;
    case "Daily":
      job = agenda.create("meetingNotification", meeting._id.toString());
      job.schedule(startDate).repeatEvery("day");
      job.save();
      break;
    case "Weekly":
      job = agenda.create("meetingNotification", meeting._id.toString());
      job.schedule(startDate).repeatEvery("week");
      job.save();
      break;
    case "Monthly (weekday)":
      job = agenda.create("meetingNotification", meeting._id.toString());
      job.schedule(startDate).repeatEvery("4 weeks");
      job.save();
      break;
    case "Monthly (day number)":
      job = agenda.create("meetingNotification", meeting._id.toString());
      job
        .schedule(startDate)
        .repeatEvery(` 0 0 ${moment(startDate).format("D")} * *`);
      job.save();
      break;
  }
}

async function getUsersForNotification(payload, withUpdatedBy = false) {
  let users = [];
  if (payload.invitedSpaces.length > 0) {
    const spaces = await Space.find({
      _id: { $in: payload.invitedSpaces }
    });
    users = users.concat(...spaces.map(s => s.accounts));
  }
  users = users.concat(
    payload.invitations.map(i => i.username)
    // payload.invitations.filter(i => i.state !== "accepted").map(i => i.username)
  );
  users = uniq(users);
  if (!withUpdatedBy) {
    users = users.filter(u => u !== payload.updatedBy);
  }
  return users;
}

module.exports = {
  Query: {
    calendar: (_, args) =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await Meeting.find(args)
            .lean()
            .exec();
          resolve(res);
        } catch (err) {
          reject(err);
        }
      }),
    companyMeetings: (_, { companyId, username }) =>
      new Promise(async (resolve, reject) => {
        try {
          let res = await Meeting.find({ companyId })
            .sort({ startDate: 1 })
            .lean()
            .exec();

          res = res.map(m => {
            if (
              m.private &&
              !m.invitations.find(i => i.username === username)
            ) {
              return {
                ...m,
                name: "Private",
                description: "private",
                color: "lightgray"
              };
            }
            return m;
          });
          resolve(res);
        } catch (err) {
          reject(err);
        }
      })
  },
  Mutation: {
    createMeeting: (_, payload) =>
      new Promise(async (resolve, reject) => {
        const newMeeting = new Meeting(payload);
        try {
          const res = await newMeeting.save();
          pubsub.publish("meetingUpdated", { meetingUpdated: res });

          // notifications for all participants
          const users = await getUsersForNotification(payload);

          const notificationsPromises = users.map(username =>
            NotificationResolver.Mutation.addNotification(null, {
              title: "You were invited to a new meeting",
              text: payload.name,
              seen: false,
              username,
              link: `/calendar/${res._id}`
            })
          );
          await Promise.all(notificationsPromises);

          scheduleMeetingNotification(res);

          resolve(res);
        } catch (err) {
          reject(err);
        }
      }),

    editMeeting: (_, payload) =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await Meeting.findOneAndUpdate(
            { _id: payload._id },
            { $set: payload },
            { new: false, lean: true, useFindAndModify: false }
          ).exec();

          if (
            payload.startDate !== res.startDate ||
            payload.endDate !== res.endDate
          ) {
            // These are old users. We don't want rescheduled message for new
            // guys
            const users = await getUsersForNotification(res);
            const notificationsPromises = users.map(username =>
              NotificationResolver.Mutation.addNotification(null, {
                title: "Meeting was rescheduled",
                text: payload.name,
                seen: false,
                link: `/calendar/${payload._id}`,
                username
              })
            );
            await Promise.all(notificationsPromises);

            scheduleMeetingNotification(payload);
          }

          if (
            payload.invitations.length > res.invitations.length ||
            payload.invitedSpaces.length > res.invitedSpaces.length
          ) {
            const users = await getUsersForNotification(
              {
                invitations: differenceBy(
                  payload.invitations,
                  res.invitations,
                  "username"
                ),
                invitedSpaces: differenceBy(
                  payload.invitedSpaces,
                  res.invitedSpaces,
                  "username"
                )
              },
              true
            );

            const notificationsPromises = users.map(username =>
              NotificationResolver.Mutation.addNotification(null, {
                title: "You were invited to a new meeting",
                text: payload.name,
                seen: false,
                username,
                link: `/calendar/${payload._id}`
              })
            );
            await Promise.all(notificationsPromises);
          }

          const updatedMeeting = { ...res, ...payload };

          pubsub.publish("meetingUpdated", { meetingUpdated: updatedMeeting });
          resolve(updatedMeeting);
        } catch (err) {
          reject(err);
        }
      }),

    deleteMeeting: (_, payload) =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await Meeting.findOneAndRemove(payload).exec();
          pubsub.publish("meetingDeleted", { meetingDeleted: res });

          const users = await getUsersForNotification(res);
          const notificationsPromises = users.map(username =>
            NotificationResolver.Mutation.addNotification(null, {
              title: "Meeting was canceled",
              text: res.name,
              seen: false,
              link: "",
              username
            })
          );
          await Promise.all(notificationsPromises);

          agenda.cancel({
            name: "meetingNotification",
            data: res._id.toString()
          });

          resolve(res);
        } catch (err) {
          reject(err);
        }
      })
  },
  Subscription: {
    meetingUpdated: {
      subscribe: withFilter(
        () => pubsub.asyncIterator("meetingUpdated"),
        ({ meetingUpdated }, { username, companyId }) => {
          // meetingUpdated.updatedBy !== username &&

          return meetingUpdated.companyId === companyId;
          // &&
          // (!meetingUpdated.private ||
          //   (meetingUpdated.private &&
          //     !!meetingUpdated.invitations.find(i => i.username === username)))
        }
      )
    },
    meetingDeleted: {
      subscribe: withFilter(
        () => pubsub.asyncIterator("meetingDeleted"),
        ({ meetingDeleted }, { companyId }) =>
          meetingDeleted.companyId === companyId
      )
    }
  }
};
