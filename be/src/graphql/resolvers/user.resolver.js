const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const moment = require("moment");

const { pubsub } = require("../subscriptions");
const { withFilter } = require("graphql-subscriptions");

const User = require("../../models/user.model");
const Company = require("../../models/company.model");
const Space = require("../../models/space.model");

const UserRelations = {
  companies: user =>
    new Promise(async (resolve, reject) => {
      try {
        const res = await Company.find({
          $or: [{ ownerId: user._id }, { accounts: user._id }]
        }).exec();

        resolve(res);
      } catch (err) {
        reject(err);
      }
    })
};

module.exports = {
  Query: {
    user: (root, args) => {
      return new Promise((resolve, reject) => {
        User.findOne(args)
          .populate()
          .lean()
          .exec((err, res) => {
            if (err) {
              return reject(err);
            }
            resolve(res);
          });
      });
    },
    users: (root, args) => {
      return new Promise((resolve, reject) => {
        User.find({
          _id: { $in: args._ids.map(_id => mongoose.Types.ObjectId(_id)) }
        })
          .populate()
          .exec((err, res) => {
            err ? reject(err) : resolve(res);
          });
      });
    }
  },
  User: UserRelations,
  Mutation: {
    addUser: (root, { username, email, password }) =>
      new Promise(async (resolve, reject) => {
        const newUser = new User({ username, email, password });
        try {
          const res = await newUser.save();

          const personalSpace = new Space({
            private: true,
            name: "Personal",
            description: "Your personal space visible only to you.",
            logo:
              "https://eazydone.s3.eu-west-3.amazonaws.com/logos/fingerprints.png",
            accounts: [res._id],
            frozen: true
          });
          await personalSpace.save();

          resolve({ ...res, payload });
        } catch (err) {
          reject(err);
        }
      }),
    editUser: (root, payload) => {
      return new Promise(async (resolve, reject) => {
        try {
          if (payload.password) {
            const salt = await bcrypt.genSalt(10);
            const hash = await bcrypt.hash(payload.password, salt);

            payload.password = hash;
          }

          const res = await User.findOneAndUpdate(
            { $or: [{ username: payload.username }, { email: payload.email }] },
            { $set: payload },
            { useFindAndModify: false }
          )
            .lean()
            .exec();

          if (res.username !== payload.username) {
            const newSpace = new Space({
              private: true,
              name: "Personal",
              description: "Your personal space visible only to you.",
              logo:
                "https://eazydone.s3.eu-west-3.amazonaws.com/logos/fingerprints.png",
              accounts: [payload.username],
              frozen: true,
              ownerId: payload._id
            });
            await newSpace.save();
          }

          resolve({ ...res, ...payload });
        } catch (err) {
          reject(err);
        }
      });
    },
    deleteUser: (root, args) => {
      return new Promise((resolve, reject) => {
        User.findOneAndRemove(args).exec((err, res) => {
          err ? reject(err) : resolve(res);
        });
      });
    },
    checkIn: (_, { username }) =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await User.findOneAndUpdate(
            { username },
            { $set: { lastSeen: moment().unix() } },
            { upsert: true, new: true, useFindAndModify: false }
          )
            .lean()
            .exec();
          res.companies = await UserRelations.companies(res);

          pubsub.publish("checkedIn", { checkedIn: res });
          resolve(res);
        } catch (err) {
          reject(err);
        }
      })
  },
  Subscription: {
    checkedIn: {
      subscribe: withFilter(
        () => pubsub.asyncIterator("checkedIn"),
        ({ checkedIn }, { companyId }) => {
          const companyIds = checkedIn.companies.map(c => c._id);
          return companyIds.includes(companyId);
        }
      )
    }
  }
};
