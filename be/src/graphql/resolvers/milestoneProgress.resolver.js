const MilestoneProgress = require("../../models/milestoneProgress.model");

module.exports = {
  Query: {
    progress: (root, { milestoneIds }) =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await MilestoneProgress.find({
            milestoneId: { $in: milestoneIds }
          })
            .lean()
            .exec();
          resolve(res);
        } catch (err) {
          reject(err);
        }
      })
  }
};
