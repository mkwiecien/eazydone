const Milestone = require("../../models/milestone.model");
const Task = require("../../models/task.model");

const { pubsub } = require("../subscriptions");
const { withFilter } = require("graphql-subscriptions");

module.exports = {
  Query: {
    milestone: (root, args) =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await Milestone.findOne(args)
            .lean()
            .exec();
          resolve(res);
        } catch (err) {
          reject(err);
        }
      }),
    spaceMilestones: (root, { spaceId }) =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await Milestone.find({ spaceId }).exec();
          resolve(res);
        } catch (err) {
          reject(err);
        }
      }),
    spacesMilestones: (_, { spaceIds }) =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await Milestone.find({ spaceId: { $in: spaceIds } })
            .lean()
            .exec();
          resolve(res);
        } catch (err) {
          reject(err);
        }
      })
  },
  Milestone: {
    tasks: milestone =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await Task.find({ milestoneId: milestone._id })
            .lean()
            .exec();
          resolve(res);
        } catch (err) {
          reject(err);
        }
      })
  },
  Mutation: {
    addMilestone: (root, payload) =>
      new Promise(async (resolve, reject) => {
        const newMilestone = new Milestone(payload);
        try {
          const res = await newMilestone.save();
          resolve(res);
        } catch (err) {
          reject(err);
        }
      }),
    editMilestone: (root, payload) =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await Milestone.findOneAndUpdate(
            { _id: payload._id },
            { $set: payload },
            { new: true, useFindAndModify: false }
          ).exec();
          resolve(res);
        } catch (err) {
          reject(err);
        }
      }),
    deleteMilestone: (root, payload) =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await Milestone.findOneAndRemove(payload).exec();
          pubsub.publish("milestoneDeleted", { milestoneDeleted: res });
          resolve(res);
        } catch (err) {
          reject(err);
        }
      })
  },
  Subscription: {
    milestoneDeleted: {
      subscribe: withFilter(
        () => pubsub.asyncIterator("milestoneDeleted"),
        ({ milestoneDeleted }, { spacesIds }) =>
          spacesIds.includes(milestoneDeleted.spaceId)
      )
    }
  }
};
