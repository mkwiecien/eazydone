const mergeResolvers = require("merge-graphql-schemas").mergeResolvers;

const User = require("./user.resolver");
const Company = require("./company.resolver");
const Space = require("./space.resolver");
const Milestone = require("./milestone.resolver");
const Task = require("./task.resolver");
const TimeLog = require("./timeLog.resolver");
const MilestoneProgress = require("./milestoneProgress.resolver");
const Meeting = require("./meeting.resolver");
const Message = require("./message.resolver");
const Notification = require("./notification.resolver");
const File = require("./file.resolver");
const Tag = require("./tag.resolver");

const resolvers = [
  User,
  Company,
  Space,
  Milestone,
  Task,
  TimeLog,
  MilestoneProgress,
  Meeting,
  Message,
  Notification,
  File,
  Tag
];

module.exports = mergeResolvers(resolvers);
