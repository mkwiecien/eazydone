const mongoose = require("mongoose");
const moment = require("moment");
const lodash = require("lodash");

const Task = require("../../models/task.model");
const Milestone = require("../../models/milestone.model");

const NotificationResolver = require("./notification.resolver");

const { pubsub } = require("../subscriptions");
const { withFilter } = require("graphql-subscriptions");
const { uniq, differenceWith, isEqual } = require("lodash");

module.exports = {
  Query: {
    task: (_, args) =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await Task.findOne(args)
            .populate()
            .lean()
            .exec();
          resolve(res);
        } catch (err) {
          reject(err);
        }
      }),
    tasks: (_, { spacesIds }) =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await Task.find({ spaceId: { $in: spacesIds } })
            .sort({ order: 1 })
            .populate()
            .lean()
            .exec();
          resolve(res);
        } catch (err) {
          reject(err);
        }
      })
  },
  Mutation: {
    createTask: (_, payload) =>
      new Promise(async (resolve, reject) => {
        try {
          const maxOrder = await Task.find({ milestoneId: payload.milestoneId })
            .sort({ order: -1 })
            .limit(1)
            .lean()
            .exec();
          const order = (maxOrder && maxOrder[0] && maxOrder[0].order) || 0;

          const newTask = new Task({ ...payload, order: order + 1 });
          const res = await newTask.save();

          if (res.assignee) {
            await NotificationResolver.Mutation.addNotification(null, {
              title: "You have been assigned to a new task",
              text: payload.title,
              link: `/tasks/${res._id}`,
              seen: false,
              username: res.assignee
            });
          }

          pubsub.publish("taskUpdated", { taskUpdated: res });
          resolve(res);
        } catch (err) {
          reject(err);
        }
      }),
    editTask: (_, payload, ctx) =>
      new Promise(async (resolve, reject) => {
        try {
          payload.updatedBy = ctx.user.username;
          let res = await Task.findByIdAndUpdate(
            { _id: payload._id },
            { $set: { ...payload } },
            { new: false, useFindAndModify: false, lean: true }
          ).exec();

          let diffs = [];
          const updatedBy = payload.updatedBy;
          const date = moment().unix();

          // title was changed
          if (payload.title && payload.title !== res.title) {
            diffs.push({
              text: `Title was changed by @${updatedBy}`,
              icon: "font-colors",
              diff: [res.title, payload.title]
            });
          }

          // state was changed
          if (payload.state && payload.state !== res.state) {
            diffs.push({
              text: `@${updatedBy} changed state to ${payload.state}`,
              diff: [res.state, payload.state]
            });
          }

          // milestone was changed
          if (
            payload.milestoneId !== undefined &&
            res.milestoneId &&
            payload.milestoneId !== res.milestoneId
          ) {
            let ids = [
              payload.milestoneId &&
                mongoose.Types.ObjectId(payload.milestoneId),
              res.milestoneId && mongoose.Types.ObjectId(res.milestoneId)
            ].filter(el => !!el);

            if (ids.length > 0) {
              const milestones = await Milestone.find({ _id: { $in: ids } })
                .lean()
                .exec();
              const oldMilestoneName =
                (
                  milestones.find(m => m._id.toString() === res.milestoneId) ||
                  {}
                ).name || "Other tasks";
              const newMilestoneName =
                (
                  milestones.find(
                    m => m._id.toString() === payload.milestoneId
                  ) || {}
                ).name || "Other tasks";

              diffs.push({
                text: `Milestone was changed to ${newMilestoneName} by @${updatedBy}`,
                icon: "highlight",
                diff: [oldMilestoneName, newMilestoneName]
              });
            }
          }

          if (payload.milestoneId && payload.milestoneId !== res.milestoneId) {
            const maxOrder = await Task.find({
              milestoneId: payload.milestoneId
            })
              .sort({ order: -1 })
              .limit(1)
              .lean()
              .exec();
            const order = maxOrder[0].order;
            res = await Task.findByIdAndUpdate(
              { _id: payload._id },
              { $set: { order: order + 1 } },
              { new: true, useFindAndModify: false, lean: true }
            ).exec();
          }

          // assignee was changed
          if (payload.assignee && res.assignee !== payload.assignee) {
            diffs.push({
              text: `@${updatedBy} assigned task to @${payload.assignee}`,
              icon: "user-add",
              diff: [res.assignee, payload.assignee]
            });
            if (payload.updatedBy !== payload.assignee) {
              await NotificationResolver.Mutation.addNotification(null, {
                title: "A task have been assigned to you",
                text: payload.title,
                link: `/tasks/${res._id}`,
                seen: false,
                username: payload.assignee
              });
            }
          }

          // some comments were added
          if (
            payload.comments &&
            payload.comments.length > res.comments.length
          ) {
            let users = uniq([
              res.assignee,
              ...res.comments.map(c => c.author)
            ]).filter(el => !!el);

            users.splice(users.indexOf(payload.updatedBy), 1);

            const notificationsPromises = users.map(username =>
              NotificationResolver.Mutation.addNotification(null, {
                title: "New comment in task",
                text: payload.title,
                username,
                seen: false,
                link: `/tasks/${res._id}`
              })
            );

            // adding notification for mentioned people in comment
            const addedComment = payload.comments[payload.comments.length - 1];
            const mentionsRe = /@([^.,\/#!$%\^&\*;:{}=\-_`~()\s$]*)/gm;
            let match = null;
            while ((match = mentionsRe.exec(addedComment.text))) {
              const username = match[1];

              notificationsPromises.push(
                NotificationResolver.Mutation.addNotification(null, {
                  title: "You were mentioned in a comment",
                  text: payload.title,
                  username,
                  seen: false,
                  link: `/tasks/${res._id}`
                })
              );
            }

            await Promise.all(notificationsPromises);
          }

          if (payload.tagsNames) {
            const previousTags = res.tagsNames || [];
            const currTags = payload.tagsNames || [];
            const removedTags = lodash.difference(previousTags, currTags);
            const addedTags = lodash.difference(currTags, previousTags);
            if (addedTags.length > 0 || removedTags.length > 0) {
              let text = [];
              if (addedTags.length > 0) {
                text.push(`added tags: ${addedTags.join(", ")}`);
              }
              if (removedTags.length > 0) {
                text.push(`removed tags: ${removedTags.join(", ")}`);
              }

              diffs.push({
                text: `@${updatedBy} ${text.join(" and ")}`,
                icon: "tags",
                diff: [previousTags.join(", "), currTags.join(", ")]
              });
            }
          }

          if (payload.deadline && res.deadline !== payload.deadline) {
            // deadline was changed
            diffs.push({
              text: `Deadline was changed by @${updateBy}`,
              icon: "clock-circle",
              diff: [res.deadline, payload.deadline]
            });
            if (res.assignee && payload.updatedBy !== payload.assignee) {
              await NotificationResolver.Mutation.addNotification(null, {
                title: "Task deadline was changed",
                text: payload.title,
                link: `/tasks/${res._id}`,
                seen: false,
                username: res.assignee
              });
            }
          }

          // description was changed
          if (payload.description && res.description !== payload.description) {
            diffs.push({
              text: `Description was changed by @${updatedBy}`,
              icon: "edit",
              diff: [res.description, payload.description]
            });
            if (res.assignee && payload.updatedBy !== payload.assignee) {
              await NotificationResolver.Mutation.addNotification(null, {
                title: "Task description has changed",
                text: payload.title,
                link: `/tasks/${res._id}`,
                seen: false,
                username: res.assignee
              });
            }
          }

          // notify assignee that todos were changed
          if (
            res.assignee &&
            differenceWith(payload.todos, res.todos, isEqual).length > 0 &&
            payload.updatedBy !== payload.assignee
          ) {
            await NotificationResolver.Mutation.addNotification(null, {
              title: "Task's todos were updated.",
              text: payload.title,
              link: `/tasks/${res._id}`,
              seen: false,
              username: res.assignee
            });
          }

          let history = [];
          diffs = diffs.map(el => ({ ...el, date, updatedBy }));
          if (diffs.length > 0) {
            const taskWithUpdatedHistory = await Task.findByIdAndUpdate(
              { _id: payload._id },
              { $push: { history: { $each: diffs } } },
              { new: true, useFindAndModify: false, lean: true }
            ).exec();

            history = taskWithUpdatedHistory.history;
          }

          const updatedTask = {
            ...res,
            ...payload,
            history: [...(payload.history || []), ...history]
          };

          pubsub.publish("taskUpdated", { taskUpdated: updatedTask });
          resolve(updatedTask);
        } catch (err) {
          reject(err);
        }
      }),
    // right now it just move tasks to the backlog keeping spaceId
    editTasks: (_, { ids }, ctx) =>
      new Promise(async (resolve, reject) => {
        try {
          const prevTasks = await Task.aggregate([
            {
              $match: {
                _id: { $in: ids.map(_id => mongoose.Types.ObjectId(_id)) }
              }
            },
            {
              $addFields: { milestoneRealId: { $toObjectId: "$milestoneId" } }
            },
            {
              $lookup: {
                from: "milestones",
                localField: "milestoneRealId",
                foreignField: "_id",
                as: "milestone"
              }
            },
            {
              $project: { _id: 1, milestone: 1 }
            }
          ]).exec();

          const bulk = Task.collection.initializeUnorderedBulkOp();
          ids.forEach(_id => {
            const prevTask = prevTasks.find(el => el._id.toString() === _id);
            bulk.find({ _id: mongoose.Types.ObjectId(_id) }).updateOne({
              $set: { milestoneId: "backlog" },
              $push: {
                history: {
                  text: `Milestone was changed to Other tasks by @${ctx.user.username}`,
                  icon: "highlight",
                  diff: [prevTask.milestone[0].name, "Other tasks"]
                }
              }
            });
          });
          await bulk.execute();

          const res = await Task.find({
            _id: { $in: ids }
          })
            .lean()
            .exec();

          pubsub.publish("tasksUpdated", { tasksUpdated: res });
          resolve(res);
        } catch (err) {
          reject(err);
        }
      }),
    changeOrder: (_, { tasks }, ctx) =>
      new Promise(async (resolve, reject) => {
        try {
          const prevTasks = await Task.find({
            _id: { $in: tasks.map(t => t._id) }
          })
            .lean()
            .exec();

          const bulk = Task.collection.initializeUnorderedBulkOp();
          tasks.forEach(t => {
            const update = {
              $set: {
                order: t.order,
                state: t.state,
                updatedBy: ctx.user.username
              }
            };

            const prevTask = prevTasks.find(
              prevTask => prevTask._id.toString() === t._id
            );

            if (t.state !== prevTask.state) {
              update["$push"] = {
                history: {
                  text: `@${ctx.user.username} changed state to ${t.state}`,
                  diff: [prevTask.state, t.state]
                }
              };
            }

            bulk
              .find({ _id: mongoose.Types.ObjectId(t._id) })
              .updateOne(update);
          });
          await bulk.execute();

          const res = await Task.find({
            _id: { $in: tasks.map(el => el._id) }
          })
            .lean()
            .exec();

          pubsub.publish("tasksUpdated", { tasksUpdated: res });
          resolve(res);
        } catch (err) {
          reject(err);
        }
      }),
    deleteTask: (_, args) =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await Task.findOneAndRemove(args).exec();
          pubsub.publish("taskDeleted", { taskDeleted: res });
          resolve(res);
        } catch (err) {
          reject(err);
        }
      }),
    deleteTasks: (_, { ids }) =>
      new Promise(async (resolve, reject) => {
        try {
          await Task.deleteMany({ _id: ids }).exec();
          resolve([]);
        } catch (err) {
          reject(err);
        }
      })
  },
  Subscription: {
    taskUpdated: {
      subscribe: withFilter(
        () => pubsub.asyncIterator("taskUpdated"),
        ({ taskUpdated }, { spacesIds, username }) => {
          return (
            spacesIds.includes(taskUpdated.spaceId) &&
            taskUpdated.updatedBy !== username
          );
        }
      )
    },
    tasksUpdated: {
      subscribe: withFilter(
        () => pubsub.asyncIterator("tasksUpdated"),
        ({ tasksUpdated }, { spacesIds }) =>
          tasksUpdated.every(t => spacesIds.includes(t.spaceId))
      )
    },
    taskDeleted: {
      subscribe: withFilter(
        () => pubsub.asyncIterator("taskDeleted"),
        ({ taskDeleted }, { spacesIds }) =>
          spacesIds.includes(taskDeleted.spaceId)
      )
    }
  }
};
