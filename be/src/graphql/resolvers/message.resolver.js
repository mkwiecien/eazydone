const { pubsub } = require("../subscriptions");
const { withFilter } = require("graphql-subscriptions");

const Message = require("../../models/message.model");
const Space = require("../../models/space.model");

const notificationResolver = require("./notification.resolver");
const companyResolver = require("./company.resolver");

module.exports = {
  Query: {
    messages: (_, { username, spacesIds }) =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await Message.find({
            // $or: [
            //   username && { from: username },
            //   username && { to: username },
            //   spacesIds && spacesIds.length > 0 && { to: { $in: spacesIds } }
            // ].filter(el => !!el)
            $or: [
              { from: username },
              { to: username },
              { to: { $in: spacesIds } }
            ]
          })
            .sort({ _id: -1 })
            .limit(50) // todo - right now it's not really used
            .lean()
            .exec();
          resolve(res.reverse());
        } catch (err) {
          reject(err);
        }
      }),
    chat: (_, { from, to, spaceId, offset }) =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await Message.find({
            $or: [{ from, to }, { from: to, to: from }, { to: spaceId }]
          })
            .sort({ _id: -1 })
            .skip(offset)
            .limit(50)
            .lean()
            .exec();

          resolve(res.reverse());
        } catch (err) {
          reject(err);
        }
      })
  },
  Mutation: {
    addMessage: (_, payload) =>
      new Promise(async (resolve, reject) => {
        const newMessage = new Message(payload);
        try {
          const res = await newMessage.save();
          pubsub.publish("addedMessage", { addedMessage: res });

          if (!payload.to.match(/^[0-9a-fA-F]{24}$/)) {
            await notificationResolver.Mutation.addNotification(null, {
              title: "Unread message",
              link: `/messages/${payload.from}`,
              seen: false,
              username: payload.to,
              text: `You have an unread message from ${payload.from}`
            });
          } else {
            // send notification to everyone in the space
            const space = await Space.findOne({ _id: payload.to });
            let users = [...space.accounts];

            // its a company space
            if (space.frozen && space.companyId) {
              const company = await companyResolver.Query.company(null, {
                _id: space.companyId
              });

              users = [
                ...company.users.map(u => u.username),
                company.owner.username
              ];
            }

            const notificationsPromises = users
              .filter(u => u !== payload.from)
              .map(u =>
                notificationResolver.Mutation.addNotification(null, {
                  title: "Unread message",
                  link: `/messages/${payload.to}`,
                  seen: false,
                  username: u
                })
              );
            await Promise.all(notificationsPromises);
          }
          resolve(res);
        } catch (err) {
          reject(err);
        }
      }),
    type: (_, { username, to }) => {
      pubsub.publish("typing", {
        typing: {
          username,
          to
        }
      });
    }
  },
  Subscription: {
    addedMessage: {
      subscribe: withFilter(
        () => pubsub.asyncIterator("addedMessage"),
        ({ addedMessage }, { username, spaces }) =>
          addedMessage.from !== username &&
          [username, ...spaces].includes(addedMessage.to)
      )
    },
    typing: {
      subscribe: withFilter(
        () => pubsub.asyncIterator("typing"),
        ({ typing }, { username, spaces }) => {
          return (
            typing.username !== username &&
            [username, ...spaces].includes(typing.to)
          );
        }
      )
    }
  }
};
