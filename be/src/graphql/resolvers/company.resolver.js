const { differenceBy } = require("lodash");
const Company = require("../../models/company.model");
const User = require("../../models/user.model");

// returns added accounts
async function addAccountsToCompany(accounts) {
  if (accounts.length === 0) return [];

  const existing = accounts.filter(a => !!a._id).map(a => a._id);
  const withoutId = accounts.filter(a => !a._id);

  const existingNonActive = await User.find({
    email: { $in: withoutId.map(u => u.email) }
  })
    .lean()
    .exec();

  existing.push(...existingNonActive.map(u => u._id));
  const newOnes = differenceBy(withoutId, existingNonActive, "email");

  let added = await User.insertMany(
    newOnes.map(u => ({
      email: u.email,
      password: u.password,
      active: false
    }))
  );

  return [...existing, ...added.map(u => u._id)];
}

const CompanyRelations = {
  users: company =>
    new Promise(async (resolve, reject) => {
      try {
        let res = await User.find({ _id: { $in: company.accounts } })
          .populate()
          .exec();
        res = res.filter(u => !!u.username);
        resolve(res);
      } catch (err) {
        reject(err);
      }
    }),
  inactiveUsers: company =>
    new Promise(async (resolve, reject) => {
      try {
        let res = await User.find({ _id: { $in: company.accounts } })
          .populate()
          .exec();
        res = res.filter(u => !u.username);
        resolve(res);
      } catch (err) {
        reject(err);
      }
    }),
  owner: company =>
    new Promise(async (resolve, reject) => {
      try {
        const res = await User.findOne({ _id: company.ownerId })
          .populate()
          .exec();
        resolve(res);
      } catch (err) {
        reject(err);
      }
    })
};

module.exports = {
  Query: {
    company: (root, args) =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await Company.findOne(args)
            .populate()
            .lean()
            .exec();

          res.users = await CompanyRelations.users(res);
          res.owner = await CompanyRelations.owner(res);
          resolve(res);
        } catch (err) {
          reject(err);
        }
      })
  },
  Company: CompanyRelations,
  Mutation: {
    addCompany: (root, payload, ctx) =>
      new Promise(async (resolve, reject) => {
        try {
          // todo validate if it is freemium then allow only  one company for
          // an owner
          const accounts = await addAccountsToCompany(payload.accounts);

          if (accounts.length > 5) {
            throw new Error(
              "Too many users. Add company with less/without users and upgrade to Premium."
            );
          }

          const newCompany = new Company({
            ...payload,
            accounts
          });
          const company = await newCompany.save();
          resolve(company);
        } catch (err) {
          reject(err);
        }
      }),
    editCompany: (root, payload) =>
      new Promise(async (resolve, reject) => {
        try {
          const company = await Company.findOne({ _id: payload._id })
            .lean()
            .exec();
          const accounts = await addAccountsToCompany(payload.accounts);
          const limit = (company && company.usersCountLimit) || 5;

          if (accounts.length > limit) {
            throw new Error("Too many users. Upgrade your plan to Premium.");
          }

          const res = await Company.findOneAndUpdate(
            { _id: payload._id },
            { $set: { ...payload, accounts } },
            { new: true, useFindAndModify: false }
          ).exec();

          res.inactiveUsers = await CompanyRelations.inactiveUsers(res);
          resolve(res);
        } catch (err) {
          reject(err);
        }
      }),
    deleteCompany: (root, args) =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await Company.findOneAndRemove(args).exec();
          resolve(res);
        } catch (err) {
          reject(err);
        }
      })
  }
};
