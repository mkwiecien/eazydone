const Notification = require("../../models/notification.model");

const { pubsub } = require("../subscriptions");
const { withFilter } = require("graphql-subscriptions");

module.exports = {
  Query: {
    notifications: (_, { username }) =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await Notification.find(
            { username },
            {},
            { sort: { _id: -1 }, limit: 20 }
          )
            .lean()
            .exec();
          resolve(res);
        } catch (err) {
          reject(err);
        }
      })
  },
  Mutation: {
    addNotification: (_, payload) =>
      new Promise(async (resolve, reject) => {
        try {
          const newNotification = new Notification(payload);
          const res = await newNotification.save();
          pubsub.publish("newNotification", { newNotification });
          resolve(res);
        } catch (err) {
          reject(err);
        }
      }),
    markSeen: (_, { _id }) =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await Notification.findByIdAndUpdate(
            { _id: _id },
            { $set: { seen: true } },
            { new: true }
          ).exec();
          resolve(res);
        } catch (err) {
          reject(err);
        }
      }),
    markSeenMany: (_, { ids }) =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await Notification.updateMany(
            { _id: { $in: ids } },
            { $set: { seen: true } }
          );
          resolve([]);
        } catch (err) {
          reject(err);
        }
      })
  },
  Subscription: {
    newNotification: {
      subscribe: withFilter(
        () => pubsub.asyncIterator("newNotification"),
        ({ newNotification }, { username }) =>
          newNotification.username === username
      )
    }
  }
};
