const mongoose = require("mongoose");

const TimeLog = require("../../models/timelog.model");

module.exports = {
  Query: {
    timeLog: (root, args) =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await TimeLog.find(args)
            .lean()
            .exec();
          resolve(res);
        } catch (err) {
          reject(err);
        }
      }),
    timeLogs: (root, { tasksIds }) =>
      new Promise(async (resolve, reject) => {
        try {
          const res = await TimeLog.find(
            { taskId: { $in: tasksIds } },
            {},
            { sort: { _id: 1 } }
          )
            .lean()
            .exec();
          resolve(res);
        } catch (err) {
          reject(err);
        }
      }),
    timeReport: (_, { userId, startDate, endDate }) =>
      new Promise(async (resolve, reject) => {
        try {
          const startId = mongoose.Types.ObjectId.createFromTime(startDate);
          const endId = mongoose.Types.ObjectId.createFromTime(endDate);
          const res = await TimeLog.aggregate([
            {
              $match: {
                userId,
                $and: [{ _id: { $gte: startId } }, { _id: { $lte: endId } }]
              }
            },
            {
              $group: {
                _id: "$taskId",
                minutes: { $sum: "$minutes" }
              }
            },
            { $addFields: { taskId: { $toObjectId: "$_id" } } },
            {
              $lookup: {
                from: "tasks",
                localField: "taskId",
                foreignField: "_id",
                as: "task"
              }
            },
            {
              $project: {
                _id: 1,
                minutes: 1,
                task: { $arrayElemAt: ["$task", 0] }
              }
            }
          ]);
          resolve(res);
        } catch (err) {
          reject(err);
        }
      })
  },
  Mutation: {
    logTime: (root, payload) =>
      new Promise(async (resolve, reject) => {
        const newTimeLog = new TimeLog(payload);
        try {
          const res = await newTimeLog.save();
          resolve(res);
        } catch (err) {
          reject(err);
        }
      }),
    deleteLastTimeLog: (root, args) =>
      new Promise(async (resolve, reject) => {
        try {
          const lastLog = await TimeLog.find(args)
            .lean()
            .sort({ _id: -1 })
            .limit(1);
          const lastLogId = (lastLog[0] || {})._id;
          const res = await TimeLog.findOneAndRemove({
            _id: lastLogId
          }).exec();
          resolve(res);
        } catch (err) {
          reject(err);
        }
      })
  }
};
