module.exports = `
  type Task {
    _id: String!
    title: String!
    createdBy: String!
    updatedBy: String
    description: String
    todos: [Todo]
    milestoneId: String
    spaceId: String!
    files: [String]
    state: String!
    difficulty: Int!
    assignee: String
    participantsIds: [String]
    comments: [Comment]
    deadline: Int,
    order: Int
    tagsNames: [String],
    history: [History]
  }

  type History {
    text: String!
    icon: String
    date: Int,
    diff: [String]
    updatedBy: String
  }

  type Comment {
    author: String!
    text: String!
    date: Int!
  }

  type Todo {
    text: String!
    done: Boolean!
  }

  input CommentInput {
    author: String!
    text: String!
    date: Int!
  }

  input TodoInput {
    text: String!
    done: Boolean!
  }

  input TaskOrderInput {
    _id: String,
    order: Int
    state: String
    updatedBy: String
  }

  type Query {
    task(_id: String!): Task
    tasks(spacesIds: [String]): [Task]
  }

  type Mutation {
    createTask(
      title: String!
      createdBy: String!
      updatedBy: String
      description: String
      todos: [TodoInput]
      milestoneId: String
      spaceId: String!
      files: [String]
      state: String!
      difficulty: Int!
      assignee: String
      comments: [CommentInput]
      deadline: Int
      tagsNames: [String]
    ): Task

    editTask(
      _id: String!
      title: String
      description: String
      todos: [TodoInput]
      milestoneId: String
      spaceId: String
      files: [String]
      state: String
      difficulty: Int
      assignee: String
      comments: [CommentInput]
      deadline: Int
      updatedBy: String
      tagsNames: [String]
    ): Task

    editTasks(ids: [String]!): [Task]

    changeOrder(tasks: [TaskOrderInput]): [Task]

    deleteTask(_id: String!): Task

    deleteTasks(ids: [String]!): [Task]
  }

  type Subscription {
    taskUpdated(spacesIds: [String]!, username: String!): Task
    tasksUpdated(spacesIds: [String]!): [Task]
    taskDeleted(spacesIds: [String]!): Task
  }

`;
