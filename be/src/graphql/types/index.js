const mergeTypes = require("merge-graphql-schemas").mergeTypes;
const User = require("./user.types");
const Company = require("./company.types");
const Space = require("./space.types");
const Milestone = require("./milestone.types");
const Task = require("./task.types");
const TimeLog = require("./timeLog.types");
const MilestoneProgress = require("./milestoneProgress.types");
const Meeting = require("./meeting.types");
const Message = require("./message.types");
const Notification = require("./notification.types");
const File = require("./file.types");
const Tag = require("./tag.types");

const typeDefs = [
  User,
  Company,
  Space,
  Milestone,
  Task,
  TimeLog,
  MilestoneProgress,
  Meeting,
  Message,
  Notification,
  File,
  Tag
];

module.exports = mergeTypes(typeDefs, { all: true });
