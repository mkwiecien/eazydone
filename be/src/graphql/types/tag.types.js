module.exports = `
  type Tag {
    _id: String!
    name: String!
    color: String!
    spaceId: String!
  }

  type Query {
    tags(spacesIds: [String]!): [Tag]
  }

  type Mutation {
    addTag(name: String!, color: String!, spaceId: String!): Tag
    editTag(_id: String!, name: String, color: String): Tag
    deleteTag(_id: String!): Tag
  }

`;
