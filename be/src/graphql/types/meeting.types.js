module.exports = `
  type Meeting {
    _id: String!
    name: String!
    description: String
    startDate: Int!
    endDate: Int
    private: Boolean
    invitations: [Invitation]
    invitedSpaces: [String]
    repetition: String
    companyId: String,
    color: String
    createdBy: String
    updatedBy: String
  }

  type Invitation {
    username: String
    state: String
  }

  input InvitationInput {
    username: String
    state: String
  }

  type Query {
    calendar(username: String!): [Meeting]
    companyMeetings(companyId: String!, username: String): [Meeting]
  }

  type Mutation {
    createMeeting(
      name: String!
      description: String
      startDate: Int!
      endDate: Int
      private: Boolean
      invitations: [InvitationInput]
      invitedSpaces: [String]
      repetition: String
      companyId: String,
      color: String!
      createdBy: String!
      updatedBy: String
    ): Meeting

    editMeeting(
      _id: String!
      name: String
      description: String
      startDate: Int
      endDate: Int
      private: Boolean
      invitations: [InvitationInput]
      invitedSpaces: [String]
      repetition: String
      updatedBy: String
    ): Meeting

    deleteMeeting(_id: String!): Meeting
  }

  type Subscription {
    meetingUpdated(username: String!, companyId: String!): Meeting
    meetingDeleted(companyId: String!): Meeting
  }

`;
