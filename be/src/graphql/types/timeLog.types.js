module.exports = `
  type TimeLog {
    _id: String
    taskId: String!
    userId: String!
    minutes: Int!
    task: Task
  }

  type Query {
    timeLog(userId: String, taskId: String): [TimeLog]
    timeLogs(tasksIds: [String]): [TimeLog]
    timeReport(userId: String!, startDate: Int!, endDate: Int!): [TimeLog]
  }

  type Mutation {
    logTime(taskId: String!, userId: String!, minutes: Int!): TimeLog
    deleteLastTimeLog(taskId: String!, userId: String!): TimeLog
  }

`;
