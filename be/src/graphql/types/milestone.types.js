module.exports = `
  type Milestone {
    _id: String!
    name: String!
    description: String
    spaceId: String!
    startDate: Int
    endDate: Int
    tasks: [Task]
    completed: Boolean
  }

  type Query {
    milestone(_id: String!): Milestone
    spaceMilestones(spaceId: String): [Milestone]
    spacesMilestones(spaceIds: [String]): [Milestone]
  }

  type Mutation {
    addMilestone(
      name: String!,
      description: String,
      spaceId: String!,
      startDate: Int,
      endDate: Int
    ): Milestone
    editMilestone(
      _id: String!,
      name: String,
      description: String,
      spaceId: String,
      startDate: Int,
      endDate: Int,
      completed: Boolean
    ): Milestone
    deleteMilestone(_id: String!): Milestone
  }

  type Subscription {
    milestoneDeleted(spacesIds: [String]!): Milestone
  }
`;
