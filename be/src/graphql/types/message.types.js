module.exports = `

  type Message {
    _id: String,
    text: String!,
    from: String!,
    to: String!,
  }

  type Query {
    messages(username: String!, spacesIds: [String]!): [Message]
    chat(from: String, to: String!, spaceId: String, offset: Int!): [Message]
  }

  type Typing {
    username: String!
    to: String!
  }

  type Mutation {
    addMessage(
      text: String!,
      from: String!,
      to: String!,
    ): Message
    type (username: String!, to: String!): Typing
  }

  type Subscription {
    addedMessage(username: String!, spaces: [String!]!): Message
    typing(username: String!, spaces: [String!]!): Typing
  }

`;
