module.exports = `
  type File {
    _id: String!,
    name: String!,
    url: String!,
    uploadedBy: String!,
    size: Int!,
    spaceId: String!
    awsKey: String
  }

  type Query {
    files(spacesIds: [String]): [File]
  }

`;
