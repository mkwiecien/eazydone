module.exports = `
  type Company {
    _id: String!,
    name: String!,
    description: String,
    logo: String,
    accounts: [String],
    users: [User],
    ownerId: String!,
    owner: User
    inactiveUsers: [User]
    storagePerUserMb: Int
    usersCountLimit: Int
  }

  input AccountInput {
    _id: String
    username: String
    email: String,
    password: String,
  }

  type Query {
    company(_id: String!): Company,
  }

  type Mutation {
    addCompany(
      name: String!,
      description: String,
      logo: String,
      accounts: [AccountInput],
      ownerId: String!,
    ): Company,
    editCompany(
      _id: String!,
      name: String,
      description: String,
      logo: String,
      accounts: [AccountInput],
    ): Company,
    deleteCompany(_id: String!): Company
  }

`;
