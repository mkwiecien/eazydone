module.exports = `
  type Notification {
    _id: String,
    title: String,
    text: String,
    link: String,
    seen: Boolean,
    username: String!
    quiet: Boolean
    important: Boolean
  }

  type Query {
    notifications(username: String!): [Notification]
  }

  type Mutation {
    addNotification(
      title: String,
      text: String,
      link: String,
      seen: Boolean,
      username: String!
      quiet: Boolean,
      important: Boolean
    ): Notification
    
    markSeen(_id: String): Notification
    markSeenMany(ids: [String!]!): [Notification]
  }

  type Subscription {
    newNotification(username: String!): Notification
  }
`;
