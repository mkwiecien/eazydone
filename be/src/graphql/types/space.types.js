module.exports = `
  type Space {
    _id: String,
    private: Boolean!,
    name: String!,
    description: String,
    accounts: [String]!,
    users: [User]
    logo: String,
    companyId: String,
    frozen: Boolean!
    ownerId: String,
    states: [State]
  }

  type State {
    text: String!,
    color: String!
  }

  input StateInput {
    text: String!,
    color: String!
  }

  type Query {
    space(_id: String!): Space
    userSpaces(username: String, email: String): [Space]
  }

  type Mutation {
    createSpace(
      private: Boolean,
      name: String!,
      description: String,
      accounts: [String],
      logo: String,
      companyId: String,
      frozen: Boolean,
      ownerId: String,
      states: [StateInput]
    ): Space

    editSpace(
      _id: String!,
      private: Boolean,
      name: String,
      description: String,
      accounts: [String],
      logo: String,
      companyId: String,
      states: [StateInput]
    ): Space

    deleteSpace(_id: String!): Space
  }
`;
