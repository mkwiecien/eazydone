module.exports = `
  type User {
    _id: String!
    username: String,
    password: String!,
    email: String!,
    name: String,
    surname: String,
    avatar: String,
    companies: [Company]!,
    permissions: [String]!,
    active: Boolean,
    accountType: String,
    lastSelectedSpaceId: String
    lastSeen: Int
    stripeCustomerId: String
    stripeTokenId: String
    stripeSubscriptionId: String
    companyName: String
    companyAddress: String
    paymentStatus: String
    jobTitle: String
    isDemo: Boolean
  }

  type Query {
    user(username: String, email: String): User
    users(_ids: [String]): [User]
  }

  type Mutation {
    addUser(
      username: String,
      email: String!,
      password: String!,
      name: String,
      surname: String,
      avatar: String,
      permissions: [String],
      active: Boolean,
      accountType: String!
    ): User
    editUser(
      username: String,
      email: String!,
      name: String,
      surname: String,
      avatar: String,
      permissions: [String],
      active: Boolean,
      password: String
      lastSelectedSpaceId: String
      jobTitle: String,
      stripeSubscriptionId: String
    ): User
    deleteUser(_id: String, username: String, email: String): User
    checkIn(username: String!): User
  }

  type Subscription {
    checkedIn(username: String!, companyId: String!): User
  }
`;
