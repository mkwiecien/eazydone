module.exports = `
  type MilestoneProgress {
    _id: String!,
    milestoneId: String!,
    progress: Int!
  }

  type Query {
    progress(milestoneIds: [String]!): [MilestoneProgress]
  }

`;
