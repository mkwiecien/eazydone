const gqltools = require("graphql-tools");
const makeExecutableSchema = gqltools.makeExecutableSchema;

const typeDefs = require("./types/");
const resolvers = require("./resolvers/");

// standard schema
const mainSchema = makeExecutableSchema({
  typeDefs,
  resolvers
});

module.exports = mainSchema;
